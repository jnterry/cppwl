#include <lie\test.hpp>
#include <cppwl-test\all.hpp>
#include <iostream>

int main(int argc, const char* argv[]) {
	lie::test::TestGroup rootTestGroup("Root");
	cppwl::Test_All(&rootTestGroup);
	if (rootTestGroup.saveReport("Unit Test Report.html")){
		std::cout << "Unit test report saved" << std::endl;
	}
	else {
		std::cout << "Failed to save unit test report" << std::endl;
	}
}