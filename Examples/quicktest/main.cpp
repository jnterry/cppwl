#include <iostream>
#include <sstream>
#include <cppwl/core.hpp>
#include <cppwl/cgi.hpp>
#include <cppwl/route.hpp>
#include <lie\core\Convert.hpp>

class StaticController{
public:
	StaticController(){
		std::cout << "Created Static Controller instance" << std::endl;
	}
	cppwl::Response& getIndex(const cppwl::ReceivedRequest& request){
		cppwl::Response* r = new cppwl::Response(200);
		r->setResponseBody("StaticController getIndex");
		return *r;
	}
};

class TestInterceptorA : public cppwl::Interceptor{
	cppwl::Response& intercept(const cppwl::ReceivedRequest& request){
		//std::cout << "TestInterceptorA pre app" << std::endl;
		cppwl::Response& r = next(request);
		//std::cout << "TestInterceptorA post app" << std::endl;
		return r;
	}
};

class TestInterceptorB : public cppwl::Interceptor{
	cppwl::Response& intercept(const cppwl::ReceivedRequest& request){
		//std::cout << "TestInterceptorB pre app" << std::endl;
		cppwl::Response& r = next(request);
		//std::cout << "TestInterceptorB post app" << std::endl;
		return r;
	}
};

int main(int argc, const char* argv[]) {
	
	cppwl::Router router;
	router.registerInterceptor(new TestInterceptorA);
	router.registerInterceptor(new TestInterceptorB);
	router.registerRoute("", new cppwl::RouteClassMethod<StaticController>(cppwl::HttpMethod::GET, 
		&StaticController::getIndex, new StaticController));

	cppwl::FastCgiSocket::RequestBuildOptions rbo;
	rbo.url = "$REQUEST_SCHEME :// $HTTP_HOST $REQUEST_URI";
	rbo.clientSocket = "$REMOTE_ADDR : $REMOTE_PORT";
	rbo.serverSocket = "$SERVER_ADDR : $SERVER_PORT";
	rbo.method = "$REQUEST_METHOD";
	rbo.contentLength = "$CONTENT_LENGTH";
	rbo.contentType = "$CONTENT_TYPE";
	rbo.deleteUsedVariables = true;
	try{
		cppwl::FastCgiSocket socket(8000, rbo);
		cppwl::ReceivedRequest* req = socket.makeRequest();
		while (socket.accept(*req)){
			std::cout << "***************************************************" << std::endl;
			std::cout << "Got request, client ip: " << req->getClientSocket().toString() << std::endl;
			cppwl::Response response;
			req->respond(router.dispatch(*req));
			std::cout << std::endl << std::endl;
		}
		delete req;
	}
	catch (std::exception& e){
		std::cout << "Program terminated due to unhandled exception" << std::endl;
		std::cout << e.what();
		std::cout << "Press enter to close" << std::endl;
		std::cin.sync();
		std::cin.get();
		return 1;
	}

	std::cout << "Program ended normally, press enter to terminate..." << std::endl;
	std::cin.get();

	return 0;
}