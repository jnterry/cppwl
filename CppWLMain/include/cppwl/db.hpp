/////////////////////////////////////////////////
///  Part of C++ Web Library - By Jamie Terry ///
/////////////////////////////////////////////////
/// \file db.hpp
/// \author Jamie Terry
/// \date 2015/08/27
/// \brief Convenience header that includes all other headers in the
/// db module
/// \defgroup db Contains classes related to creating database connections
/// to various sql databases, also contains ORM classes
/// \ingroup sb
/////////////////////////////////////////////////

#ifndef CPPWL_DB_HPP
#define CPPWL_DB_HPP

#include "db\SqliteConnection.hpp"

#endif