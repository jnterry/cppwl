/////////////////////////////////////////////////
///  Part of C++ Web Library - By Jamie Terry ///
/////////////////////////////////////////////////
/// \file core.hpp
/// \author Jamie Terry
/// \date 2015/07/24
/// \brief Convenience header that includes all other headers in the core
/// module
/// 
/// \defgroup core Contains a number of classes that are depended on by
/// all other modules in CppWL, the classes are all related to web and
/// or networking, for example URLs, IPs and classes for representing HTTP 
/// requests and responses.
/// \ingroup core
/////////////////////////////////////////////////

#ifndef CPPWL_CORE_HPP
#define CPPWL_CORE_HPP

#include "core\IPv4.hpp"
#include "core\Socket.hpp"
#include "core\ExcepNetworkError.hpp"
#include "core\HttpMethod.hpp"
#include "core\HTTPStatus.hpp"
#include "core\Request.hpp"
#include "core\ReceivedRequest.hpp"
#include "core\Response.hpp"
#include "core\HtmlDebug.hpp"

#endif