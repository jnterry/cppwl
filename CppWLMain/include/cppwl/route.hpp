/////////////////////////////////////////////////
///  Part of C++ Web Library - By Jamie Terry ///
/////////////////////////////////////////////////
/// \file route.hpp
/// \author Jamie Terry
/// \date 2015/08/22
/// \brief Convenience header that includes the other
/// headers in the route group
/// \defgroup route Contains classes for specifying routes which are functions
/// that map ReceivedRequest instances into Response instances.
/// \ingroup route
/////////////////////////////////////////////////

#ifndef CPPWL_ROUTE_HPP
#define CPPWL_ROUTE_HPP

#include "route\Route.hpp"
#include "route\RouteClassMethod.hpp"

#include "route\Interceptor.hpp"

#include "route\Router.hpp"

#endif