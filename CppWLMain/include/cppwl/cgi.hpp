/////////////////////////////////////////////////
///  Part of C++ Web Libary - By Jamie Terry  ///
/////////////////////////////////////////////////
/// \file cgi.hpp
/// \author Jamie Terry
/// \date 2015/07/24
/// \brief Convinience header that includes all other headers in
/// the CGI module
/// 
/// \defgroup CGI Contains classes for dealing with communication with
/// a web server via both the CGI and FastCGI protocols
/// \ingroup CGI
/////////////////////////////////////////////////

#ifndef CPPWL_CGI_HPP
#define CPPWL_CGI_HPP

#include "CGI\FastCgiSocket.hpp"

#endif