/////////////////////////////////////////////////
///  Part of C++ Web Library - By Jamie Terry ///
/////////////////////////////////////////////////
/// \file FastCgiSocket.hpp
/// \author Jamie Terry
/// \date 2015/08/18
/// \brief Contains decleration of the cppwl::FastCgiSocket class
/// 
/// \ingroup cgi
/////////////////////////////////////////////////
#ifndef CPPWL_CGI_FASTCGISOCKET_HPP
#define CPPWL_CGI_FASTCGISOCKET_HPP

#include <string>
#include "../core/ExcepNetworkError.hpp"

namespace cppwl{
	//forward deceleration
	class ReceivedRequest;
	class Url;
	class Socket;

	//////////////////////////////////////////////////////////////////////////
	/// \brief Represents a socket that has been setup for incoming fcgi connections
	/// from a web server
	/// Suggested usage:
	/// \code
	/// FastCgiSocket sock(8000);
	/// ReceivedRequest& request = sock.makeRequest();
	/// while(sock.accept(request)){
	///		//do stuff with request to calculate a response
	///     request.respond(responce)
	/// }
	/// \endcode
	//////////////////////////////////////////////////////////////////////////	
	class FastCgiSocket{
	public:
		//////////////////////////////////////////////////////////////////////////
		/// \brief Struct which holds various parameters that determine how a
		/// RecievedRequest object is constructed from environment variables
		/// in the .accept() method.
		/// Data such as the client socket, server socket, request url, etc are 
		/// calculated from the enviroment variables provided by the server.
		/// The strings in this struct should be used to specify how these parameters
		/// are calculated.
		/// The strings in this struct are used to generate a string to represent the
		/// data in question, the syntax for the strings is as follows:
		/// $ENV_VAR_NAME text $ANOTHER_ENV_VAR_NAME
		/// Tokens are separated by whitespace, if the first character in a token is a
		/// '$' it is taken to be the name of an environment variable, hence it is substituted
		/// for the value of the environment variable in the produced string. Otherwise the token
		/// is inserted as is into the produced string.
		/// For example, the string for the "url" member may be:
		/// \code
		/// $PROTOCOL :// $HOST / $DOCUMENT_URI
		/// \endcode
		/// The :// and / is inserted into the string and the variable names are replaced with
		/// their corrosponding value.
		/// For example, if $PROTOCOL = http, $HOST = "www.test.com" and 
		/// $DOCUMENT_URI = "thing.php?test=1" then the produced string would be:
		/// \code
		/// http://www.test.com/thing.php?test=1
		/// \endcode
		/// This would then be parsed to produce the object returned by request.getUrl()
		//////////////////////////////////////////////////////////////////////////
		struct RequestBuildOptions{
			///< Variable string used to produce a string that is passed to cppwl::parseHttpMethod()
			///< to determine the value returned by request.getMethod()
			std::string method;

			///< Variable string used to produce a string that is parsed by cppwl::Url
			///< to produce the object returned by request.getUrl()
			std::string url;

			///< Variable string used to produce a string that is parsed by cppwl::Socket
			///< to produce the object returned by request.getClientSocket()
			std::string clientSocket;

			///< Variable string used to produce a string that is parsed by cppwl::Socket
			///< to produce the obhect returned by request.getServerSocket()
			std::string serverSocket;

			///< Variable string used to produce a string that is returned by request.getContentType()
			std::string contentType;

			///< Variable string used to produce a string that is then passed to
			///< lie::core::parseInt to determine the length in bytes of the content
			///< included with the request
			std::string contentLength;
			
			//////////////////////////////////////////////////////////////////////////
			/// \brief If true any environment variables used in the construction of
			/// the request object will be deleted from the environment variable
			/// map. The data doesn't need to be stored twice so this should be true
			/// in most cases. Setting it to true also makes the system more robust as if
			/// the server changes + the environment variables change their name
			/// only the values in this struct need to be changed, rather than the whole
			/// code base if everywhere access the environment variables directly, rather than
			/// calling request.getClientSocket, getServerSocket, etc
			//////////////////////////////////////////////////////////////////////////
			bool deleteUsedVariables;
		};
		//////////////////////////////////////////////////////////////////////////
		/// \brief Creates and initializes a new FastCgiSocket to listen on the
		/// specified port number
		/// \param portNumber The port number of the socket to open to listen on
		/// \param options instance of RequestBuildOptions that will be used when 
		/// filling in the details of a ReceivedRequest in the accept method
		/// \throw lie::core::Exception if an error occurs initializing the socket
		//////////////////////////////////////////////////////////////////////////
		FastCgiSocket(unsigned short portNumber, const RequestBuildOptions& options) throw(cppwl::ExcepNetworkError);

		//////////////////////////////////////////////////////////////////////////
		/// \brief Closes the FastCgiSocket
		//////////////////////////////////////////////////////////////////////////
		~FastCgiSocket();

		//////////////////////////////////////////////////////////////////////////
		/// \brief Sets the RequestBuildOptions instance used to build the request
		/// in the accept method
		//////////////////////////////////////////////////////////////////////////
		void setRequestBuildOptions(const RequestBuildOptions& options);

		//////////////////////////////////////////////////////////////////////////
		/// \brief Returns a reference to the RequestBuildOptions instance that will be used
		/// to build request objects in the accept method. Modifying this object WILL
		/// update the one used by this FastCgiSocket. Doing this while a thread is waiting on
		/// .accept will cause undefined behavior
		//////////////////////////////////////////////////////////////////////////
		RequestBuildOptions& getRequestBuildOptions();

		//////////////////////////////////////////////////////////////////////////
		/// \brief Creates and returns a new instance of a class derived from 
		/// RecievedRequest used with the accept method
		/// \return pointer to the new ReceivedRequest object, the caller becomes
		/// the owner of this memory and must delete it when finished by doing:
		/// \code
		/// ReceivedRequest* req = socket.makeRequest();
		/// //do stuff
		/// delete req;
		/// \endcode
		//////////////////////////////////////////////////////////////////////////
		ReceivedRequest* makeRequest() throw(lie::core::Exception);

		//////////////////////////////////////////////////////////////////////////
		/// \brief Blocking function that waits for the web server to send a request
		/// to this FastCgiSocket, once received modifies the specified RecievedRequest
		/// object such that it represents the request sent by the client.
		/// \return bool True if no errors occurred, else false. If false is returned
		/// the RecievedRequest object 'request' is considered invalid and should
		/// no longer be used.
		/// \note This function is thread safe, hence you can have a pool of threads
		/// all simultaneously waiting for Requests by having each call accept(),
		/// this allows your program to respond to multiple clients making requests
		/// from the web server simultaneously
		/// \note You may only  use ReceivedRequest instances created using the
		/// makeRequest method, any other instances will result in undefined behavior
		//////////////////////////////////////////////////////////////////////////
		bool accept(ReceivedRequest& request);
	private:
		///< descriptor of the socket this is wrapping, value returned by FCGX_OpenSocket
		int mSocketDescriptor;

		///< The options used when builing a Request object in .accept()
		RequestBuildOptions mReqOptions;
	};
}

#endif