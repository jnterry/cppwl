/////////////////////////////////////////////////
///  Part of C++ Web Library - By Jamie Terry ///
/////////////////////////////////////////////////
/// \file HtmlDebug.hpp
/// \author Jamie Terry
/// \date 2015/08/22
/// \brief Contains functions for generating html debug reports
/// for various classes in the core module
/// \ingroup core
/////////////////////////////////////////////////

#ifndef CPPWL_CORE_HTMLDEBUG_HPP
#define CPPWL_CORE_HTMLDEBUG_HPP

#include "ReceivedRequest.hpp"
#include <lie\core\StringUtils.hpp>

namespace cppwl{
	//////////////////////////////////////////////////////////////////////////
	/// \brief Appends a html debug report about a ReceivedRequest instance
	/// to a string builder. This includes the values of .getXXX as
	/// well as a list of all the environment variables.
	//////////////////////////////////////////////////////////////////////////
	void htmlDebug(lie::core::StringBuilder& str, const ReceivedRequest& request);
}

#endif