/////////////////////////////////////////////////
///  Part of C++ Web Library - By Jamie Terry ///
/////////////////////////////////////////////////
/// \file Request.hpp
/// \author Jamie Terry
/// \date 2015/07/24
/// \brief Contains the Request class
/// \ingroup core
/////////////////////////////////////////////////

#ifndef CPPWL_CORE_REQUEST_HPP
#define CPPWL_CORE_REQUEST_HPP

#include "HttpMethod.hpp"
#include "../core/Url.hpp"

#include <string>

namespace cppwl{

	//////////////////////////////////////////////////////////////////////////
	/// \brief Represents a HTTP request that may or may not have yet been sent,
	/// as such this could represent a request received from a client or a request
	/// that this application will send. The derived class ClientRequest will be returned
	/// by cppwl for requests that have been received as it ClientRequest contains 
	/// additional information about the server, remote and server ip, etc.
	/// \see ClientRequest
	//////////////////////////////////////////////////////////////////////////
	class Request{
	public:
		//////////////////////////////////////////////////////////////////////////
		/// \brief Creates a new Request for the specified resource using the specified
		/// HttpMethod
		/// For example, to create a GET request to www.example.com/test/thing.php?blob=1&stuff=a
		/// make the following call
		/// \code
		///		cppwl::Request r(cppwl::HttpMethod::GET, "www.example.com", "/test/thing.php", "blob=1&stuff=a")
		/// \endcode
		/// \param method The HTTP method used to make the request
		/// \param serverRootURL The root URL of the web server, 
		//////////////////////////////////////////////////////////////////////////
		Request(HttpMethod method, const Url& url);

		//////////////////////////////////////////////////////////////////////////
		/// \brief Cleans up after this request instance
		//////////////////////////////////////////////////////////////////////////
		virtual ~Request();

		//////////////////////////////////////////////////////////////////////////
		/// \brief Returns the HttpMethod (eg, GET, POST, etc) used by the client
		//////////////////////////////////////////////////////////////////////////
		HttpMethod getMethod() const;

		//////////////////////////////////////////////////////////////////////////
		/// \brief Returns the content type for the content of this request
		//////////////////////////////////////////////////////////////////////////
		const std::string& getContentType() const;

		//////////////////////////////////////////////////////////////////////////
		/// \brief Returns the length of the content in bytes (excluding the null terminator)
		/// of the content included with this request
		//////////////////////////////////////////////////////////////////////////
		size_t getContentLength() const;

		//////////////////////////////////////////////////////////////////////////
		/// \brief Returns pointer to '\0' terminated array of characters containing
		/// the content of this request.
		/// \note The memory pointed at is owned by this Request object and will
		/// be deleted when the Request object is deleted
		/// \note Will return nullptr if this request has no content (eg, a GET
		/// request)
		//////////////////////////////////////////////////////////////////////////
		const char* getContent() const;

		//////////////////////////////////////////////////////////////////////////
		/// \brief Returns const reference to the url that is being requested
		//////////////////////////////////////////////////////////////////////////
		const Url& getUrl() const;

		//////////////////////////////////////////////////////////////////////////
		/// \brief Returns a reference to the url that is being requested
		//////////////////////////////////////////////////////////////////////////
		Url& getUrl();

		//////////////////////////////////////////////////////////////////////////
		/// \brief Sets the content and content type that is included with the request, 
		/// this should only be included in a POST, PUT, etc request
		//////////////////////////////////////////////////////////////////////////
		void setContent(std::string type, std::string content){
			setContentType(type);
			setContent(content);
		}

		//////////////////////////////////////////////////////////////////////////
		/// \brief Sets the content and content type that is included with the request, 
		/// this should only be included in a POST, PUT, etc request
		/// \param type String holding the MIME type of the content
		/// \param content Pointer to array containing the content, if nullptr is
		/// passed then contentLength is ignored and the current content of the request
		/// is deleted
		/// \param contentLength The length in bytes of the content
		/// \note This Request instance will copy the data, the client retains ownership
		/// of the content pointer
		//////////////////////////////////////////////////////////////////////////
		void setContent(std::string type, const char* content, size_t contentLength){
			setContentType(type);
			setContent(content, contentLength);
		}

		//////////////////////////////////////////////////////////////////////////
		/// \brief Sets the content without changing the content type
		//////////////////////////////////////////////////////////////////////////
		void setContent(std::string content){
			this->setContent(content.c_str(), content.length());
		}

		//////////////////////////////////////////////////////////////////////////
		/// \brief Sets the content without changing the content type
		/// \param content Pointer to array containing the content, if nullptr is
		/// passed then contentLength is ignored and the current content of the request
		/// is deleted
		/// \param contentLength The length in bytes of the content
		/// \note This Request instance will copy the data, the client retains ownership
		/// of the content pointer
		//////////////////////////////////////////////////////////////////////////
		void setContent(const char* content, size_t contentLength);

		//////////////////////////////////////////////////////////////////////////
		/// \brief Sets the content type without changing the content included in
		/// this request
		//////////////////////////////////////////////////////////////////////////
		void setContentType(std::string content);

		//////////////////////////////////////////////////////////////////////////
		/// \brief Sets the HTTP method that this request uses
		//////////////////////////////////////////////////////////////////////////
		void setMethod(HttpMethod method);

		//////////////////////////////////////////////////////////////////////////
		/// \brief Sets the URL being requested, copies data from the passed in
		/// url instance, this Request does not take ownership of the passed in
		/// url instance and does not require it to exist after this call
		//////////////////////////////////////////////////////////////////////////
		void setUrl(const Url& url);
	private:
		///< The method used by the client to make the request
		HttpMethod mMethod;

		///< The type of content included with the request (empty string except for for POST, PUT, etc requests)
		std::string mContentType;

		///< The length (in bytes) of the content included with the request
		///< (0 if mContent = nullptr)
		size_t mContentLength;

		///< The content included with the request, nullptr if no content
		char* mContent;

		///< The url being requested
		Url mUrl;
	};
}

#endif