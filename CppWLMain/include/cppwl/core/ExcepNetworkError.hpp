/////////////////////////////////////////////////
///  Part of C++ Web Library - By Jamie Terry ///
/////////////////////////////////////////////////
/// \file ExcepNetworkError.hpp
/// \author Jamie Terry
/// \date 2015/08/18
/// \brief Contains the ExcepNetworkError class
/// \ingroup core
/////////////////////////////////////////////////

#ifndef CPPWL_EXCEPNETWORKERROR_HPP
#define CPPWL_EXCEPNETWORKERROR_HPP

#include <string>
#include <lie\core\Exception.hpp>
#include "Socket.hpp"

namespace cppwl{

	//////////////////////////////////////////////////////////////////////////
	/// \brief thrown when an error occurs involving a network connection
	//////////////////////////////////////////////////////////////////////////
	class ExcepNetworkError : public lie::core::Exception{
	public:
		//////////////////////////////////////////////////////////////////////////
		/// \brief Creates a new ExcepNetworkError instance
		/// \param function string containing the name of the throwing function, use
		/// the compiler macro __FUNCTION__
		/// \param local The socket being used localally to make the connection, may be
		/// set to nullptr if not applicable, ip will be localhost (usually)
		/// \param remote The socket being connected to, may also be on localhost if
		/// connecting to another process on the same machine
		/// \param msg string containing extra details about the error, should be human readable
		/// \param cause Pointer to the exception that caused this one to be thrown
		//////////////////////////////////////////////////////////////////////////
		ExcepNetworkError(std::string function, const Socket* local, const Socket* remote, std::string msg, lie::core::Exception* cause = nullptr);
		
		//////////////////////////////////////////////////////////////////////////
		/// \brief Deletes the exception
		//////////////////////////////////////////////////////////////////////////
		~ExcepNetworkError();

		//////////////////////////////////////////////////////////////////////////
		/// \brief Returns the socket used locally for the connection
		//////////////////////////////////////////////////////////////////////////
		Socket* getLocalSocket();

		//////////////////////////////////////////////////////////////////////////
		/// \brief Returns the remote socket that is being connected to/is connected
		/// to, this may also be on localhost if connecting to another process on the
		/// same machine
		//////////////////////////////////////////////////////////////////////////
		Socket* getRemoteSocket();
	private:
		Socket* mLocalSocket;
		Socket* mRemoteSocket;
	};
}

#endif
