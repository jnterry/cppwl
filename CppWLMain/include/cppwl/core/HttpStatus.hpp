/////////////////////////////////////////////////
///  Part of C++ Web Library - By Jamie Terry ///
/////////////////////////////////////////////////
/// \file HttpStatus.hpp
/// \author Jamie Terry
/// \date 2015/08/21
/// \brief Contains the cppwl::HttpStatus struct
/// \ingroup core
/////////////////////////////////////////////////

#ifndef CPPWL_CORE_HTTPSTATUS_HPP
#define CPPWL_CORE_HTTPSTATUS_HPP

#include <string>

namespace cppwl{
	//////////////////////////////////////////////////////////////////////////
	/// \brief Represents a HTTP status, this includes a code and text describing
	/// the code. Eg, 200 OK, 404 NOT FOUND, etc
	/// Also contains various commonly used statuses that can be used in a
	/// Response object rather than creating a new instance each time
	//////////////////////////////////////////////////////////////////////////
	struct HttpStatus{
		//////////////////////////////////////////////////////////////////////////
		/// \brief Enumeration of the types of status codes, value of the enum is
		/// the first digit of status codes in that series, for example the 2xx
		/// successful series has the value '2'
		//////////////////////////////////////////////////////////////////////////
		enum Series{
			//////////////////////////////////////////////////////////////////////////
			/// \brief The status is not in a know series
			//////////////////////////////////////////////////////////////////////////
			UNKNOWN = 0,

			//////////////////////////////////////////////////////////////////////////
			/// \brief The status is in the 1xx series
			//////////////////////////////////////////////////////////////////////////
			INFORMATIONAL = 1,

			//////////////////////////////////////////////////////////////////////////
			/// \brief The status is in the 2xx series
			//////////////////////////////////////////////////////////////////////////
			SUCCESSFUL = 2,

			//////////////////////////////////////////////////////////////////////////
			/// \brief The status is in the 3xx series
			//////////////////////////////////////////////////////////////////////////
			REDIRECTION = 3,

			//////////////////////////////////////////////////////////////////////////
			/// \brief The status is in the 4xx series
			//////////////////////////////////////////////////////////////////////////
			CLIENT_ERROR = 4,

			//////////////////////////////////////////////////////////////////////////
			/// \brief The status is in the 5xx series
			//////////////////////////////////////////////////////////////////////////
			SERVER_ERROR = 5,
		};
		//////////////////////////////////////////////////////////////////////////
		/// \brief Creates a new HttpStatus instance with the specified code and text
		//////////////////////////////////////////////////////////////////////////
		HttpStatus(unsigned short newCode, std::string newText);

		//////////////////////////////////////////////////////////////////////////
		/// \brief Creates a new HttpStatus instance with the specified code, if the code
		/// is recognized sets the text to the correct value, eg, code 200 is "OK",
		/// code 404 is "NOT FOUND", else sets the text to "UNKNOWN"
		//////////////////////////////////////////////////////////////////////////
		HttpStatus(unsigned short newCode);

		//////////////////////////////////////////////////////////////////////////
		/// \brief Returns a number indicating the series the status code is in,
		/// 1 for the 1xx series, 2 for the 2xx series, etc.
		/// Returns 0 if no in any series.
		/// These return values may be used with the Series enum
		//////////////////////////////////////////////////////////////////////////
		unsigned short getSeries();

		//////////////////////////////////////////////////////////////////////////
		/// \brief 200, status indicating the resource was found and the response is included
		//////////////////////////////////////////////////////////////////////////
		static const HttpStatus OK;

		//////////////////////////////////////////////////////////////////////////
		/// \brief 301, This and all future requests for this resource should be directed
		/// to the URI that is included in the response
		//////////////////////////////////////////////////////////////////////////
		static const HttpStatus MOVED_PERMANENTLY;

		//////////////////////////////////////////////////////////////////////////
		/// \brief 303, The response to the RI using the GET method. If received as a
		/// response to a POST/PUT/DELETE request the client can assume the server
		/// accepted the data and should redirect using GET to see a result
		//////////////////////////////////////////////////////////////////////////
		static const HttpStatus SEE_OTHER;

		//////////////////////////////////////////////////////////////////////////
		/// \brief 307, The request should be repeated using another URI, but future request
		/// should continue to use the original URI. The request method must not be changed
		/// when reissuing the original request
		//////////////////////////////////////////////////////////////////////////
		static const HttpStatus TEMPORARY_REDIRECT;

		//////////////////////////////////////////////////////////////////////////
		/// \brief 308, This request and all future requests should be repeated using
		/// another URI, however unlike 301 the request method must not be changed
		//////////////////////////////////////////////////////////////////////////
		static const HttpStatus PERMANENT_REDIRECT;

		//////////////////////////////////////////////////////////////////////////
		/// \brief 400, The server cannot or will not process the request due to something
		/// perceived as a client error, eg, malformed request syntax
		//////////////////////////////////////////////////////////////////////////
		static const HttpStatus BAD_REQUEST;

		//////////////////////////////////////////////////////////////////////////
		/// \brief 401, The client is not allowed to receive the resource as authentication
		/// is required but has not been provided or the provided details are wrong
		//////////////////////////////////////////////////////////////////////////
		static const HttpStatus UNAUTHORIZED;

		//////////////////////////////////////////////////////////////////////////
		/// \brief 403, The server refuses to respond even though the request is valid.
		/// Authentication will make no difference (unlike CLIENT_ERROR_UNAUTHORIZED)
		//////////////////////////////////////////////////////////////////////////
		static const HttpStatus FORBIDDEN;

		//////////////////////////////////////////////////////////////////////////
		/// \brief 404, the resource could not be found but may be available
		/// again in the future
		//////////////////////////////////////////////////////////////////////////
		static const HttpStatus NOT_FOUND;

		//////////////////////////////////////////////////////////////////////////
		/// \brief 500 A generic error message, the sever encountered an unexpected
		/// condition and no more specific message is suitable
		//////////////////////////////////////////////////////////////////////////
		static const HttpStatus INTERNAL_SERVER_ERROR;

		//////////////////////////////////////////////////////////////////////////
		/// \brief 501 The server lacks the ability to fulfill the request, usually
		/// implies future availability, eg, new feature in web service API
		//////////////////////////////////////////////////////////////////////////
		static const HttpStatus NOT_IMPLEMENTED;

		//////////////////////////////////////////////////////////////////////////
		/// \brief 502,The server was acting as a gateway or proxy and received an
		/// invalid response from the upstream server
		//////////////////////////////////////////////////////////////////////////
		static const HttpStatus BAD_GATEWAY;

		//////////////////////////////////////////////////////////////////////////
		/// \brief 503, The server is currently unavailable, eg, down for maintenance
		/// or overloaded. Generally this is a temporary state.
		//////////////////////////////////////////////////////////////////////////
		static const HttpStatus SERVICE_UNAVAILABLE;

		//////////////////////////////////////////////////////////////////////////
		/// \brief 504,The server was acting as a gateway or proxy and did not receive
		/// a timely response from the upstream server
		//////////////////////////////////////////////////////////////////////////
		static const HttpStatus GATEWAY_TIMEOUT;

		///< The status code, eg, 200, 404
		unsigned short code;

		///< Text describing the code, eg, OK, Not Found
		std::string text;
	};
}

#endif