/////////////////////////////////////////////////
///  Part of C++ Web Library - By Jamie Terry ///
/////////////////////////////////////////////////
/// \file Url.hpp
/// \author Jamie Terry
/// \date 2015/08/19
/// \brief Contains decleration of cppwl::Url class
/// \ingroup core
/////////////////////////////////////////////////

#ifndef CPPWL_CORE_URL_HPP
#define CPPWL_CORE_URL_HPP

#include <string>
#include <map>
#include <vector>
#include <lie\core\ExcepParseError.hpp>

namespace cppwl{

	//////////////////////////////////////////////////////////////////////////
	/// \brief Class for representing and manipulating a url such as
	/// http://www.example.com/test/thing.html?blob=1#fragmentIdentifier
	//////////////////////////////////////////////////////////////////////////
	class Url{
	public:
		//////////////////////////////////////////////////////////////////////////
		/// \brief Creates a new default url, with the domain set to "0.0.0.0" and
		/// all other components left empty
		//////////////////////////////////////////////////////////////////////////
		Url();

		//////////////////////////////////////////////////////////////////////////
		/// \brief Creates a new url from a string, parsing it to find the individual
		/// components
		/// \param url String containing url to parse, default to "0.0.0.0"
		/// \throw lie::core::ExcepParseError if the url string is invalid
		//////////////////////////////////////////////////////////////////////////
		Url(std::string url) throw(lie::core::ExcepParseError);

		//////////////////////////////////////////////////////////////////////////
		/// \brief Creates a new url from the supplied components
		/// \throw lie::core::ExcepParseError if the query string could not be parsed
		//////////////////////////////////////////////////////////////////////////
		Url(std::string protocol, std::string domain, std::string resource = "", 
			std::string queryString = "", std::string fragmentIdentifier = "")
			throw (lie::core::ExcepParseError);

		//////////////////////////////////////////////////////////////////////////
		/// \brief Returns the protocol to be used, ie, for 
		/// http://www.example.com/test/thing.html?blob=1#fragmentIdentifier
		/// returns "http"
		//////////////////////////////////////////////////////////////////////////
		const std::string& getProtocol() const;

		//////////////////////////////////////////////////////////////////////////
		/// \brief Returns the domain section of the url, eg for
		/// http://www.example.com/test/thing.html?blob=1#fragmentIdentifier
		/// returns "www.example.com"
		//////////////////////////////////////////////////////////////////////////
		const std::string& getDomain() const;

		//////////////////////////////////////////////////////////////////////////
		/// \brief Returns the resource to be obtained from the server without the leading '/'
		/// eg, for http://www.example.com/test/thing.html?blob=1#fragmentIdentifier
		/// returns "test/thing.html"
		/// \note May or may not have a trailing '/' depending on the URL, eg,
		/// ....com/thing/ would return "thing/", .....com/thing would  return "thing"
		/// \see getResourceComponent
		//////////////////////////////////////////////////////////////////////////
		const std::string getResource() const;

		//////////////////////////////////////////////////////////////////////////
		/// \brief Returns the specified component of the resource string,
		/// components are separated by '/', they are indexed as follows:
		/// example.com/0/1/2/3/4/5
		/// \note Undefined behaviour will occur if index is too large
		//////////////////////////////////////////////////////////////////////////
		const std::string& getResourceComponent(unsigned short index) const;

		//////////////////////////////////////////////////////////////////////////
		/// \brief Returns the number of components in the resource string
		/// Components are separated by '/', they are indexed as follows:
		/// example.com/0/1/2/3/4/5, the maximum index is one less than the value
		/// returned by this function
		//////////////////////////////////////////////////////////////////////////
		unsigned short getResourceComponentCount() const;

		//////////////////////////////////////////////////////////////////////////
		/// \brief Returns the value of the specified query parameter,
		/// eg, for http://www.example.com/test/thing.html?blob=1#fragmentIdentifier
		/// returns "1" for "blob" and empty string for all else
		/// \param The name of the parameter to get the value of
		/// \return string with the value, empty string if parameter does not exist
		//////////////////////////////////////////////////////////////////////////
		const std::string& getQueryParam(std::string paramName) const;

		//////////////////////////////////////////////////////////////////////////
		/// \brief Returns the number of query string parameters
		//////////////////////////////////////////////////////////////////////////
		int getQueryParamCount() const;

		//////////////////////////////////////////////////////////////////////////
		/// \brief Generates and returns a string that represents a query string with
		/// all the parameters in this url, note that the order of the parameters is
		/// not defined even if setQueryString is used, hence calling getQueryString may
		/// not return exactly what was set using setQueryString
		/// \note The returned string DOES NOT include the leading "?"
		//////////////////////////////////////////////////////////////////////////
		const std::string getQueryString() const;

		//////////////////////////////////////////////////////////////////////////
		/// \brief Returns the fragment identifier, ie, the part after the last '#'
		/// also known as an "anchor". Eg, for http://www.example.com/test/thing.html?blob=1#fragmentIdentifier
		/// returns "fragmentIdentifier"
		//////////////////////////////////////////////////////////////////////////
		const std::string& getFragmentIdentifier() const;

		//////////////////////////////////////////////////////////////////////////
		/// \brief Returns true if the specified query parameter exists
		//////////////////////////////////////////////////////////////////////////
		bool hasQueryParam(std::string paramName) const;

		//////////////////////////////////////////////////////////////////////////
		/// \brief Sets the protocol part of the url, does not need trailing "://",
		/// ie, pass "http", not "http://"
		//////////////////////////////////////////////////////////////////////////
		void setProtocol(std::string protocol);

		//////////////////////////////////////////////////////////////////////////
		/// \brief Sets the domain, eg, www.example.com
		//////////////////////////////////////////////////////////////////////////
		void setDomain(std::string domain);

		//////////////////////////////////////////////////////////////////////////
		/// \brief Sets the resource part of the url, ie, all the bits after the first
		/// '/' and before the '?', or'#' or end of string, the passed in value can but
		/// is not required to have a leading '/' character, eg, for 
		/// http://www.example.com/test/thing.html?blob=1#fragmentIdentifier
		/// pass either "/test/thing.html" or "test/thing.html"
		/// \note This completely removes all previous resource components and adds an
		/// appropriate number, depending on the number of '/' in the specified string
		/// \see pushResourceComponent
		/// \see popResourceComponent
		//////////////////////////////////////////////////////////////////////////
		void setResource(std::string resource);

		//////////////////////////////////////////////////////////////////////////
		/// \brief Sets the specified resource component to the specified value
		/// Resource components are indexed as follows:
		/// www.example.com/0/1/2/3/4/5. Only the specified component is modified.
		/// If the index is too large undefined behavior occurs
		//////////////////////////////////////////////////////////////////////////
		void setResourceComponent(unsigned short index, std::string newValue);

		//////////////////////////////////////////////////////////////////////////
		/// \brief Pops the last resource component from the url,
		/// for example www.example.com/test/thing.html would become
		/// www.example.com/test/
		/// \note If there are no resource components (ie: getResourceComponentCount
		/// returns 0) then this function does nothing, no exception is thrown
		//////////////////////////////////////////////////////////////////////////
		void popResourceComponent();

		//////////////////////////////////////////////////////////////////////////
		/// \brief Adds new component(s) on to the end of the resource string,
		/// for example, pushing "hello/world" to www.test.com/thing
		/// would result in www.test.com/thing/hello/world
		/// Any number of components can be pushed at once using this function
		//////////////////////////////////////////////////////////////////////////
		void pushResourceComponent(std::string components);

		//////////////////////////////////////////////////////////////////////////
		/// \brief Sets the value of the specified query parameter, adds a new query parameter
		/// if it does not already exists.
		/// \param paramName the name of the query parameter
		/// \param value The value for the specified parameter, pass empty string to delete the parameter
		//////////////////////////////////////////////////////////////////////////
		void setQueryParam(std::string paramName, std::string value);

		//////////////////////////////////////////////////////////////////////////
		/// \brief Sets the fragment identifer part of the url
		//////////////////////////////////////////////////////////////////////////
		void setFragmentIdentifier(std::string fragId);

		//////////////////////////////////////////////////////////////////////////
		/// \brief Removes all current query parameters from the url and parses the
		/// specified string to find new query parameters, passing an empty string
		/// simply deletes all current query parameters
		/// \param queryString string containing the query parameters, must be formatted
		/// as follows: key1=value1&key2=value2 or ?key1=value1&key2=value2
		/// \throw lie::core::ExcepParseError if the query string is not valid
		//////////////////////////////////////////////////////////////////////////
		void setQueryString(std::string queryString) throw(lie::core::ExcepParseError);

		//////////////////////////////////////////////////////////////////////////
		/// \brief Deletes the specified query parameter, no effect if the parameter
		/// does not exists
		/// \note The following calls are equivalent:
		/// \code
		/// url.unsetQueryParam("param")
		/// url.setQueryParam("param", "")
		/// \endcode
		//////////////////////////////////////////////////////////////////////////
		void unsetQueryParam(std::string paramName);

		//////////////////////////////////////////////////////////////////////////
		/// \brief Parses the specified url, throws lie::core::ExcepParseError if 
		/// the url is not valid
		/// The only required part is the domain name, this may or may not be prefixed
		/// with a protocol (eg: http://) and may or may not be preceded by a document
		/// name (eg: /test/thing.html), query string (?key1=val1&key2=val2) and
		/// fragment identifier (eg: #fragId). The order must be
		/// [document name][querry string][fragment identifer] however each can be
		/// present with or without any of the other two.
		/// \throw lie::core::ExcepParseError if the url is not valid, if an exception
		/// is thrown this Url instance will NOT be modified in any way
		//////////////////////////////////////////////////////////////////////////
		void parse(std::string url) throw(lie::core::ExcepParseError);

		//////////////////////////////////////////////////////////////////////////
		/// \brief Returns a string representation of the url
		//////////////////////////////////////////////////////////////////////////
		operator std::string() const;

		//////////////////////////////////////////////////////////////////////////
		/// \brief Converts the url to a string
		/// \note Url also supports being cast to a string, ie:
		/// \code
		/// url.toString() == (std::string)url
		/// \endcode
		//////////////////////////////////////////////////////////////////////////
		std::string toString() const { return *this; }

		//////////////////////////////////////////////////////////////////////////
		/// \brief Returns true if two URL instances are exactly equal, ie, all parts
		/// (domain, resource, query string, etc) are the same
		//////////////////////////////////////////////////////////////////////////
		bool operator==(const Url& other) const;

		//////////////////////////////////////////////////////////////////////////
		/// \brief Returns true if two URL instances differ in any way
		//////////////////////////////////////////////////////////////////////////
		bool operator!=(const Url& other) const { return !((*this) == other); }

		//////////////////////////////////////////////////////////////////////////
		/// \brief Makes the LHS Url instance equal the RHS url instance, hence the LHS
		/// becomes a deep copy of the RHS, does not modify the RHS instance
		//////////////////////////////////////////////////////////////////////////
		Url& operator=(const Url& other);
	private:
		///< The url is stored in the following components
		///< [mProtocol]://[mDomain][mResource]?[mQueryParams]#[mFragmentIdentifier]

		std::string mProtocol;
		std::string mDomain;
		std::vector<std::string> mResourceComponents;
		bool mResourceHasTrailingSlash;
		std::map<std::string, std::string> mQueryParams;
		std::string mFragmentIdentifier;
	};
}

#endif