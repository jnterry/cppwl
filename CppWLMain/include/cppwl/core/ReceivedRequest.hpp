/////////////////////////////////////////////////
///  Part of C++ Web Library - By Jamie Terry ///
/////////////////////////////////////////////////
/// \file ReceivedRequest.hpp
/// \author Jamie Terry
/// \date 2015/08/19
/// \brief Contains deceleration of the ReceivedRequest class
/// \ingroup core
/////////////////////////////////////////////////

#ifndef CPPWL_CORE_RECEIVEDREQUEST_HPP
#define CPPWL_CORE_RECEIVEDREQUEST_HPP

#include "Request.hpp"
#include "..\core\Socket.hpp"

namespace cppwl{
	class Response;

	//////////////////////////////////////////////////////////////////////////
	/// \brief Pure virtual class representing a HTTP request received from a client, 
	/// contains functionality for querying the request. This has additional data 
	/// compared to Request as a Request has not necessarily yet been sent let alone received
	/// by some server, however a ReceivedRequest has been received by some server,
	/// and hence contains additional information, for example, the client and server IP.
	/// The server is also able to set arbitrary "environment variables", these can be
	/// queried.
	/// This class also has a pure virtual method for sending a response to the client,
	/// the implementation of this method depends on how the request was received, for example
	/// via fast cgi, a http server implementation, etc
	//////////////////////////////////////////////////////////////////////////
	class ReceivedRequest : public Request{
	public:
		typedef std::map<std::string, std::string> EnvVarMapType;
		typedef EnvVarMapType::iterator iterator;
		typedef EnvVarMapType::reverse_iterator reverse_iterator;
		typedef EnvVarMapType::const_iterator const_iterator;
		typedef EnvVarMapType::const_reverse_iterator const_reverse_iterator;

		//////////////////////////////////////////////////////////////////////////
		/// \brief Creates a new Request with the specified values, unspecified values
		/// are set to defaults
		//////////////////////////////////////////////////////////////////////////
		ReceivedRequest(HttpMethod method, const Url& url);

		//////////////////////////////////////////////////////////////////////////
		/// \brief Sends a response to the client that made the request, virtual method 
		/// as depending on the source of the request (cgi, fcgi, http server, etc) the 
		/// code to respond is different
		//////////////////////////////////////////////////////////////////////////
		virtual void respond(const Response& response) = 0;

		//////////////////////////////////////////////////////////////////////////
		/// \brief Returns the socket that the web server is listening for requests on
		//////////////////////////////////////////////////////////////////////////
		const Socket& getServerSocket() const;

		//////////////////////////////////////////////////////////////////////////
		/// \brief Returns the socket that was used by the client to make the HTTP request
		//////////////////////////////////////////////////////////////////////////
		const Socket& getClientSocket() const;

		//////////////////////////////////////////////////////////////////////////
		/// \brief Sets the socket the server is using to listen for http requests
		/// and which received this request
		//////////////////////////////////////////////////////////////////////////
		void setServerSocket(const Socket& socket);

		//////////////////////////////////////////////////////////////////////////
		/// \brief Sets the socket that the client used to send the request
		//////////////////////////////////////////////////////////////////////////
		void setClientSocket(const Socket& socket);

		//////////////////////////////////////////////////////////////////////////
		/// \brief Retrieves the value of the named environmental variable,
		/// \return The value of the variable or empty string if it is not set
		//////////////////////////////////////////////////////////////////////////
		const std::string& getEnviromentVariable(std::string varName) const;

		//////////////////////////////////////////////////////////////////////////
		/// \brief Returns the number of the environment variables that the server
		/// has set
		//////////////////////////////////////////////////////////////////////////
		size_t getEnviromentVariableCount() const;

		//////////////////////////////////////////////////////////////////////////
		/// \brief Sets the value of the specified environmental variable, adding
		/// and entry to the variable map if varName does not already exist
		/// as a key
		/// \param varName The name of environment variable to change/create
		/// \param value The new value, if empty string is passed deletes
		/// the environment variable if it exists, else does nothing
		//////////////////////////////////////////////////////////////////////////
		void setEnviromentVariable(std::string varName, std::string value);

		//////////////////////////////////////////////////////////////////////////
		/// \brief Deletes the specified environment variable
		/// \note If no such variable exists does nothing
		//////////////////////////////////////////////////////////////////////////
		void unsetEnviromentVariable(std::string varName);

		//////////////////////////////////////////////////////////////////////////
		/// \brief Erases all environment variables in this request
		//////////////////////////////////////////////////////////////////////////
		void clearEnviromentVariables();

		//////////////////////////////////////////////////////////////////////////
		/// \brief Returns iterator to the beginning of the environment variable map
		//////////////////////////////////////////////////////////////////////////
		iterator begin() { return mEnvVars.begin();  }

		//////////////////////////////////////////////////////////////////////////
		/// \brief Returns iterator to the end the environment variable map
		//////////////////////////////////////////////////////////////////////////
		iterator end() { return mEnvVars.end();  }

		//////////////////////////////////////////////////////////////////////////
		/// \brief Returns reverse_iterator to the reverse beginning of the environment variable map
		//////////////////////////////////////////////////////////////////////////
		reverse_iterator rbegin() { return mEnvVars.rbegin();  }

		//////////////////////////////////////////////////////////////////////////
		/// \brief Returns reverse_iterator to the reverse end of the environment variable map
		//////////////////////////////////////////////////////////////////////////
		reverse_iterator rend() { return mEnvVars.rend();  }

		//////////////////////////////////////////////////////////////////////////
		/// \brief Returns const_iterator to the beginning of the environment variable map
		//////////////////////////////////////////////////////////////////////////
		const_iterator cbegin() const { return mEnvVars.cbegin(); }

		//////////////////////////////////////////////////////////////////////////
		/// \brief Returns const_iterator to the end of the environment variable map
		//////////////////////////////////////////////////////////////////////////
		const_iterator cend() const { return mEnvVars.cend();  }

		//////////////////////////////////////////////////////////////////////////
		/// \brief Returns const_reverse_iterator to the reverse beginning of the environment variable map
		//////////////////////////////////////////////////////////////////////////
		const_reverse_iterator crbegin() const { return mEnvVars.crbegin(); }

		//////////////////////////////////////////////////////////////////////////
		/// \brief Returns const_reverse_iterator to the reverse end of the environment variable map
		//////////////////////////////////////////////////////////////////////////
		const_reverse_iterator crend() const{ return mEnvVars.crend(); }


	private:
		///< The socket used by the http server to listen for requests
		Socket mServerSocket;

		///< The socket used by the client to make the http request
		Socket mClientSocket;

		EnvVarMapType mEnvVars;
	};
}

#endif