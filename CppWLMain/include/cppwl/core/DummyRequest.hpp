/////////////////////////////////////////////////
///  Part of C++ Web Library - By Jamie Terry ///
/////////////////////////////////////////////////
/// \file D:\Dev\C++\Current Projects\CppWL\CppWLMain\include\cppwl\core\DummyRequest.hpp.hpp
/// \author Jamie Terry
/// \date 2015/09/15
/// \brief Contains cppwl::DummyRequest class
/// \ingroup core
/////////////////////////////////////////////////

#ifndef CPPWL_CORE_DUMMYREQUEST_HPP
#define CPPWL_CORE_DUMMYREQUEST_HPP

#include "ReceivedRequest.hpp"

namespace cppwl{

	//////////////////////////////////////////////////////////////////////////
	/// \brief A dummy implementation of ReceivedRequest whose respond method is
	/// overloaded as a no-op. Designed to be used for testing without requiring
	/// an actually http server, cgi socket, etc
	//////////////////////////////////////////////////////////////////////////
	class DummyRequest : public ReceivedRequest{
	public:
		//////////////////////////////////////////////////////////////////////////
		/// \brief Constructs a new DummyRequest for the specified url using the specified
		/// method
		/// \param newMethod The HTTP method this request is for
		/// \param newUrl The url this request is for
		//////////////////////////////////////////////////////////////////////////
		DummyRequest(cppwl::HttpMethod newMethod, const cppwl::Url& newUrl)
			: ReceivedRequest(newMethod, newUrl){
			//empty body
		}

		//////////////////////////////////////////////////////////////////////////
		/// \brief Sends response to the client that made the request.
		/// Since this is a DummyRequest does nothing
		/// \param the response to reply with
		//////////////////////////////////////////////////////////////////////////
		void respond(const cppwl::Response& res){}
	};
}

#endif