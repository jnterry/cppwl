/////////////////////////////////////////////////
///  Part of C++ Web Libary - By Jamie Terry  ///
/////////////////////////////////////////////////
/// \file IPv4.hpp
/// \author Jamie Terry
/// \date 2015/07/24
/// \brief Contains decleration of the cppwl::IPv4 class
/// \ingroup core
/////////////////////////////////////////////////

#ifndef CPPWL_CORE_IPV4_HPP
#define CPPWL_CORE_IPV4_HPP

#include <stdint.h>
#include <string>
#include <lie\core\ExcepParseError.hpp>

namespace cppwl{

	//////////////////////////////////////////////////////////////////////////
	/// \brief Class representing an ip v4 address
	//////////////////////////////////////////////////////////////////////////
	class IPv4{
	public:
		///< IPv4 instance representing the ip of localhost, ie: 127.0.0.1
		static const IPv4 LOCALHOST;

		//////////////////////////////////////////////////////////////////////////
		/// \brief Default constructor, returns a new IPv4 instance with all 
		/// components initialized to 0, ie: 0.0.0.0
		//////////////////////////////////////////////////////////////////////////
		IPv4();

		//////////////////////////////////////////////////////////////////////////
		/// \brief Creates a new IPv4 instance from 4 components between 0 and 255
		/// Order of components is as listed, ie: comp0.comp1.comp2.comp3
		////////////////////////////////////////////////////////////////////////// 
		IPv4(uint8_t comp0, uint8_t comp1, uint8_t comp2, uint8_t comp3);

		//////////////////////////////////////////////////////////////////////////
		/// \brief Parses an IPv4 address from a string formatted as 255.255.255.255
		/// or 255.255.255.255:8000, note that if the port number is included this 
		/// information will be lost once parsed
		/// \throw If the string could not be parsed throws a lie::core::ExcepParseError
		//////////////////////////////////////////////////////////////////////////
		IPv4(std::string value) throw(lie::core::ExcepParseError);

		//////////////////////////////////////////////////////////////////////////
		/// \brief Creates an IPv4 instance from a 32 bit unsigned integer where the bits
		/// store the 4 8 bit components 0-3 as follows:
		/// 3333 3333 2222 2222 1111 1111 0000 0000
		//////////////////////////////////////////////////////////////////////////
		IPv4(uint32_t value);

		//////////////////////////////////////////////////////////////////////////
		/// \brief Returns the specified component of this IP address
		/// \param component uint between 0 and 3 representing the component to retrieve,
		/// components labeled as follows : 000.001.002.003
		/// \note if component is out of range (0-3 inclusive) undefined behavoir
		/// will occur
		//////////////////////////////////////////////////////////////////////////
		uint8_t getComponent(uint8_t component) const;

		//////////////////////////////////////////////////////////////////////////
		/// \brief Sets the value of the specified component
		/// \param component uint between 0 and 3 representing the component to modify,
		/// components labeled as follows : 000.001.002.003
		/// \param value The new value for the specified component
		/// \return reference to this instance for operator chaining
		/// \note If component is out of range (0-3 inclusive) undefined behavior
		/// will occur
		//////////////////////////////////////////////////////////////////////////
		IPv4& setComponent(uint8_t component, uint8_t value);
		
		//////////////////////////////////////////////////////////////////////////
		/// \brief Sets each component of this IPv4 to the specified value,
		/// Order of components is as listed, ie: comp0.comp1.comp2.comp3
		/// \return reference to this IPv4 for operator chaining
		//////////////////////////////////////////////////////////////////////////
		IPv4& set(uint8_t comp0, uint8_t comp1, uint8_t comp2, uint8_t comp3);

		//////////////////////////////////////////////////////////////////////////
		/// \brief Parses the specified string and modifies this IPv4 instance, returns
		/// false if the parse failed, under these circumstances this instance will
		/// NOT be modified
		/// \param value The string to parse, may be formatted as 255.255.255.255 
		/// or 255.255.255.255:80 (ie, including a port number), note that the 
		/// 3 digit components may be shorter, ie, both 127.0.0.1 and 127.000.000.001 are valid
		/// \throw Instance of lie::core::ExcepParaseError if the passed string "value"
		/// is not a valid ip address
		/// \return reference to this object for operator chaining
		//////////////////////////////////////////////////////////////////////////
		IPv4& parse(std::string value) throw(lie::core::ExcepParseError);

		//////////////////////////////////////////////////////////////////////////
		/// \brief Returns a traditional string representation of this IPv4 instance,
		/// eg, 255.255.255.255, 
		/// \note components that can be represented using 2 or 1 digit will be,
		/// eg, 127.0.0.1 will be returned rather than 127.000.000.001
		//////////////////////////////////////////////////////////////////////////
		std::string toString() const;

		//////////////////////////////////////////////////////////////////////////
		/// \brief Returns a single unsigned 32 bit integer that represents the ipv4 address,
		/// the bits store the components 0-3 as follows:
		/// 3333 3333 2222 2222 1111 1111 0000 0000
		/// \note This could be useful for storing ips in a database/file etc as a single
		/// 32 bit number is stored rather than a string or 4 8 bit components
		//////////////////////////////////////////////////////////////////////////
		uint32_t toInt() const;

		//////////////////////////////////////////////////////////////////////////
		/// \brief Sets the values of each component of this IPv4 from a 32 bit unsigned integer,
		/// the 32 bits store the 8 bit components 0-3 as follows:
		/// 3333 3333 2222 2222 1111 1111 0000 0000
		/// \param value the 32 bit unsigned integer to use to set the components of this IPv4 instance
		/// \note This should be used to recreate an IPv4 from an integer produced using asInt
		/// \return Reference to this instance for operator chaining
		//////////////////////////////////////////////////////////////////////////
		IPv4& fromInt(uint32_t value);

		//////////////////////////////////////////////////////////////////////////
		/// \brief Sets all the components of this IPv4 equal to the components of the other
		//////////////////////////////////////////////////////////////////////////
		IPv4& operator=(const IPv4 other);

		//////////////////////////////////////////////////////////////////////////
		/// \brief Returns true if two IPv4 instances represent the exact same address,
		/// else returns false
		//////////////////////////////////////////////////////////////////////////
		bool operator==(const IPv4& other) const;

		//////////////////////////////////////////////////////////////////////////
		/// \brief Returns true if two IPv4 instances represent different addresses,
		/// else returns false
		//////////////////////////////////////////////////////////////////////////
		bool operator!=(const IPv4& other) const;
	private:
		///< 32 bit unsigned int type for storing the address
		uint32_t mIP;

	};
}

#endif