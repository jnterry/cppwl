/////////////////////////////////////////////////
///  Part of C++ Web Library - By Jamie Terry ///
/////////////////////////////////////////////////
/// \file Socket.hpp
/// \author Jamie Terry
/// \date 2015/08/19
/// \brief Contains the Socket struct
/// \ingroup core
/////////////////////////////////////////////////

#ifndef CPPWL_CORE_SOCKET_HPP
#define CPPWL_CORE_SOCKET_HPP

#include "IPv4.hpp"
#include <stdint.h>

namespace cppwl{

	//////////////////////////////////////////////////////////////////////////
	/// \brief Stores information about a socket, this is a combination of an
	/// ip address and a port number
	//////////////////////////////////////////////////////////////////////////
	struct Socket{
		IPv4 ip;
		uint16_t port;

		//////////////////////////////////////////////////////////////////////////
		/// \brief Creates a new socket instance representing the socket 0.0.0.0:0
		//////////////////////////////////////////////////////////////////////////
		Socket() : Socket(IPv4(0, 0, 0, 0), 0){}

		//////////////////////////////////////////////////////////////////////////
		/// \brief Creates a socket instance from an ipv4 and a port number
		//////////////////////////////////////////////////////////////////////////
		Socket(IPv4 ip, uint16_t port);

		//////////////////////////////////////////////////////////////////////////
		/// \brief Copy constructor, clones an existing Socket instance
		//////////////////////////////////////////////////////////////////////////
		Socket(const Socket& socket);

		//////////////////////////////////////////////////////////////////////////
		/// \brief Constructs a new Socket instance by parsing the specified string
		//////////////////////////////////////////////////////////////////////////
		Socket(std::string value) throw (lie::core::ExcepParseError);

		//////////////////////////////////////////////////////////////////////////
		/// \brief Casts the Socket to a string, equivilent to .toString()
		//////////////////////////////////////////////////////////////////////////
		operator std::string() const{ return this->toString(); }

		//////////////////////////////////////////////////////////////////////////
		/// \brief Represents this socket as a string, final string will be in the format
		/// xxx.xxx.xxx.xxx:yyyyy, ie: [ip-address]:[port number]
		/// \return std::string representing the socket
		//////////////////////////////////////////////////////////////////////////
		std::string toString() const;

		//////////////////////////////////////////////////////////////////////////
		/// \brief Returns true if two Socket instances represent the same socket, ie,
		/// both the ip and port number are the same
		//////////////////////////////////////////////////////////////////////////
		bool operator==(const Socket& other) const;

		//////////////////////////////////////////////////////////////////////////
		/// \brief Returns true if two Socket instances do not represent the same socket,
		/// ie, either the ip or port numbers or both are different
		//////////////////////////////////////////////////////////////////////////
		bool operator!=(const Socket& other) const;

		//////////////////////////////////////////////////////////////////////////
		/// \brief Assigns the ip and port of some other socket instance to this instance
		/// \return Reference to this instance for operator chaining
		//////////////////////////////////////////////////////////////////////////
		Socket& operator=(const Socket& other);

		//////////////////////////////////////////////////////////////////////////
		/// \brief Parses a string with the format 0.0.0.0:port
		///
		//////////////////////////////////////////////////////////////////////////
		void parse(std::string value) throw (lie::core::ExcepParseError);
	};
}

#endif