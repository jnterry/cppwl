/////////////////////////////////////////////////
///  Part of C++ Web Library - By Jamie Terry ///
/////////////////////////////////////////////////
/// \file RouteClassMethod.hpp
/// \author Jamie Terry
/// \date 2015/08/27
/// \brief Contains the RouteClassMethod class
/// \ingroup route
/////////////////////////////////////////////////

#ifndef CPPWL_ROUTE_ROUTECLASSMETHOD_HPP
#define CPPWL_ROUTE_ROUTECLASSMETHOD_HPP

namespace cppwl{

	//////////////////////////////////////////////////////////////////////////
	/// \brief Template class derived from Route that will call a non-static
	/// member function of the class T_CLASS each time the route is used. The
	/// route can either reuse a specified instance of T_CLASS each time it is
	/// used or automatically construct an instance (provided the class has
	/// a constructor that takes no parameters)
	//////////////////////////////////////////////////////////////////////////
	template<typename T_CLASS>
	class RouteClassMethod : public Route{
	public:
		///< The type of the member function pointer
		typedef Response&(T_CLASS::*RouteFunc)(const ReceivedRequest&);

		//////////////////////////////////////////////////////////////////////////
		/// \brief Creates a new instance of RouteClassMethod
		/// \param methods The set of HttpMethods that this route responds to
		/// \param newFunc Pointer to a member function of the class T_CLASS that will
		/// be called each time the route is used
		/// \param instance Pointer to an instance of T_CLASS, defaults to nullptr
		/// If set to nullptr then each time go() is called a new instnace of the class
		/// will be constructed using the default constructor, it will then be deleted
		/// at the end of the go() method. If set to a pointer to an instance of the class
		/// then the instance will be reused each time the go() method is called. NOTE 
		/// THAT THE INSTANCE WILL BE DELETED IN THE DESTRUCTOR OF THIS CLASS.
		//////////////////////////////////////////////////////////////////////////
		RouteClassMethod(HttpMethodType methods, RouteFunc func, T_CLASS* instance = nullptr)
			: Route(methods), mFunc(func), mInstance(instance){
			//empty body
		}

		//////////////////////////////////////////////////////////////////////////
		/// \brief Destructor, 
		//////////////////////////////////////////////////////////////////////////
		~RouteClassMethod(){
			if (this->mInstance != nullptr){
				delete this->mInstance;
			}
		}

		Response& go(const ReceivedRequest& request){
			if (mInstance == nullptr){
				T_CLASS instance;
				return ((instance).*(mFunc))(request);
			} else {
				return ((mInstance)->*(mFunc))(request);
			}
		}

	private:
		RouteFunc mFunc;
		T_CLASS* mInstance;
	};
}

#endif