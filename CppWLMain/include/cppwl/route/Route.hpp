/////////////////////////////////////////////////
///  Part of C++ Web Library - By Jamie Terry ///
/////////////////////////////////////////////////
/// \file Route.hpp
/// \author Jamie Terry
/// \date 2015/08/27
/// \brief Contains the Route class
/// \ingroup route
/////////////////////////////////////////////////

#ifndef CPPWL_ROUTE_ROUTE_HPP
#define CPPWL_ROUTE_ROUTE_HPP

#include "..\core\HttpMethod.hpp"

namespace cppwl{

	//forward deceleration
	class Response;
	class ReceivedRequest;

	//////////////////////////////////////////////////////////////////////////
	/// \brief Virtual internally used class for representing a registered route
	//////////////////////////////////////////////////////////////////////////
	class Route{
		friend class Router;
	public:
		//////////////////////////////////////////////////////////////////////////
		/// \brief Creates a new Route instance that will respond to the specified
		/// http methods
		/// \param methods Bit flag set of the HttpMethod enum constants that this
		/// route will respond to
		//////////////////////////////////////////////////////////////////////////
		Route(HttpMethodType methods);

		//////////////////////////////////////////////////////////////////////////
		/// \brief Virtual method that should be overridden to provide the functionality
		/// for executing the route, this function is given the ReceivedRequest and should
		/// produce a Response that will be returned to the client
		//////////////////////////////////////////////////////////////////////////
		virtual Response& go(const ReceivedRequest&) = 0;

		//////////////////////////////////////////////////////////////////////////
		/// \brief Returns the bit flag set of the HttpMethods that this route
		/// responds to
		//////////////////////////////////////////////////////////////////////////
		HttpMethodType getMethods();
	private:
		///< Bit set of the HttpMethods that this route should respond to
		HttpMethodType mMethods;

		///< the id of the route, unique for a Router instance, the first Route added
		///< to a router will be given the id 0 and each subsequent root will
		///< have the subsequent id, when dispatching a request the route with the
		///< lowest id  that accepts the Request will be used, this allows
		///< routes such as /users/register/ and /users/* to both be registered,
		///< the register route has a lower id so will be used for a request
		///< to /users/register/, where as /users/some_user_name will be passed
		///< to the users/* route
		int mId;
	};
}

#endif