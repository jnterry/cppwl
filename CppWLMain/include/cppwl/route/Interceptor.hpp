/////////////////////////////////////////////////
///  Part of C++ Web Library - By Jamie Terry ///
/////////////////////////////////////////////////
/// \file Interceptor.hpp
/// \author Jamie Terry
/// \date 2015/08/23
/// \brief Contains the cppwl::Interceptor class
/// \ingroup route
/////////////////////////////////////////////////

#ifndef CPPWL_ROUTE_INTERCEPTOR_HPP
#define CPPWL_ROUTE_INTERCEPTOR_HPP

#include "..\core\Response.hpp"
#include "..\core\ReceivedRequest.hpp"

namespace cppwl{

	//Forward decelerations
	class Router;

	//////////////////////////////////////////////////////////////////////////
	/// \brief Interceptor is a virtual class that can be derived from to
	/// create classes that are able to intercept both ReceivedRequest instances
	/// entering  your application and Response instances leaving it. They can be seen 
	/// as layers around your application logic, each layer is able to examine a Request
	/// and/or Response and determine whether it can continue to the next layer,
	/// for example, an Interceptor may check if the user is authenticated
	/// before passing the request on. Another usage may be checking if the website
	/// is down for maintenance, if it is the RecievedRequest is not allowed
	/// to propagate into the application and a difference Response is returned,
	/// indicating to the user that the website is temporarily unavailable.
	///
	/// Interceptors are to be used with a Router instance, they can be applied
	/// globally to all ReceivedRequests or only to certain routes.
	///
	/// Interceptors make use of standard function calling, in the Intercept
	/// method they should call "next()" to pass the request on the to the
	/// next Interceptor, this function will then return the Response
	/// that is about to be sent to the client, the Interceptor can
	/// immediately return this response or modify/examine it in some way
	/// For example, an Interceptor to check the user is authenticated might
	/// be implemented as follows:
	/// \code
	/// Response& intercept(const ReceivedRequest& request){
	///		if(User::isAuthenticated()){
	///			return this->next(request);
	///     } else {
	///			return Response(HttpStatus::UNAUTHORIZED);
	///     }
	/// }
	/// \endcode
	/// Processing may also be done after the request has been processed by
	/// subsequent Interceptors and the application logic and a Response object
	/// has been produced, for example, the following code will
	/// redirect the user to a login page if an Interceptor lower in the
	/// stack has returned an unauthorized response
	/// \code
	/// Response& intercept(const ReceivedRequest& request){
	///     //no processing done before passing request to next layer
	///		Response& r = next(request);
	///     if(r.getStatus().code == Http::Status::UNAUTHORIZED.code){
	///			return redirect("/login");
	///     } else {
	///			return r;
	///		}
	/// }
	/// \endcode
	///
	/// \note An Interceptor instance should only be registered with a single Router
	/// or with multiple Route instances registered with the same Router, failing to do 
	/// this will cause the router() method's return value to be incorrect
	//////////////////////////////////////////////////////////////////////////
	class Interceptor{
		friend class Router;
	public:
		//////////////////////////////////////////////////////////////////////////
		/// \brief Creates a new Interceptor instance
		//////////////////////////////////////////////////////////////////////////
		Interceptor();

		virtual ~Interceptor(){}

		//////////////////////////////////////////////////////////////////////////
		/// \brief This method must be overridden by derived classes and should perform
		/// all the processing done by this Interceptor, this method should call
		/// next() in order to pass the RecievedRequest on to subsequent layers
		/// and the application logic. You may obtain a pointer to the Router currently
		/// routing by calling getRouter(), this may be for redirecting the user
		/// as you can call Router::generateUrl, hence if a Route's url changes
		/// the redirect code does not need to be updated
		//////////////////////////////////////////////////////////////////////////
		virtual Response& intercept(const ReceivedRequest& request) = 0;
	protected:
		//////////////////////////////////////////////////////////////////////////
		/// \brief Function to be called by derived classes in the intercept method.
		/// When called the request is passed on the next layer in the Interceptor
		/// stack, this and subsequent layers will process the request until either
		/// the request is processed by the application or an Interceptor returns a
		/// Response, this Response is then passed back up the Interceptor stack until
		/// it leaves the application and is returned from the Router's dispatch call
		//////////////////////////////////////////////////////////////////////////
		Response& next(const ReceivedRequest& request);

		//////////////////////////////////////////////////////////////////////////
		/// \brief Returns the router currently processing the request, this Interceptor
		/// will have either been registered with the router or with a route registered
		/// with the router. 
		/// \note Interceptor instances should never be registered with multiple 
		/// Router instances or with Route instances registered with multiple
		/// Router instances
		/// \note If this interceptor has not been registered with a Router
		/// will return nullptr, this method is designed to be called in the intercept
		/// method, this method is designed to be called by a Router, hence if this class
		/// is used as intended this function will never return nullptr
		//////////////////////////////////////////////////////////////////////////
		Router* getRouter();
	private:
		///< The next interceptor in the stack
		Interceptor* mNext;

		///< The router that this Interceptor has been registered with
		Router* mRouter;
	};
}

#endif