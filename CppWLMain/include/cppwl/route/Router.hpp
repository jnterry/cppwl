/////////////////////////////////////////////////
///  Part of C++ Web Library - By Jamie Terry ///
/////////////////////////////////////////////////
/// \file Router.hpp
/// \author Jamie Terry
/// \date 2015/08/22
/// \brief Contains the Router class
/// \ingroup route
/////////////////////////////////////////////////

#ifndef CPPWL_ROUTE_ROUTER_HPP
#define CPPWL_ROUTE_ROUTER_HPP

#include <functional>
#include <regex>
#include "..\core\Response.hpp"
#include "..\core\ReceivedRequest.hpp"
#include "Interceptor.hpp"
#include "ExcepDuplicateRoute.hpp"
#include "Route.hpp"

namespace cppwl{
	//////////////////////////////////////////////////////////////////////////
	/// \brief The router is used to determine which code to run given a 
	/// ReceivedRequest, it has "routes" registered (which is simply
	/// a function that given a ReceivedRequest produces a Response)
	/// and can then dispatch a ReceivedRequeest to the appropriate
	/// route by examining the URL and HttpMethod of the request.
	//////////////////////////////////////////////////////////////////////////
	class Router : public Interceptor{
	private:		
		//////////////////////////////////////////////////////////////////////////
		/// \brief Internally used struct, represents a node in the routing tree
		//////////////////////////////////////////////////////////////////////////
		struct RouteTreeNode;
	public:
		Router();

		~Router();

		//////////////////////////////////////////////////////////////////////////
		/// \brief Registers an Interceptor to be used for any request that is passed
		/// to dispatch, the order that the interceptors are registered in is the order
		/// that they will be processed in.
		/// For example, registering in this order:
		/// - InterceptorCheckDownForMaintenance
		/// - InterceptorCheckCsrfToken
		/// will process as follows:
		/// \code
		/// InterceptorCheckDownForMaintenance
		///		InterceptorCheckCsrfToken
		///			[route specific interceptors]
		///				[registered route code]
		///			[route specific interceptors]
		///    InterceptorCheckCsrfToken
		/// InterceptorCheckDownForMaintenance
		/// \endcode
		//////////////////////////////////////////////////////////////////////////
		void registerInterceptor(Interceptor* interceptor);

		//////////////////////////////////////////////////////////////////////////
		/// \brief Registers a new Route with this Router
		/// \param urlMatcher std::string describing the set of Urls the route should be
		/// used for. This string has the same syntax as the resource section of a URL, 
		/// for example, passing /app/dashboard/ will match the url www.webstite.com/app/dashboard/
		/// You can also use wildcards with {}, for example
		/// /user/{Integer}/ will match the urls /user/1/, /user/2/, etc.
		/// Multiple wildcards can be used in a single url matcher.
		/// The wildcards you can use are:
		///  - {Integer} - a negative or positive integer value (note, may be larger than can be stored by int or long)
		///  - {+Integer} - a positive integer value
		///  - {-Integer} - a negative integer value
		///  - {*} - a single url component containing any value
		///  - {**} - any number of components (including 0) that contain anything, this must appear at the end
		/// Anything else inside a pair of curly braces will be interpreted as a custom
		/// regex for matching the url component.
		/// \throw cppwl::ExcepDuplicateRoute if another route has already been registered
		/// with the same urlMathcer and which responds to at least one of the same HttpMethods
		/// \throw std::regex_error if the urlMatcher contains a custom regex which contains
		/// a syntax error
		//////////////////////////////////////////////////////////////////////////
		void registerRoute(std::string urlMatcher, Route* route)
			throw(cppwl::ExcepDuplicateRoute, std::regex_error);

		//////////////////////////////////////////////////////////////////////////
		/// \brief Sets the response to be returned if dispatch is called with a
		/// Request that no registered Route accepts.
		/// \note This Router takes ownership of the specified Response and will
		/// delete it in its destructor, the caller of this method should not keep
		/// a pointer to the Response instance passed to this Router
		//////////////////////////////////////////////////////////////////////////
		void setNotFoundResponse(Response& response);

		//////////////////////////////////////////////////////////////////////////
		/// \brief Returns reference to the Response that is returned if a Request is
		/// passed to dispatch that no registered route responds to.
		/// The Response can be modified using this reference
		//////////////////////////////////////////////////////////////////////////
		Response& getNotFoundResponse();

		//////////////////////////////////////////////////////////////////////////
		/// \brief If set to true and an exception is thrown while processing a
		/// request in the dispatch() method details about the exception will be added 
		/// to the response, this is helpful while building an application as errors will
		/// be displayed in the web browser, but could be a security concern for an
		/// application in production.
		//////////////////////////////////////////////////////////////////////////
		void setDisplayErrors(bool displayErrors);

		//////////////////////////////////////////////////////////////////////////
		/// \brief Returns true if this Router will add error information to
		/// Response instances returned by dispatch.
		/// \see setDisplayErrors
		//////////////////////////////////////////////////////////////////////////
		bool getDisplayErrors();

		//////////////////////////////////////////////////////////////////////////
		/// \brief This function should be called for each request and allows
		/// the 
		//////////////////////////////////////////////////////////////////////////
		Response& dispatch(const ReceivedRequest& request);

		//////////////////////////////////////////////////////////////////////////
		/// \brief Generates the URL for the specified named route
		/// \param routeName The name of the route
		/// \return Url instance representing the URL that would access the specified
		/// route or Url() if no such named route exists
		//////////////////////////////////////////////////////////////////////////
		template<typename... T>
		Url generateUrl(std::string routeName, T... args){}

		//////////////////////////////////////////////////////////////////////////
		/// \brief Generates the URL for the specified route
		/// \param route The route that
		//////////////////////////////////////////////////////////////////////////
		template<typename... T>
		Url generateUrl(Route& route, T... args){}
	protected:
		//////////////////////////////////////////////////////////////////////////
		/// \brief The Router is the last Interceptor in the stack, this method
		/// decides which route the request should be passed to
		//////////////////////////////////////////////////////////////////////////
		Response& intercept(const ReceivedRequest& request);
	private:
		///< Pointer to the head of the linked list of Interceptor instances
		///< that are registered globally with this Router
		Interceptor* mFirstGlobalInterceptor;

		///< Pointer to the tail of the linked list of Interceptor instances
		///< that are registered globally with this Router
		Interceptor* mLastGlobalInterceptor;

		///< If true and an error (exception is thrown) in .dispatch details
		///< are included with the returned Response
		bool mDisplayErrors;

		///< The Response returned if there is not route registered that will respond
		///< to some request
		Response* mNotFoundResponse;

		///< The id that will be given to the next route added to the router
		int mNextRouteId;

		///< The root route, sent as a Response to a request to '/', all other roots
		///< are registered as sub routes of this
		RouteTreeNode* mRootRoute;
	};
}

#endif