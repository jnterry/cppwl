/////////////////////////////////////////////////
///  Part of C++ Web Library - By Jamie Terry ///
/////////////////////////////////////////////////
/// \file ExcepDuplicateRoute.hpp
/// \author Jamie Terry
/// \date 2015/08/26
/// \brief Contains decleration of the cppwl::ExcepDuplicateRoute exception
/// \ingroup route
/////////////////////////////////////////////////

#ifndef CPPWL_ROUTE_EXCEPDUPLICATEROUTE_HPP
#define CPPWL_ROUTE_EXCEPDUPLICATEROUTE_HPP

#include <string>
#include <lie\core\Exception.hpp>
#include "..\core\HttpMethod.hpp"

namespace cppwl{

	//////////////////////////////////////////////////////////////////////////
	/// \brief Exception class that is thrown when two routes are added to a
	/// Router for the same URL that both respond to the same HttpMethod
	//////////////////////////////////////////////////////////////////////////
	class ExcepDuplicateRoute : public lie::core::Exception{
	public:
		//////////////////////////////////////////////////////////////////////////
		/// \brief Creates a new ExcepDuplicateRoot
		/// \param function The name of the function throwing the exception
		/// \param urlMathcer The url matcher of the root being added
		/// \param previousRouteMethdods bit flag set of the HttpMethod the previously 
		/// registered route responds to
		/// \param newRouteMethods bit flag set of the HttpMethods the route being registered
		/// responds to
		//////////////////////////////////////////////////////////////////////////
		ExcepDuplicateRoute(std::string function, std::string urlMatcher, HttpMethodType previousRouteMethods, 
			HttpMethodType newRouteMethods, lie::core::Logger& logger = lie::core::DLog);

		//////////////////////////////////////////////////////////////////////////
		/// \brief Returns a bit set of the HttpMethods the existing route responds to
		//////////////////////////////////////////////////////////////////////////
		HttpMethodType getExistingRouteMethods();

		//////////////////////////////////////////////////////////////////////////
		/// \brief Returns a bit set of the HttpMethods the new route being registered responds to
		//////////////////////////////////////////////////////////////////////////
		HttpMethodType getNewRouteMethods();
	private:
		///< The methods the existing route responds to
		HttpMethodType mOldMethods;

		///< The methods the new route responds to
		HttpMethodType mNewMethods;
	};
}

#endif