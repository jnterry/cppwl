/////////////////////////////////////////////////
///  Part of C++ Web Library - By Jamie Terry ///
/////////////////////////////////////////////////
/// \file SqliteConnection.cpp
/// \author Jamie Terry
/// \date 2015/08/28
/// \brief Contains implementation of cppwl::db::SqliteConnection
/// \ingroup db
/////////////////////////////////////////////////

#include <cppwl\db\SqliteConnection.hpp>
#include "sqlite3.h"

namespace cppwl{
	SqliteConnection::SqliteConnection()
		: mConnection(nullptr){

	}

	SqliteConnection::SqliteConnection(const char* name)
		: SqliteConnection() {
		open(name);
	}

	SqliteConnection::~SqliteConnection(){
		this->close();
	}

	bool SqliteConnection::open(const char* name){
		this->close();
		int result = sqlite3_open(name, &this->mConnection);
		if (result == SQLITE_OK){
			return true;
		} else {
			return false;
		}
	}

	bool SqliteConnection::isOpen(){
		return this->mConnection != nullptr;
	}

	bool SqliteConnection::close(){
		if (this->mConnection != nullptr){
			sqlite3_close(this->mConnection);
		}
	}

	void SqliteConnection::execute(const char* sql){

	}

	SqliteConnection::SqlitePreparedStatement prepare(const char* sql){

	}

	
}