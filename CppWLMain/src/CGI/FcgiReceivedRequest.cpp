/////////////////////////////////////////////////
///  Part of C++ Web Library - By Jamie Terry ///
/////////////////////////////////////////////////
/// \file FcgiReceivedRequest.cpp
/// \author Jamie Terry
/// \date 2015/08/21
/// \brief Contains implementation of cppwl::FcgiReceivedRequest
/// \ingroup cgi
/////////////////////////////////////////////////

#include "FcgiReceivedRequest.hpp"
#include <cppwl\core\Response.hpp>

namespace cppwl{

	FcgiReceivedRequest::FcgiReceivedRequest(int socketId)
	: ReceivedRequest(HttpMethod::GET, Url()){
		if (FCGX_InitRequest(&request, socketId, 0) != 0){
			throw lie::core::Exception(__FUNCTION__, "Failed to create FcgiReceivedRequest instance as FCGX_InitRequest failed");
		}
	}

	FcgiReceivedRequest::~FcgiReceivedRequest(){
		FCGX_Free(&this->request, true);
	}

	void FcgiReceivedRequest::respond(const Response& response){
		std::string head = response.getResponseHead();
		std::string body = response.getResponseBody();

		fcgi_streambuf reqOut(this->request.out);
		reqOut.sputn(head.c_str(), head.length());
		reqOut.sputn(body.c_str(), body.length());
		FCGX_Finish_r(&request);
	}


}