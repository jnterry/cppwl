/////////////////////////////////////////////////
///  Part of C++ Web Library - By Jamie Terry ///
/////////////////////////////////////////////////
/// \file FastCgiSocket.cpp
/// \author Jamie Terry
/// \date 2015/08/19
/// \brief Contains implementation of cppwl::FastCgiSocket class
/// \ingroup cgi
/////////////////////////////////////////////////

#include <iostream>

#include <lie\core\Convert.hpp>
#include <lie\core\ExcepParseError.hpp>

#include <cppwl\core\ExcepNetworkError.hpp>
#include <cppwl\CGI\FastCgiSocket.hpp>
#include <cppwl\core\Request.hpp>

#include <fastcgi.h>
#include <fcgio.h>

#include "FcgiReceivedRequest.hpp"

namespace {
	bool initedFcgiLib = false;

	//////////////////////////////////////////////////////////////////////////
	/// \brief Uses a variable string in a RequestBuildOptions to generate a string,
	/// obtains variable values from the request object, adds the names of all used
	/// variables to the usedVars vector
	//////////////////////////////////////////////////////////////////////////
	std::string genStringFromVariableString(std::string varString, 
		const cppwl::ReceivedRequest& request, std::vector<std::string>& usedVars) throw(lie::core::ExcepParseError){
		if (varString.empty()){ return ""; }
		std::string result;
		int tokenStart = 0;
		do{
			int tokenEnd = varString.find(" ", tokenStart);
			if (tokenEnd == std::string::npos){
				tokenEnd = varString.length();
			}
			std::string token = varString.substr(tokenStart, tokenEnd - tokenStart);
			if (token.empty()){
				//must be multiple spaces next to each other
				++tokenStart;
				continue;
			}
			if (token == "$"){
				throw lie::core::ExcepParseError(__FUNCTION__, varString,
					"Variable String", "", "Found '$' character that was not followed by a variable name");
			} else if (token[0] != '$'){
				result += token;
			} else {
				token = token.substr(1); //chop off the '$'
				usedVars.push_back(token); 
				result += request.getEnviromentVariable(token);
			}
			tokenStart = tokenEnd + 1;
		} while (tokenStart < varString.length());
		return result;
	}
}

namespace cppwl{

	FastCgiSocket::FastCgiSocket(unsigned short portNumber, const RequestBuildOptions& options) throw(cppwl::ExcepNetworkError)
	: mReqOptions(options){
		if (!initedFcgiLib){
			int retVal = FCGX_Init();
			if (retVal != 0){
				throw lie::core::Exception(__FUNCTION__,
					"Attempted to create a FastCgiSocket but failed to initiaize fcgi libary, FCGX_Init() returned " + lie::core::toString(retVal));
			}
		}
		mSocketDescriptor = FCGX_OpenSocket((":" + lie::core::toString(portNumber)).c_str(), 16);
		if (mSocketDescriptor == -1){
			throw cppwl::ExcepNetworkError(__FUNCTION__,
				&Socket(IPv4::LOCALHOST, portNumber), nullptr, "Attempted to create a FastCgiSocket but failed to open socket");
		}
	}

	FastCgiSocket::~FastCgiSocket(){
		
	}

	void FastCgiSocket::setRequestBuildOptions(const RequestBuildOptions& options){
		this->mReqOptions = options;
	}

	FastCgiSocket::RequestBuildOptions& FastCgiSocket::getRequestBuildOptions(){
		return this->mReqOptions;
	}

	ReceivedRequest* FastCgiSocket::makeRequest() throw(lie::core::Exception){
		return new FcgiReceivedRequest(this->mSocketDescriptor);
	}

	bool FastCgiSocket::accept(ReceivedRequest& request){
		FcgiReceivedRequest* req = reinterpret_cast<FcgiReceivedRequest*>(&request);
		int result = FCGX_Accept_r(&req->request);
		if (result != 0){
			//then an error occurred
			return false;
		}
		req->clearEnviromentVariables();

		//the envp is a pointer to an array of char*, each
		//char sequence has the format "key=value"
		//need to extract these and add to map
		char** env = req->request.envp;
		for (int i = 0; env[i] != NULL; ++i){
			std::string var(env[i]);
			int equalPos = var.find("=");
			if (equalPos == std::string::npos){
				continue; //ignore this variable
			}
			req->setEnviromentVariable(var.substr(0, equalPos), var.substr(equalPos + 1));
		}

		//the env map is now filled in, now need to fill in the other details
		std::vector<std::string> usedVariables;
		try{
			req->setUrl(Url(genStringFromVariableString(this->mReqOptions.url, *req, usedVariables)));
		} catch (std::exception& e){
			std::cerr << e.what() << std::endl;
			req->setUrl(Url());
		}
		try{
			req->setClientSocket(Socket(genStringFromVariableString(this->mReqOptions.clientSocket, *req, usedVariables)));
		}
		catch (std::exception& e){
			std::cerr << e.what() << std::endl;
			req->setClientSocket(Socket());
		}
		try{
			req->setServerSocket(Socket(genStringFromVariableString(this->mReqOptions.serverSocket, *req, usedVariables)));
		}
		catch (std::exception& e){
			std::cerr << e.what() << std::endl;
			req->setServerSocket(Socket());
		}
		try{
			req->setMethod(parseHttpMethod(genStringFromVariableString(this->mReqOptions.method, *req, usedVariables)));
		}
		catch (std::exception& e){
			std::cerr << e.what() << std::endl;
			req->setMethod(HttpMethod::UNKNOWN);
		}
		try{
			req->setContentType(genStringFromVariableString(this->mReqOptions.contentType, *req, usedVariables));
		}
		catch (std::exception& e){
			std::cerr << e.what() << std::endl;
			req->setContentType("");
		}
		int contentLength = 0;
		try{
			std::string lengthStr = genStringFromVariableString(this->mReqOptions.contentLength, *req, usedVariables);
			if (lengthStr.empty()){
				contentLength = 0;
			} else {
				contentLength = lie::core::parseInt(lengthStr);
			}
		}
		catch (std::exception& e){
			std::cerr << e.what() << std::endl;
			req->setContent("");
		}
		if (contentLength == 0){
			req->setContent("");
		} else {
			char* contentData = new char[contentLength];
			fcgi_streambuf reqIn(req->request.in);
			reqIn.sgetn(contentData, contentLength);
			req->setContent(contentData, contentLength);
		}
		
		//delete used vars if that option is set
		if (this->mReqOptions.deleteUsedVariables){
			for (auto it = usedVariables.begin(); it != usedVariables.end(); ++it){
				req->unsetEnviromentVariable(it->c_str());
			}
		}
	}
}