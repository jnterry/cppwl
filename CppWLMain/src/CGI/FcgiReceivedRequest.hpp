/////////////////////////////////////////////////
///  Part of C++ Web Library - By Jamie Terry ///
/////////////////////////////////////////////////
/// \file FcgiReceivedRequest.hpp
/// \author Jamie Terry
/// \date 2015/08/21
/// \brief Contains the implementation only class FcgiReceivedRequest
/// This is a class derived from ReceivedRequest that has a respond method
/// that correctly responds to a request received using the fcgi library
/// \ingroup cgi
/////////////////////////////////////////////////
#ifndef LIE_FCGIRECEIVEDREQUEST_HPP
#define LIE_FCGIRECEIVEDREQUEST_HPP

#include <cppwl\core\ReceivedRequest.hpp>
#include <fcgio.h>

namespace cppwl{
	struct FcgiReceivedRequest : public ReceivedRequest{
	public:
		//////////////////////////////////////////////////////////////////////////
		/// \brief Creates a new FcgiReceivedRequest instance
		/// \param socketId the socket that the request is for, pass the value
		/// returned from FCGX_OpenSocket
		/// \throw lie::core::Exception if there was a problem initiaizing the request
		//////////////////////////////////////////////////////////////////////////
		FcgiReceivedRequest(int socketId) throw(lie::core::Exception);

		//////////////////////////////////////////////////////////////////////////
		/// \brief Deletes the FCGX_Request struct
		//////////////////////////////////////////////////////////////////////////
		~FcgiReceivedRequest();

		//////////////////////////////////////////////////////////////////////////
		/// \brief Sends a response back to the client
		//////////////////////////////////////////////////////////////////////////
		void respond(const Response& response);

		///< The fcgi library request struct associated with this FcgiReceivedRequest
		FCGX_Request request;
	};
}

#endif