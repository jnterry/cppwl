/////////////////////////////////////////////////
///  Part of C++ Web Library - By Jamie Terry ///
/////////////////////////////////////////////////
/// \file Socket.cpp
/// \author Jamie Terry
/// \date 2015/08/19
/// \brief Contains implementation of cppwl::Socket
/// \ingroup core
/////////////////////////////////////////////////

#include <cppwl\core\Socket.hpp>
#include <lie\core\Convert.hpp>

namespace cppwl{

	Socket::Socket(IPv4 newIp, uint16_t newPortNumber)
	: ip(newIp), port(newPortNumber){
		//empty body
	}

	Socket::Socket(const Socket& socket)
	: ip(socket.ip), port(socket.port){
		//empty body
	}

	Socket::Socket(std::string value){
		this->parse(value);
	}

	std::string Socket::toString() const{
		return this->ip.toString() + ":" + lie::core::toString(this->port);
	}

	bool Socket::operator==(const Socket& other) const{
		return this->ip == other.ip && this->port == other.port;
	}

	bool Socket::operator!=(const Socket& other) const{
		return this->ip != other.ip || this->port != other.port;
	}

	Socket& Socket::operator=(const Socket& other){
		this->ip = other.ip;
		this->port = other.port;
		return *this;
	}

	void Socket::parse(std::string value){
		int colonIndex = value.find_last_of(":");
		if(colonIndex == std::string::npos){
			throw lie::core::ExcepParseError(__FUNCTION__, value, 
				"0.0.0.0:portNumber (ipv4:integer)", "", "No colon found in string");
		}
		if (colonIndex == value.length() - 1){
			throw lie::core::ExcepParseError(__FUNCTION__, value, 
				"0.0.0.0:portNumber (ipv4:integer)", "", "No port number followed the colon");
		}
		this->ip.parse(value.substr(0, colonIndex));
		try{
			this->port = lie::core::parseInt(value.substr(colonIndex + 1));
		} catch (lie::core::ExcepParseError e){
			throw lie::core::ExcepParseError(__FUNCTION__, value,
				"0.0.0.0:portNumber (ipv4:integer)", "", "IPv4 address was invalid", new lie::core::Exception(e));
		}
	}
}
