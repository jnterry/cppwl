/////////////////////////////////////////////////
///  Part of C++ Web Library - By Jamie Terry ///
/////////////////////////////////////////////////
/// \file Response.cpp
/// \author Jamie Terry
/// \date 2015/08/21
/// \brief Contains implementation of the cppwl::Response class
/// \ingroup core
/////////////////////////////////////////////////

#include <cppwl\core\Response.hpp>
#include <lie\core\Convert.hpp>
#include <lie\core\StringUtils.hpp>

namespace cppwl{

	Response::Response(HttpStatus status)
	: mStatus(status), mHTTPVersion("HTTP/1.1"), mContentType(""), mServerName(""){
		
	}

	std::string Response::getResponseHead() const{
		lie::core::StringBuilder sb;
		sb << this->mHTTPVersion << "\r\n";
		sb << "Status: " << lie::core::toString(this->mStatus.code) << " " << this->mStatus.text << "\r\n";
		if (!this->mContentType.empty()){
			sb << "Content-type: " << this->mContentType << "\r\n";
		}
		if (!this->mServerName.empty()){
			sb << "Server: " << this->mServerName << "\r\n";
		}
		sb << "\r\n";
		return sb;
	}

	const std::string& Response::getResponseBody() const{
		return this->mBody;
	}

	const std::string& Response::getContentType() const{
		return this->mContentType;
	}

	const std::string& Response::getServerName() const{
		return this->mServerName;
	}

	const HttpStatus& Response::getStatus() const{
		return this->mStatus;
	}

	void Response::setResponseBody(std::string body){
		this->mBody = body;
	}

	void Response::setContentType(std::string type){
		this->mContentType = type;
	}

	void Response::setServerName(std::string name){
		this->mServerName = name;
	}

	void Response::setStatus(HttpStatus status){
		this->mStatus = status;
	}
}