/////////////////////////////////////////////////
///  Part of C++ Web Library - By Jamie Terry ///
/////////////////////////////////////////////////
/// \file ExcepNetworkError.cpp
/// \author Jamie Terry
/// \date 2015/08/19
/// \brief Contains the implementation of cppwl::ExcepNetworkError
/// \ingroup core
/////////////////////////////////////////////////

#include <cppwl\core\ExcepNetworkError.hpp>
#include <lie\core\StringUtils.hpp>

namespace {
	std::string generateMessage(const cppwl::Socket* local, const cppwl::Socket* remote, std::string msg){
		lie::core::StringBuilder sb;
		sb << "A network error occurred";
		if (local != nullptr){
			sb << ", local socket: \"" << local->toString() << "\"";
		}
		if (remote != nullptr){
			sb << ", remote socket: \"" << remote->toString() << "\"";
		}
		sb << ".\n";
		sb << "Details: " << msg;
		return sb.str();
	}
}

namespace cppwl{

	ExcepNetworkError::ExcepNetworkError(std::string function, const Socket* local, const Socket* remote,
		std::string msg, lie::core::Exception* cause)
		: Exception("cppwl::ExcepNetworkError", function, generateMessage(local, remote, msg), cause),
		mLocalSocket(nullptr), mRemoteSocket(nullptr){
		if (local != nullptr){
			this->mLocalSocket = new Socket(*local);
		}
		if (remote != nullptr){
			this->mRemoteSocket = new Socket(*remote);
		}
	}

	ExcepNetworkError::~ExcepNetworkError(){
		delete this->mLocalSocket;
		delete this->mRemoteSocket;
	}


	Socket* ExcepNetworkError::getLocalSocket(){
		return this->mLocalSocket;
	}

	Socket* ExcepNetworkError::getRemoteSocket(){
		return this->mRemoteSocket;
	}
}