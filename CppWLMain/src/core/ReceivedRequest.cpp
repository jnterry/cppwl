/////////////////////////////////////////////////
///  Part of C++ Web Library - By Jamie Terry ///
/////////////////////////////////////////////////
/// \file ReceivedRequest.cpp
/// \author Jamie Terry
/// \date 2015/08/21
/// \brief Contains the implementation for the cppwl::ReceivedRequest class
/// \ingroup core
/////////////////////////////////////////////////

#include <cppwl\core\ReceivedRequest.hpp>

namespace{
	std::string emptyString = "";
}

namespace cppwl{
	ReceivedRequest::ReceivedRequest(HttpMethod method, const Url& url)
	: Request(method, url){
		//empty body
	}

	const Socket& ReceivedRequest::getServerSocket() const{
		return this->mServerSocket;
	}

	const Socket& ReceivedRequest::getClientSocket() const{
		return this->mClientSocket;
	}

	void ReceivedRequest::setServerSocket(const Socket& socket){
		this->mServerSocket = socket;
	}

	void ReceivedRequest::setClientSocket(const Socket& socket){
		this->mClientSocket = socket;
	}

	const std::string& ReceivedRequest::getEnviromentVariable(std::string varName) const{
		auto it = this->mEnvVars.find(varName);
		if (it == this->mEnvVars.end()){
			return emptyString;
		} else {
			return it->second;
		}
	}

	size_t ReceivedRequest::getEnviromentVariableCount() const{
		return this->mEnvVars.size();
	}

	void ReceivedRequest::setEnviromentVariable(std::string varName, std::string value){
		if (value.empty()){
			this->unsetEnviromentVariable(varName);
		} else {
			this->mEnvVars[varName] = value;
		}
	}

	void ReceivedRequest::unsetEnviromentVariable(std::string varName){
		auto it = this->mEnvVars.find(varName);
		if (it != this->mEnvVars.end()){
			this->mEnvVars.erase(it);
		}
	}

	void ReceivedRequest::clearEnviromentVariables(){
		this->mEnvVars.clear();
	}
}
