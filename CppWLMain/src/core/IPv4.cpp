#include <cppwl\core\IPv4.hpp>
#include <lie\core\StringUtils.hpp>
#include <lie\core\Convert.hpp>
#include <lie\core\ExcepParseError.hpp>
#include <lie\core\Exception.hpp>

namespace {
	uint32_t ipComponentsToInt(uint32_t comp0, uint32_t comp1, uint32_t comp2, uint32_t comp3){
		return comp0
			+ (comp1 << 8)
			+ (comp2 << 16)
			+ (comp3 << 24);
	}

	uint8_t ipIntToComponent(uint8_t component, uint32_t ip){
		int shift = 8 * component;
		return (uint8_t)((ip & (255 << shift)) >> (shift));
	}

	uint32_t componentMasks[] = {
		0x000000FF,
		0x0000FF00,
		0x00FF0000,
		0xFF000000
	};
}

namespace cppwl{

	const IPv4 IPv4::LOCALHOST = IPv4(127, 0, 0, 1);

	IPv4::IPv4() : mIP(0) {
		//empty body
	}

	IPv4::IPv4(uint8_t comp0, uint8_t comp1, uint8_t comp2, uint8_t comp3)
	: mIP(ipComponentsToInt(comp0, comp1, comp2, comp3)){
		//empty body
	}

	IPv4::IPv4(std::string value) : mIP(0){
		this->parse(value);
	}

	IPv4::IPv4(uint32_t value) : mIP(value){
		//empty body
	}

	uint8_t IPv4::getComponent(uint8_t component) const{
		return ipIntToComponent(component, this->mIP);
	}

	IPv4& IPv4::setComponent(uint8_t component, uint8_t value){
		uint32_t mask = ~componentMasks[component];
		this->mIP &= mask;
		this->mIP |= (((uint32_t)value) << (component * 8));
		return *this;
	}

	IPv4& IPv4::set(uint8_t comp0, uint8_t comp1, uint8_t comp2, uint8_t comp3){
		this->mIP = ipComponentsToInt(comp0, comp1, comp2, comp3);
		return *this;
	}

	IPv4& IPv4::parse(std::string value) throw(lie::core::ExcepParseError) {
		//the new value of the ip, dont directly modify this->mIP as if parse error this function should make no changes
		uint32_t newIP = 0;
		int compStartIndex = 0;
		int compEndIndex = 0;

		for (int compIndex = 0; compIndex < 4; ++compIndex){
			compEndIndex = value.find(".", compStartIndex);
			if (compEndIndex == std::string::npos){
				if (compIndex < 3){
					throw lie::core::ExcepParseError(__FUNCTION__, value,
						"xxx.xxx.xxx.xxx where each xxx is a 1-3 digit number between 0 and 255", "",
						"IPv4 addresses contain 4 components, string contains " + lie::core::toString(compIndex));
				}
				else {
					//this is the last component
					//if format is 1.2.3.4:8000 then use : as end of component, else use end of string
					compEndIndex = value.find(":", compStartIndex);
				}
				if (compEndIndex == std::string::npos){
					//use length even though it is an invalid index as the actual char
					//at the index isnt included (as the dot wouldnt be)
					compEndIndex = value.length();
				}
			}
			int componentValue = 0;
			try{
				componentValue = lie::core::parseInt(value.substr(compStartIndex, compEndIndex - compStartIndex));
			}
			catch (lie::core::ExcepParseError e){
				throw lie::core::ExcepParseError(__FUNCTION__, value,
					"xxx.xxx.xxx.xxx where each xxx is a 1-3 digit number between 0 and 255", "",
					"Component index " + lie::core::toString(compIndex) + " is not an integer", new lie::core::Exception(e));
			}
			if (componentValue < 0 || componentValue > 255){
				throw lie::core::ExcepParseError(__FUNCTION__, value,
					"xxx.xxx.xxx.xxx where each xxx is a 1-3 digit number between 0 and 255", "",
					"Component index " + lie::core::toString(compIndex) + " not in range 0-255, value: " + lie::core::toString(componentValue));
			}
			newIP |= componentValue << (compIndex * 8);
			compStartIndex = compEndIndex + 1;
		}
		this->mIP = newIP;
	}

	std::string IPv4::toString() const{
		return lie::core::StringBuilder()	<< (uint32_t)ipIntToComponent(0, this->mIP) << "." 
											<< (uint32_t)ipIntToComponent(1, this->mIP) << "."
											<< (uint32_t)ipIntToComponent(2, this->mIP) << "."
											<< (uint32_t)ipIntToComponent(3, this->mIP);
	}

	uint32_t IPv4::toInt() const{
		return this->mIP;
	}

	IPv4& IPv4::fromInt(uint32_t value){
		this->mIP = value;
		return *this;
	}

	IPv4& IPv4::operator=(const IPv4 other){
		this->mIP = other.mIP;
		return *this;
	}

	bool IPv4::operator==(const IPv4& other) const{
		return this->mIP == other.mIP;
	}

	bool IPv4::operator!=(const IPv4& other) const{
		return this->mIP != other.mIP;
	}
}
