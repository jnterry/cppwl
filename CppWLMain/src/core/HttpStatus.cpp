/////////////////////////////////////////////////
///  Part of C++ Web Library - By Jamie Terry ///
/////////////////////////////////////////////////
/// \file HttpStatus.cpp
/// \author Jamie Terry
/// \date 2015/08/21
/// \brief Contains implementation of HttpStatus class
/// \ingroup core
/////////////////////////////////////////////////

#include <cppwl\core\HttpStatus.hpp>

namespace cppwl{

	const HttpStatus HttpStatus::OK = HttpStatus(200);
	const HttpStatus HttpStatus::MOVED_PERMANENTLY = HttpStatus(301);
	const HttpStatus HttpStatus::SEE_OTHER = HttpStatus(303);
	const HttpStatus HttpStatus::TEMPORARY_REDIRECT = HttpStatus(307);
	const HttpStatus HttpStatus::PERMANENT_REDIRECT = HttpStatus(308);
	const HttpStatus HttpStatus::BAD_REQUEST = HttpStatus(400);
	const HttpStatus HttpStatus::UNAUTHORIZED = HttpStatus(401);
	const HttpStatus HttpStatus::FORBIDDEN = HttpStatus(403);
	const HttpStatus HttpStatus::NOT_FOUND = HttpStatus(404);
	const HttpStatus HttpStatus::INTERNAL_SERVER_ERROR = HttpStatus(500);
	const HttpStatus HttpStatus::NOT_IMPLEMENTED = HttpStatus(501);
	const HttpStatus HttpStatus::BAD_GATEWAY = HttpStatus(502);
	const HttpStatus HttpStatus::SERVICE_UNAVAILABLE  = HttpStatus(503);
	const HttpStatus HttpStatus::GATEWAY_TIMEOUT  = HttpStatus(504);

	HttpStatus::HttpStatus(unsigned short newCode, std::string newText)
	: code(newCode), text(newText){
		//empty body
	}

	HttpStatus::HttpStatus(unsigned short newCode) : code(newCode){
		switch (newCode){
		case 100:
			text = "Continue";
			break;
		case 101:
			text = "Switching Protocols";
			break;
		case 102:
			text = "Processing";
			break;

		case 200:
			text = "OK";
			break;
		case 201:
			text = "Created";
			break;
		case 202:
			text = "Accepted";
			break;
		case 203:
			text = "Non-Authoritative Information";
			break;
		case 204:
			text = "No Content";
			break;
		case 205:
			text = "Partial Content";
			break;
		case 206:
			text = "Partial Content";
			break;
		case 207:
			text = "Multi-HttpStatus";
			break;

		case 300:
			text = "Multiple Choices";
			break;
		case 301:
			text = "Moved Permanently";
			break;
		case 302:
			text = "Found";
			break;
		case 303:
			text = "See Other";
			break;
		case 304:
			text = "Not Modified";
			break;
		case 305:
			text = "Use Proxy";
			break;
		case 306:
			text = "Switch Proxy";
			break;
		case 307:
			text = "Temporary Redirect";
			break;
		case 308:
			text = "Permanent Redirect";
			break;
		case 400:
			text = "Bad Request";
			break;
		case 401:
			text = "Unauthorized";
			break;
		case 402:
			text = "Payment Required";
			break;
		case 403:
			text = "Forbidden";
			break;
		case 404:
			text = "Not Found";
			break;
		case 405:
			text = "Method Not Allowed";
			break;
		case 406:
			text = "Not Acceptable";
			break;
		case 408:
			text = "Request Timeout";
			break;

		case 500:
			text = "Internal Server Error";
			break;
		case 501:
			text = "Not Implemented";
			break;
		case 502:
			text = "Bad Gateway";
			break;
		case 503:
			text = "Service Unavailable";
			break;
		case 504:
			text = "Gateway Timeout";
			break;
		case 505:
			text = "HTTP Version Not Supported";
			break;
		}
	}

	unsigned short HttpStatus::getSeries(){
		if (this->code > 599){
			return Series::UNKNOWN;
		} else if (this->code > 499){
			return Series::SERVER_ERROR;
		} else if (this->code > 399){
			return Series::CLIENT_ERROR;
		} else if (this->code > 299){
			return Series::REDIRECTION;
		} else if (this->code > 199){
			return Series::SUCCESSFUL;
		} else if (this->code > 99){
			return Series::INFORMATIONAL;
		} else {
			return Series::UNKNOWN;
		}
	}
}