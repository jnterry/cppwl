/////////////////////////////////////////////////
///  Part of C++ Web Library - By Jamie Terry ///
/////////////////////////////////////////////////
/// \file HtmlDebug.cpp
/// \author Jamie Terry
/// \date 2015/08/22
/// \brief Contains implementations for the functions declared in HtmlDebug.hpp
/// \ingroup core
/////////////////////////////////////////////////

#include <cppwl\core\HtmlDebug.hpp>

namespace cppwl{
	void htmlDebug(lie::core::StringBuilder& str, const ReceivedRequest& request){
		str << "<div>";
		str << "<h1>Request Debug</h1>";
		str << "<div>";
		str << "<h3>Request Data</h3>";
		str << "<table>";
		str << "<tr><td>HTTP Method:</td><td>" << cppwl::httpMethodToString(request.getMethod()) << "</td></tr>";
		str << "<tr><td>URL:</td><td>" << (std::string)request.getUrl() << "</td></tr>";
		str << "<tr><td>Client Socket:</td><td>" << (std::string)request.getClientSocket() << "</td></tr>";
		str << "<tr><td>Server Socket:</td><td>" << (std::string)request.getServerSocket() << "</td></tr>";
		str << "<tr><td>Content Type:</td><td>" << request.getContentType() << "</td></tr>";
		str << "<tr><td>Content Length:</td><td>" << request.getContentLength() << "</td></tr>";
		str << "<tr><td>Content:</td><td>" << (request.getContent() == nullptr ? "nullptr" : request.getContent()) << "</td></tr>";
		str << "</table>";
		str << "</div>";
		str << "<div>";
		str << "<h3>Environment Variables</h3>";
		str << "<table>";
		str << "<tr><th>Name</th><th>Value</th></td>";
		for (auto it = request.cbegin(); it != request.cend(); ++it){
			str << "<tr><td>" << it->first << "</td><td>" << it->second << "</td></tr>";
		}
		str << "</table>";
		str << "</div>";
		str << "</div>";
	}
}