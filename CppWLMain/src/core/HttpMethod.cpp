/////////////////////////////////////////////////
///  Part of C++ Web Library - By Jamie Terry ///
/////////////////////////////////////////////////
/// \file HttpMethod.cpp
/// \author Jamie Terry
/// \date 2015/08/21
/// \brief Contains implementation of functions in the HttpMethod.hpp file
/// \ingroup core
/////////////////////////////////////////////////

#include <cppwl\core\HttpMethod.hpp>
#include <lie\core\StringUtils.hpp>

namespace cppwl{
	std::string httpMethodToString(HttpMethod method){
		switch (method){
		case GET:
			return "GET";
		case HEAD:
			return "HEAD";
		case POST:
			return "POST";
		case PUT:
			return "PUT";
		case DELETE:
			return "DELETE";
		case TRACE:
			return "TRACE";
		case OPTIONS:
			return "OPTIONS";
		case PATCH:
			return "PATCH";
		default:
			return "UNKNOWN";
		}
	}

	HttpMethod parseHttpMethod(std::string value){
		if (lie::core::equalsIgnoreCase(value, "get")){
			return HttpMethod::GET;
		} else if (lie::core::equalsIgnoreCase(value, "head")){
			return HttpMethod::HEAD;
		} else if (lie::core::equalsIgnoreCase(value, "post")){
			return HttpMethod::POST;
		} else if (lie::core::equalsIgnoreCase(value, "put")){
			return HttpMethod::PUT;
		} else if (lie::core::equalsIgnoreCase(value, "delete")){
			return HttpMethod::DELETE;
		} else if (lie::core::equalsIgnoreCase(value, "trace")){
			return HttpMethod::TRACE;
		} else if (lie::core::equalsIgnoreCase(value, "options")){
			return HttpMethod::OPTIONS;
		} else if (lie::core::equalsIgnoreCase(value, "patch")){
			return HttpMethod::PATCH;
		} else {
			return HttpMethod::UNKNOWN;
		}
	}
}