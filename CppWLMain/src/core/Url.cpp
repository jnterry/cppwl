/////////////////////////////////////////////////
///  Part of C++ Web Library - By Jamie Terry ///
/////////////////////////////////////////////////
/// \file Url.cpp
/// \author Jamie Terry
/// \date 2015/08/19
/// \brief Contains the implementation for the cppwl::Url class
/// \ingroup core
/////////////////////////////////////////////////

#include <lie\core\StringUtils.hpp>
#include <cppwl\core\Url.hpp>
#include <regex>

namespace{
	const std::string emptyString = "";
}
namespace cppwl{
	Url::Url()
	: mProtocol(""), mDomain("0.0.0.0"), mFragmentIdentifier(""), mResourceHasTrailingSlash(false){
		//empty body
	}

	Url::Url(std::string url) throw(lie::core::ExcepParseError){
		this->parse(url);
	}

	Url::Url(std::string protocol, std::string domain, std::string resource, 
		std::string queryString, std::string fragmentIdentifier)
	: mProtocol(protocol), mDomain(domain), mFragmentIdentifier(fragmentIdentifier), mResourceHasTrailingSlash(false){
		this->setResource(resource);
		this->setQueryString(queryString);
	}
	
	
	const std::string& Url::getProtocol() const{
		return this->mProtocol;
	}

	const std::string& Url::getDomain() const{
		return this->mDomain;
	}

	const std::string Url::getResource() const{
		if (this->mResourceComponents.size() == 0){
			return "";
		} else {
			std::string result = this->mResourceComponents[0];
			for (int i = 1; i < this->mResourceComponents.size(); ++i){
				result += "/";
				result += this->mResourceComponents[i];
			}
			if (this->mResourceHasTrailingSlash){
				result += "/";
			}
			return result;
		}
	}

	const std::string& Url::getResourceComponent(unsigned short index) const{
		return this->mResourceComponents[index];
	}

	unsigned short Url::getResourceComponentCount() const{
		return this->mResourceComponents.size();
	}

	const std::string& Url::getQueryParam(std::string paramName) const{
		auto iter = this->mQueryParams.find(paramName);
		if (iter == this->mQueryParams.end()){
			//no such parameter
			return emptyString;
		} else {
			return iter->second;
		}
	}

	const std::string Url::getQueryString() const{
		if (this->mQueryParams.empty()){
			return "";
		} else {
			lie::core::StringBuilder sb;
			bool needAnd = false;
			for (auto it = this->mQueryParams.begin(); it != this->mQueryParams.end(); ++it){
				if (needAnd){
					sb << "&";
				}
				sb << it->first << "=" << it->second;
				needAnd = true;
			}
			return sb.str();
		}
	}

	int Url::getQueryParamCount() const{
		return this->mQueryParams.size();
	}

	const std::string& Url::getFragmentIdentifier() const{
		return this->mFragmentIdentifier;
	}

	bool Url::hasQueryParam(std::string paramName) const{
		return this->mQueryParams.find(paramName) != this->mQueryParams.end();
	}

	void Url::setProtocol(std::string protocol){
		this->mProtocol = protocol;
	}

	void Url::setDomain(std::string domain){
		this->mDomain = domain;
	}

	void Url::setResource(std::string resource){
		this->mResourceComponents.clear();
		this->pushResourceComponent(resource);
	}

	void Url::setResourceComponent(unsigned short index, std::string newValue){
		this->mResourceComponents[index] = newValue;
	}

	void Url::popResourceComponent(){
		if (!this->mResourceComponents.empty()){
			this->mResourceHasTrailingSlash = true;
			this->mResourceComponents.pop_back();
		}
	}

	void Url::pushResourceComponent(std::string components){
		if (components.empty()){ return; }
		this->mResourceHasTrailingSlash = (components.at(components.length() - 1) == '/');
		lie::core::stringSplit(this->mResourceComponents, components, '/');
	}

	void Url::setQueryParam(std::string paramName, std::string value){
		if (value.empty()){
			this->unsetQueryParam(paramName);
		} else {
			this->mQueryParams[paramName] = value;
		}
	}

	void Url::setQueryString(std::string queryStr) throw(lie::core::ExcepParseError){
		if (queryStr.empty()){
			this->mQueryParams.clear();
			return;
		}
		if (queryStr[0] == '?'){
			queryStr = queryStr.substr(1);
		}
		std::map<std::string, std::string> newQueryParams;
		int lastCharIndex = queryStr.length();
		int lastAnd = 0;
		int nextAnd;
		do{
			nextAnd = queryStr.find("&", lastAnd);
			if (nextAnd == std::string::npos){
				nextAnd = lastCharIndex;
			}

			int equalIndex = queryStr.find("=", lastAnd);
			if (equalIndex == std::string::npos){
				throw lie::core::ExcepParseError(__FUNCTION__, queryStr, "\"key1=value1\", \"key1=value1&key2=value2\" etc...", "",
					"No value was found for the key " + queryStr.substr(lastAnd, nextAnd - lastAnd));
			}
			newQueryParams[queryStr.substr(lastAnd, equalIndex - lastAnd)] = queryStr.substr(equalIndex+1, nextAnd - (equalIndex+1));

			lastAnd = nextAnd + 1;
		} while (nextAnd != lastCharIndex);

		//not yet thrown, therefore url is valid
		this->mQueryParams = newQueryParams;
	}

	void Url::setFragmentIdentifier(std::string fragId){
		this->mFragmentIdentifier = fragId;
	}

	void Url::unsetQueryParam(std::string paramName){
		this->mQueryParams.erase(paramName);
	}

	void Url::parse(std::string url) throw(lie::core::ExcepParseError){
		std::string newProtocol = "";
		std::string newDomain = "";
		std::string newResource = "";
		std::string newFragmentIdentifier = "";

		int protEnd = url.find("://");
		if (protEnd != std::string::npos){
			newProtocol = url.substr(0, protEnd);
			if (!std::regex_match(newProtocol, std::regex("^[a-z][a-z0-9+-.]*$"))){
				throw lie::core::ExcepParseError(__FUNCTION__, newProtocol, 
					"Valid Protocol - Lowercase letter followed by any length combination of 'a-z', '0-9', plus '+', '-' and '.'", 
					"^[a-z][a-z0-9+-.]*$",
					"The url \"" + url + "\" contains an invalid protocol (scheme name)");
			}
			protEnd += 3; //advance to end of :// sequence
		} else {
			//no protocol in url
			protEnd = 0;
		}

		int domainEnd = url.length();
		int resourceStart = url.find("/", protEnd);
		int resourceEnd = std::string::npos;
		int queryStart = url.find("?", protEnd);
		int queryEnd = std::string::npos;
		int fragIdentStart = url.find("#", protEnd);

		if (queryStart != std::string::npos && fragIdentStart != std::string::npos && queryStart > fragIdentStart){
			//then the fragment identifier contains a '?', make it so the query string is not parsed, but the
			//fragment contains the '?' and what follows it
			queryStart = std::string::npos;
		}

		if (resourceStart != std::string::npos){
			domainEnd = resourceStart;
			++resourceStart; //past the '/' char
			if (queryStart != std::string::npos){
				resourceEnd = queryStart;
			} else if (fragIdentStart != std::string::npos){
				resourceEnd = fragIdentStart;
				++fragIdentStart; //past the '#' char
			} else {
				resourceEnd = url.length();
			}
		} else if(queryStart != std::string::npos){
			domainEnd = queryStart;
			++queryStart; //past the '?' char
		} else if (fragIdentStart != std::string::npos){
			domainEnd = fragIdentStart;
			++fragIdentStart; //past the '#' char
		}
		if (fragIdentStart != std::string::npos){
			queryEnd = fragIdentStart;
		} else {
			queryEnd = url.length();
		}

		newDomain = url.substr(protEnd, domainEnd - protEnd);
		if (newProtocol != "file"){
			//then need a domain
			if (newDomain.empty()){
				throw lie::core::ExcepParseError(__FUNCTION__, url, "Valid URL", "", 
					"The URL did not contain a domain, this is only allowed for urls with the 'file' protocol");
			}
		}

		if (resourceStart != std::string::npos){
			newResource = url.substr(resourceStart, resourceEnd - resourceStart);
		}
		if (fragIdentStart != std::string::npos){
			newFragmentIdentifier = url.substr(fragIdentStart);
			if (newFragmentIdentifier.empty()){
				throw lie::core::ExcepParseError(__FUNCTION__, url, "Valid URL", "",
					"The URL contains a '#' indicating the start of a fragment identifier, but no fragment identifier followed.");
			}
		}

		//all newXXX strings now populated
		//parse query string, if valid, url is valid
		if (queryStart != std::string::npos){
			try{
				std::string newQueryString = url.substr(queryStart, queryEnd - queryStart);
				if (newQueryString.empty()){
					throw lie::core::ExcepParseError(__FUNCTION__, url, "Valid URL", "",
						"The URL contains a '?' indicating the start of a query string, but no query string followed.");
				}
				this->setQueryString(newQueryString);
			} catch (lie::core::ExcepParseError e){
				throw lie::core::ExcepParseError(__FUNCTION__, url, "Valid URL", "",
					"Failed to parse query string", new lie::core::Exception(e));
			}
		}
		this->setResource(newResource);
		this->mProtocol = newProtocol;
		this->mDomain = newDomain;
		this->mFragmentIdentifier = newFragmentIdentifier;
	}

	Url::operator std::string() const{
		lie::core::StringBuilder sb;
		if (!this->mProtocol.empty()){
			sb << this->mProtocol << "://";
		}
		sb << this->mDomain;
		if (!this->mResourceComponents.empty()){
			sb << "/" << this->getResource();
		}
		if (!this->mQueryParams.empty()){
			sb << "?" << this->getQueryString();
		}
		if (!this->mFragmentIdentifier.empty()){
			sb << "#" << this->mFragmentIdentifier;
		}
		return sb.str();
	}

	bool Url::operator==(const Url& other) const{
		if (this == &other){
			//same object
			return true;
		}
		if (this->mDomain != other.mDomain ||
			this->mResourceComponents.size() != other.mResourceComponents.size() ||
			this->mProtocol != other.mProtocol ||
			this->mQueryParams.size() != other.mQueryParams.size() ||
			this->mFragmentIdentifier != other.mFragmentIdentifier){
			return false;
		}
		//still going? basic checks equal, see if resource components and query params are equal
		for (int i = 0; i < this->mResourceComponents.size(); ++i){
			if (this->mResourceComponents[0] != other.mResourceComponents[0]){
				return false;
			}
		}

		for (auto it = this->mQueryParams.begin(); it != this->mQueryParams.end(); ++it){
			auto it2 = other.mQueryParams.find(it->first);
			if (it2 == other.mQueryParams.end() || it->second != it2->second){
				return false;
			}
		}
		//still going? must be equal
		return true;
	}

	Url& Url::operator=(const Url& other){
		if (&other == this){ return *this; }
		this->mProtocol = other.mProtocol;
		this->mDomain = other.mDomain;
		this->mResourceComponents = other.mResourceComponents;
		this->mFragmentIdentifier = other.mFragmentIdentifier;
		this->mQueryParams = other.mQueryParams;
		return *this;
	}
}