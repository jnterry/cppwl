/////////////////////////////////////////////////
///  Part of C++ Web Library - By Jamie Terry ///
/////////////////////////////////////////////////
/// \file Request.cpp
/// \author Jamie Terry
/// \date 2015/08/21
/// \brief Contains implementation for cppwl::Request
/// \ingroup core
/////////////////////////////////////////////////

#include <cppwl\core\Request.hpp>

namespace cppwl{
	Request::Request(HttpMethod method, const Url& url)
	: mMethod(method), mUrl(url), mContent(nullptr), mContentLength(0), mContentType(""){
		//empty body
	}

	Request::~Request(){
		if (this->mContent != nullptr){
			delete[] this->mContent;
		}
	}

	HttpMethod Request::getMethod() const{
		return this->mMethod;
	}

	const std::string& Request::getContentType() const{
		return this->mContentType;
	}

	size_t Request::getContentLength() const{
		return this->mContentLength;
	}

	const char* Request::getContent() const{
		return this->mContent;
	}

	const Url& Request::getUrl() const{
		return this->mUrl;
	}

	Url& Request::getUrl(){
		return this->mUrl;
	}

	void Request::setContent(const char* content, size_t contentLength){
		if (this->mContent != nullptr){
			delete[] mContent;
		}
		this->mContent = new char[contentLength+1];
		memcpy(this->mContent, content, contentLength);
		this->mContentLength = contentLength;
		this->mContent[contentLength] = '\0'; //null terminate the content
	}

	void Request::setContentType(std::string content){
		this->mContentType = content;
	}

	void Request::setMethod(HttpMethod method){
		this->mMethod = method;
	}

	void Request::setUrl(const Url& url){
		this->mUrl = url;
	}
}