/////////////////////////////////////////////////
///  Part of C++ Web Library - By Jamie Terry ///
/////////////////////////////////////////////////
/// \file Route.cpp
/// \author Jamie Terry
/// \date 2015/08/27
/// \brief Contains implementation of the cppwl::Route class
/// \ingroup route
/////////////////////////////////////////////////

#include <cppwl\route\Route.hpp>

namespace cppwl{
	Route::Route(HttpMethodType methods)
		: mMethods(methods){
		//empty body
	}

	HttpMethodType Route::getMethods(){
		return mMethods;
	}
}