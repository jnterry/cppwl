/////////////////////////////////////////////////
///  Part of C++ Web Library - By Jamie Terry ///
/////////////////////////////////////////////////
/// \file ExcepDuplicateRoute.cpp
/// \author Jamie Terry
/// \date 2015/08/26
/// \brief Contains implementation of the cppwl::ExcepDuplicateRoute class
/// \ingroup route
/////////////////////////////////////////////////

#include <lie\core\StringUtils.hpp>
#include <cppwl\route\ExcepDuplicateRoute.hpp>

namespace{
	void appendMethodList(cppwl::HttpMethodType methods, lie::core::StringBuilder& sb){
		bool added = false;
		if ((cppwl::HttpMethod::GET & methods) != 0){
			if (added){ sb << ", "; }
			sb << "GET";
			added = true;
		}
		if ((cppwl::HttpMethod::HEAD & methods) != 0){
			if (added){ sb << ", "; }
			sb << "HEAD";
			added = true;
		}
		if ((cppwl::HttpMethod::POST & methods) != 0){
			if (added){ sb << ", "; }
			sb << "POST";
			added = true;
		}
		if ((cppwl::HttpMethod::PUT & methods) != 0){
			if (added){ sb << ", "; }
			sb << "PUT";
			added = true;
		}
		if ((cppwl::HttpMethod::DELETE & methods) != 0){
			if (added){ sb << ", "; }
			sb << "DELETE";
			added = true;
		}
		if ((cppwl::HttpMethod::TRACE & methods) != 0){
			if (added){ sb << ", "; }
			sb << "TRACE";
			added = true;
		}
		if ((cppwl::HttpMethod::OPTIONS & methods) != 0){
			if (added){ sb << ", "; }
			sb << "OPTIONS";
			added = true;
		}
		if ((cppwl::HttpMethod::PATCH & methods) != 0){
			if (added){ sb << ", "; }
			sb << "PATCH";
			added = true;
		}
	}

	std::string generateMessage(std::string urlMatcher, cppwl::HttpMethodType oldRoute, cppwl::HttpMethodType newRoute){
		lie::core::StringBuilder sb;
		sb << "Attempted to register a new route with the url matcher \"" << urlMatcher << "\" to respond to the methods \"";
		appendMethodList(newRoute, sb);
		sb << "\"\nAn existing route with the same url matcher responds to at least one of the same methods, it responds to \"";
		appendMethodList(oldRoute, sb);
		sb << "\"\nThe methods that both routes respond to are \"";
		appendMethodList(oldRoute & newRoute, sb);
		sb << "\"";
		return sb;
	}
}

namespace cppwl{
	ExcepDuplicateRoute::ExcepDuplicateRoute(std::string function, std::string urlMatcher, HttpMethodType previousRouteMethods, HttpMethodType newRouteMethods, lie::core::Logger& logger /* = lie::core::DLog */)
		: Exception("cppwl::ExcepDuplicateRoute", function, 
		generateMessage(urlMatcher, previousRouteMethods, newRouteMethods), nullptr, logger),
		mOldMethods(previousRouteMethods), mNewMethods(newRouteMethods){
		//empty body
	}

	HttpMethodType ExcepDuplicateRoute::getExistingRouteMethods(){
		return mOldMethods;
	}

	HttpMethodType ExcepDuplicateRoute::getNewRouteMethods(){
		return mNewMethods;
	}
}