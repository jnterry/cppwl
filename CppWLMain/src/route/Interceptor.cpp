/////////////////////////////////////////////////
///  Part of C++ Web Library - By Jamie Terry ///
/////////////////////////////////////////////////
/// \file Interceptor.cpp
/// \author Jamie Terry
/// \date 2015/08/23
/// \brief Contains implementation of the cppwl::Interceptor class
/// \ingroup route
/////////////////////////////////////////////////

#include <cppwl\route\Interceptor.hpp>

namespace cppwl{
	Interceptor::Interceptor()
		: mRouter(nullptr), mNext(nullptr){
		//empty body
	}

	Response& Interceptor::next(const ReceivedRequest& request){
		return this->mNext->intercept(request);
	}

	Router* Interceptor::getRouter(){
		return this->mRouter;
	}


}