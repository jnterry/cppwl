/////////////////////////////////////////////////
///  Part of C++ Web Library - By Jamie Terry ///
/////////////////////////////////////////////////
/// \file Router.cpp
/// \author Jamie Terry
/// \date 2015/08/22
/// \brief Contains implementation of the cppwl::Router class
/// \ingroup route
/////////////////////////////////////////////////

#include <cppwl\route\Router.hpp>
#include <cppwl\route\Route.hpp>
#include <limits>

namespace{
	int nextRouteId = 0;
}

namespace cppwl{
	struct Router::RouteTreeNode{
		///< The child nodes of this one, eg, if the web endpoints are /app/dashboard/
		///< and /app/create/ then the /app/ route has the child routes dashboard and create
		std::vector<RouteTreeNode> childNodes;

		///< The routes that could respond to the url represented by this node,
		///< these must not have duplicate HttpMethods (ie, for a given url only a single route
		///< can respond to each HttpMethod)
		std::vector<Route*> routes;

		///< The raw matcher that was given to this node. For example, /app/thing/{Integer}/
		///< would create the nodes with raw matchers "app", "thing" and "{Integer}"
		std::string rawMatcher;

		///< Regex describing what this part of the url should look like
		std::regex regex;
	};

	Router::Router()
	 : mDisplayErrors(false), mNotFoundResponse(new Response(404)), mFirstGlobalInterceptor(this),
	 mLastGlobalInterceptor(nullptr), mNextRouteId(0), mRootRoute(new RouteTreeNode()){
		//empty body
		this->mRootRoute->rawMatcher = "";
		this->mRootRoute->regex = std::regex("^$");
	}

	Router::~Router(){
		delete this->mNotFoundResponse;
		delete this->mRootRoute;

		//delete the registered interceptors
		Interceptor* next = this->mFirstGlobalInterceptor;
		Interceptor* toDel = nullptr;
		if (next != nullptr){
			while (toDel != mLastGlobalInterceptor){
				toDel = next;
				next = next->mNext;
				delete toDel;
			}
		}
	}

	Response& Router::dispatch(const ReceivedRequest& request){
		try{
			return this->mFirstGlobalInterceptor->intercept(request);
		} catch (std::exception& e){
			Response r(cppwl::HttpStatus::INTERNAL_SERVER_ERROR);
			lie::core::StringBuilder sb;
			sb << "<h1>ERROR 500: Internal Server Error</h1>";
			sb << "<hr>";
			sb << "<div>";
			sb << "Exception thrown while processing request<br>";
			sb << "<pre>" << e.what() << "</pre>";
			sb << "</div>";
			r.setResponseBody(sb.str());
			return r;
		}
	}

	Response& Router::intercept(const ReceivedRequest& request){
		int lowestId = std::numeric_limits<int>::max();
		Route* route = nullptr;
		RouteTreeNode* curNode = this->mRootRoute;
		for (int compIndex = 0; compIndex < request.getUrl().getResourceComponentCount(); ++compIndex){
			std::string component = request.getUrl().getResourceComponent(compIndex);
			if (curNode->rawMatcher == "{**}"){
				//then this node could match the route regardless of remaining components
				for (int r = 0; r < curNode->routes.size(); ++r){
					Route* rt = curNode->routes[r];
					//don't bother continuing as subsequent routes will have higher ids 
					if (rt->mId > lowestId){ break; }
					if (rt->mMethods & request.getMethod() != 0){
						route = rt;
						lowestId = rt->mId;
						break;
					}
				}
			}
			//find the child node that represents the next component
			bool found = false;
			for (int i = 0; i < curNode->childNodes.size(); ++i){
				if (std::regex_match(component, curNode->childNodes[i].regex)){
					curNode = &curNode->childNodes[i];
					found = true;
					break;
				}
			}
			if (!found){
				//Then there was no node for this component, don't process any more (as if no node 
				//for /app/admin/ there wont be a node for /app/admin/users/
				//Also set curNode to nullptr as otherwise the previously used node will be searched for
				//a route, eg, /app/ would be searched for the correct http method if /app/admin/
				//does not exist.
				curNode = nullptr;
				break;
			}
		}

		//curNode will now point to the node that represents the url or nullptr if there is no such node
		//find the route with the correct method registered with curNode
		if (curNode != nullptr){
			for (int i = 0; i < curNode->routes.size(); ++i){
				if (curNode->routes[i]->mId >= lowestId){
					//no point continuing as subsequent roots have even higher ids
					break;
				}
				if ((curNode->routes[i]->mMethods & request.getMethod()) != 0){
					route = curNode->routes[i];
					lowestId = route->mId;
					break;
				}
			}
		}

		if (route == nullptr){
			//then no route found that responds to the request
			return *(new Response(*this->mNotFoundResponse));
		} else {
			return route->go(request);
		}
	}

	void Router::registerInterceptor(Interceptor* interceptor){
		if (this->mLastGlobalInterceptor == nullptr){
			this->mFirstGlobalInterceptor = interceptor;
			this->mLastGlobalInterceptor = interceptor;
		} else {
			this->mLastGlobalInterceptor->mNext = interceptor;
			this->mLastGlobalInterceptor = interceptor;
		}
		interceptor->mNext = this;
	}

	void Router::registerRoute(std::string urlMatcher, Route* route)
		throw(cppwl::ExcepDuplicateRoute, std::regex_error){
		route->mId = this->mNextRouteId++;
		RouteTreeNode* curNode = this->mRootRoute;
		RouteTreeNode* nextNode = nullptr;
		std::vector<std::string> urlComponents;
		lie::core::stringSplit(urlComponents, urlMatcher, '/');

		for (int i = 0; i < urlComponents.size(); ++i){
			nextNode = nullptr;
			std::string component = urlComponents[i];
			//attempt to find an existing node with that url component
			for (int i = 0; i < curNode->childNodes.size(); ++i){
				if (curNode->childNodes[i].rawMatcher == component){
					nextNode = &curNode->childNodes[i];
					break;
				}
			}
			if (nextNode == nullptr){
				//then failed to find existing node with that url component, make new node
				curNode->childNodes.emplace_back();
				nextNode = &curNode->childNodes.back();
				nextNode->rawMatcher = component;
				if (component[0] == '{' && component[component.length() - 1] == '}'){
					//then not a raw component but a 'matcher'
					if (component == "{Integer}"){
						nextNode->regex = std::regex("^[+-]?[0-9]+$");
					} else if (component == "{+Integer}"){
						nextNode->regex = std::regex("^+?[0-9]+$");
					} else if (component == "{-Integer}"){
						nextNode->regex = std::regex("^-[0-9]+$");
					} else if (component == "{*}" || component == "{**}"){
						nextNode->regex = std::regex("^.+$");
					} else {
						//then user specified regex, chop off the { and }
						nextNode->regex = std::regex(component.substr(1, component.length() - 2));
					}
				} else {
					//component is not {...}, interpret it as a raw component to be matched
					//(ie: /{Integer}/ mathces all ints, /app/ matches only "app"
					nextNode->regex = std::regex("^" + component + "$");
				}
			}
			//now have a pointer to nextNode, may or may not be new
			curNode = nextNode;
		}
		//found the current node, add the route
		//check if there is an existing route that responds to the same method
		for (int i = 0; i < curNode->routes.size(); ++i){
			if ((curNode->routes[i]->mMethods & route->mMethods) != 0){
				throw cppwl::ExcepDuplicateRoute(__FUNCTION__, urlMatcher, curNode->routes[i]->mMethods, route->mMethods);
			}
		}
		curNode->routes.push_back(route);
	}

	void Router::setNotFoundResponse(Response& response){
		if (&response == this->mNotFoundResponse){ return; }
		delete this->mNotFoundResponse;
		mNotFoundResponse = &response;
	}

	Response& Router::getNotFoundResponse(){
		return *this->mNotFoundResponse;
	}

	void Router::setDisplayErrors(bool displayErrors){
		this->mDisplayErrors = displayErrors;
	}

	bool Router::getDisplayErrors(){
		return this->mDisplayErrors;
	}
	
}