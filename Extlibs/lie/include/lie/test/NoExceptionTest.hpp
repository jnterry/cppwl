/////////////////////////////////////////////////
//Part of Lithium Engine - By Jamie Terry
/////////////////////////////////////////////////
/// \file NoExceptionTest.hpp
/// \author Jamie Terry
/// \date 2015/08/19
/// \brief Contains the lie::test::NoExceptionTest class
/// \ingroup test
/////////////////////////////////////////////////
#ifndef LIE_TEST_NOEXCEPTIONTEST_HPP
#define LIE_TEST_NOEXCEPTIONTEST_HPP

namespace lie{
	namespace test{

		//////////////////////////////////////////////////////////////////////////
		/// \brief Calls a function in a try catch block, test passes if no exception is thrown, 
		/// else test fails and details are collected about the exception that was thrown
		/// \note This test does not care about the return value hence it is not stored, this
		/// allows the test to allow test a function that has no return (ie: returns void)
		//////////////////////////////////////////////////////////////////////////
		template<typename FuncReturn, typename... FuncArgs>
		class NoExceptionTest : public TestObject{
		public:
			NoExceptionTest(std::string name, std::function<FuncReturn(FuncArgs...)> func, FuncArgs... args)
				: TestObject(name), mCaught(false), mCaughtType(nullptr), mExceptionMsg(""){
				try{
					mReturnValue = func(args...);
				} catch (int e) {
					mCaught = true;
					mCaughtType = &typeid(int);
					mExceptionMsg = lie::core::toString(e);
				} catch (std::exception e){
					mCaught = true;
					mCaughtType = &typeid(std::exception);
					mExceptionMsg = e.what();
				} catch (...) {
					mCaught = true;
					//no way for us to know what type
				}
			}

			/////////////////////////////////////////////////
			/// \brief Returns 1 if this test failed, else returns 0
			/////////////////////////////////////////////////
			unsigned int getFailCount(){
				return this->mCaught ? 1 : 0;
			}

			/////////////////////////////////////////////////
			/// \brief Returns 1 if this test passed, else returns 0
			/////////////////////////////////////////////////
			unsigned int getPassCount(){
				return this->mCaught ? 0 : 1;
			}

		private:
			void getHtmlSummary(std::ostream& ss){
				if (this->getPassCount() == 1){
					ss << "<li class=\"test-pass\">";
				} else {
					ss << "<li class=\"test-fail\">";
				}
				ss << this->getName();
				if (this->getFailCount() != 0){
					ss << "<br>";
					if (this->mCaughtType){
						ss << " - Fail (Caught exception of type: " << this->mCaughtType->name() << ")";
					} else {
						"- Fail (Caught exception of unknown type)";
					}
					if (this->mExceptionMsg != ""){
						ss << "<br><b>Exception Message:</b><br>";
						ss << "<pre>";
						ss << this->mExceptionMsg;
						ss << "</pre>";
					}
				} else {
					ss << " - pass (no exception thrown), function returned: " << mReturnValue;
				}

				ss << std::endl << "</li>" << std::endl;
			}

			///< if true an exception was caught when the function was called
			bool mCaught;

			///< the type of exception that was caught, nullptr if no exception was caught
			///< or the type is not known (ie, caught with catch(...) syntax)
			const std::type_info* mCaughtType;

			///< The value returned by the tested function
			FuncReturn mReturnValue;

			///< The message of the exception, will be empty string if the exception type
			///< is not known or no exception is caught
			std::string mExceptionMsg;
		};


		//specialization for function that has no return value
		template<typename... FuncArgs>
		class NoExceptionTest<void, FuncArgs...> : public TestObject{
		public:
			NoExceptionTest(std::string name, std::function<void(FuncArgs...)> func, FuncArgs... args)
				: TestObject(name), mCaught(false), mCaughtType(nullptr), mExceptionMsg(""){
				try{
					func(args...);
				}
				catch (int e) {
					mCaught = true;
					mCaughtType = &typeid(int);
					mExceptionMsg = lie::core::toString(e);
				}
				catch (std::exception& e){
					mCaught = true;
					mCaughtType = &typeid(std::exception);
					mExceptionMsg = e.what();
				}
				catch (...) {
					mCaught = true;
					//no way for us to know what type
				}
			}

			/////////////////////////////////////////////////
			/// \brief Returns 1 if this test failed, else returns 0
			/////////////////////////////////////////////////
			unsigned int getFailCount(){
				return this->mCaught ? 1 : 0;
			}

			/////////////////////////////////////////////////
			/// \brief Returns 1 if this test passed, else returns 0
			/////////////////////////////////////////////////
			unsigned int getPassCount(){
				return this->mCaught ? 0 : 1;
			}

		private:
			void getHtmlSummary(std::ostream& ss){
				if (this->getPassCount() == 1){
					ss << "<li class=\"test-pass\">";
				}
				else {
					ss << "<li class=\"test-fail\">";
				}
				ss << this->getName();
				if (this->getFailCount() != 0){
					ss << "<br>";
					if (this->mCaughtType){
						ss << " - Fail (Caught exception of type: " << this->mCaughtType->name() << ")";
					}
					else {
						"- Fail (Caught exception of unknown type)";
					}
					if (this->mExceptionMsg != ""){
						ss << "<br><b>Exception Message:</b><br>";
						ss << "<pre>";
						ss << this->mExceptionMsg;
						ss << "</pre>";
					}
				}
				else {
					ss << " - pass (no exception thrown)";
				}

				ss << std::endl << "</li>" << std::endl;
			}

			///< if true an exception was caught when the function was called
			bool mCaught;

			///< the type of exception that was caught, nullptr if no exception was caught
			///< or the type is not known (ie, caught with catch(...) syntax)
			const std::type_info* mCaughtType;

			///< The message of the exception, will be empty string if the exception type
			///< is not known or no exception is caught
			std::string mExceptionMsg;
		};

	}
}

#endif