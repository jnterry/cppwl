#ifndef LIE_CORE_EXCEPTION_HPP
#define LIE_CORE_EXCEPTION_HPP

#include <exception>
#include "Logger.hpp"
#include "StringUtils.hpp"

namespace lie{
    namespace core{

		//////////////////////////////////////////////////////////////////////////
		/// \brief Instances of this class (or instances of derived classes) are thrown
		/// by some methods of Lithium Engine to indicate an error has occurred, it contains
		/// useful functionality for querying information about the error.
		/// It inherits std::exception and henc can be caught by a catch(std::exception)
		/// block, the what() method is overridden to return a report contain as much
		/// information as possible about this Exception
		//////////////////////////////////////////////////////////////////////////
        class Exception : public std::exception{
        public:
			//////////////////////////////////////////////////////////////////////////
			/// \brief Creates a new Exception instance, this should only be called by derived 
			/// classes as they need to specify the "typeName" parameter, use the constructor
			/// with no "typeName" parameter if you manually create an instance of 
			/// lie::core::Exception
			/// \param typeName The name of the the derived class
			/// \param funcName The name of the function throwing the exception, use
			/// the compiler macro __FUNCTION__
			/// \param msg String containing human readable message detailing the
			/// cause of this exception
			/// \param cause Pointer to the exception that caused this exception to
			/// be thrown, defaults to nullptr
			/// \note If the parameter cause is set to anything other than nullptr
			/// the passed in object WILL be modified such that its own cause() is set
			/// to nullptr, this is because the newly constructed exception now owns that
			/// memory
			//////////////////////////////////////////////////////////////////////////
			Exception(std::string typeName, std::string funcName, std::string msg, 
				Exception* cause = nullptr, lie::core::Logger& log = lie::core::DLog);

			//////////////////////////////////////////////////////////////////////////
			/// \brief Creates a new Exception instance with the type set to
			/// "lie::core::Exception"
			////////////////////////////////////////////////////////////////////////// 
			Exception(std::string funcName, std::string msg, 
				Exception* cause = nullptr, lie::core::Logger& log = lie::core::DLog);

			//////////////////////////////////////////////////////////////////////////
			/// \brief Private constructor to copy an exception, does not relog the message
			/// \note This modifies the passed in "other" instance so that its mCause pointer
			/// is set to nullptr, this is because the newly created instance now owns the
			/// memory pointed to by mCause
			////////////////////////////////////////////////////////////////////////// 
			Exception(Exception& other);

			//////////////////////////////////////////////////////////////////////////
			/// \brief Destroys this instance and also deletes the exception pointed to
			/// be cause() if not nullptr
			////////////////////////////////////////////////////////////////////////// 
			~Exception() throw();

			//////////////////////////////////////////////////////////////////////////
			/// \brief Returns the name of the type of exception, for example,
			/// "lie::core::Exception", derived types would return a different name
			////////////////////////////////////////////////////////////////////////// 
			std::string type() const throw();

			//////////////////////////////////////////////////////////////////////////
			/// \brief Returns the human readable message indicating what the problem is
			//////////////////////////////////////////////////////////////////////////
			std::string message() const throw();
			
			//////////////////////////////////////////////////////////////////////////
			/// \brief Returns the name of the function that threw this exception
			//////////////////////////////////////////////////////////////////////////
			std::string function() const throw();

			//////////////////////////////////////////////////////////////////////////
			/// \brief Returns the exception that caused this exception to be thrown,
			/// or nullptr if this exception is not a result of another, for example,
			/// if parsing an IPv4 address (aaa.bbb.ccc.ddd, eg 127.0.0.1) each component
			/// needs to be parsed as an integer, this itself could cause an exception
			/// to be thrown, if this is caught by the IP parsing code it could throw a 
			/// new exception with extra details, the cause of which is the exception
			/// indicating an integer could not be parsed.
			//////////////////////////////////////////////////////////////////////////
			const Exception* cause() const throw();

			//////////////////////////////////////////////////////////////////////////
			/// \brief Generates a report with as much information as possible about
			/// this exception, its causes, etc and returns a pointer to the character
			/// array containing the report. This pointer is guaranteed to be valid
			/// until the destruction of this Exception instance
			//////////////////////////////////////////////////////////////////////////
			const char* what() const throw();
        private:
			///< The name of the class that this instance is
			///< This is NOT a virtual method as we need to be able to copy
			///< exceptions in order to have the mCause pointer (as once caught
			///< the exception would be deleted, need to copy using
			///< mCause = new Exception(original), by having this as a member variable
			///< instead of a virtual type() method the lie::core::Exception class can
			///< imitate any derived class when copying
			std::string mType;

			///< The human readable message detailing the problem
            std::string mMessage;

			///< The name of the function that threw this exception
			std::string mFunction;

			///< Pointer to the exception that caused this one to be thrown, or 
			///< nullptr if this exception was not thrown due to another one
			///< being thrown
			const Exception* mCause;

			///< String to be returned by .what(), needs to be stored as the pointer
			///< returned by .what() is guarrenteed to be valid until the destruction
			///< of this object
			std::string mWhat;
        };

    }
}

#endif // LIE_CORE_EXCEPTION_HPP
