#ifndef LIE_CORE_METATYPEFIELD_HPP
#define LIE_CORE_METATYPEFIELD_HPP

#include "QualifiedMetaType.hpp"
#include <string>

namespace lie{
    namespace core{

        /////////////////////////////////////////////////
        /// \brief Class which represents a single field in a MetaType
        /////////////////////////////////////////////////
        class MetaTypeField{
        public:
            /////////////////////////////////////////////////
            /// \brief Constructor which initialised the fields values to be
            /// passed in type, name, offset and optionally count
            ///
            /// \param type The type that this field stores
            ///
            /// \param name The name of this field
            ///
            /// \param offet The size in bytes between the start of the type and the start of this field
            ///
            /// \param count The number of instances of the type the field holds, defaults to 1, this should only
            /// be changed for a field that is a staitc array (ie: declared as int/type mArray[x]) where x is a compile time constant
            ///
            /// \return new instance of meta type field
            /////////////////////////////////////////////////
            MetaTypeField(QualifiedMetaType type, std::string name, size_t offset, unsigned int count = 1);

            /////////////////////////////////////////////////
            /// \brief Returns the distance in bytes between the first byte of the type this field is in
            /// and the first byte of this field
            ///
            /// eg: type that contains an int, char and short may be layed out in memory as follows:
            /// | int | int | int | int | char | short | short |
            /// The int field has an offset of 0, as it is located at the first byte of the type (ie: byte 0)
            /// The char field has an offset of 4 as it is starts in the 5th byte (byte index 4)
            /// the short field has an offset of 5 as it starts in the 6th byte (byte index 5)
            ///
            /// Note: the compiler with often add padding to types to ensure fields start on x byte alignments
            /// This offset must reflect this to be used correctly (it will if the type is defined using a TypeDefiner)
            ///
            /// return size_t Offset in bytes
            /////////////////////////////////////////////////
            size_t getOffset() const;

            /////////////////////////////////////////////////
            /// \brief Returns the name of the field in the type
            /// Eg: struct Vec3{int x,y,z;};
            /// has fields of the name "x", "y" and "z"
            ///
            /// \return std::string containing the name of the type
            /////////////////////////////////////////////////
            std::string getName() const;

            /////////////////////////////////////////////////
            /// \brief Returns the size in bytes of this field
            /// (ie: size of its type * number of instance of the type it holds)
            ///
            /// \return size_t size of field in bytes
            /////////////////////////////////////////////////
            size_t getSize() const;

            /////////////////////////////////////////////////
            /// \brief Returns the QualifiedMetaType of this field
            /////////////////////////////////////////////////
            const QualifiedMetaType getType() const;

			//////////////////////////////////////////////////////////////////////////
			/// \brief Compares two MetaTypeField instances
			/// \return True if the MetaTypeFields are equivalent, else false
			//////////////////////////////////////////////////////////////////////////
			bool operator=(const MetaTypeField& other) const;

			//////////////////////////////////////////////////////////////////////////
			/// \brief Compares two MetaTypeField instances
			/// \return True if the MetaTypeFields are not the same, else false
			//////////////////////////////////////////////////////////////////////////
			bool operator!=(const MetaTypeField& other) const { return !this->operator=(other); }
        private:
            //field's type, as it points to a meta type, types that contain fields which are non-primative types can be described by the meta type system
            QualifiedMetaType mType;

            //number of objects this field holds, will usally be 1, static arrays (eg: int mArray[5]) will have a count of the arrays size (5 in this case)
            //dynamic arrays whose sizes are not constant between instances of a type (eg: int* mArray; int mArrayLength;) will have a count of 1
            unsigned int mCount;

            //name of the field
            std::string mName;

            //distance in bytes between the start of the type this field belongs to and the first byte of this field
            size_t mOffset;
        };

    }
}

#endif // LIE_CORE_METATYPEFIELD_HPP
