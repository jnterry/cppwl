#ifndef LIE_CORE_RESOURCEHANDLE_HPP
#define LIE_CORE_RESOURCEHANDLE_HPP

#include "Resource.hpp"

namespace lie{
    namespace core{

        /////////////////////////////////////////////////
        /// \brief A ResourceHandle is a speciallised type of smart pointer used to
        /// refer to a resource managed by a ResourceDatabase.
        /// Where as a normal smart pointer will delete a resource when the reference count
        /// reaches 0, a ResourceHandle will notify the ResourceDatabase that the resource is
        /// not in use and, depending on the unload policy of the Resource, it will at some point
        /// be unloaded.
        /// Simerally, if a resource is not yet loaded the ResourceHandle will request it be loaded
        /////////////////////////////////////////////////
        template <typename T>
        class ResourceHandle{
        public:
            ResourceHandle(Resource<T>* res) : mRes(res){
                if(res != nullptr){
                    res->request(); //request the resource is loaded
                }
            }

            ~ResourceHandle(){
                if(mRes != nullptr){
                    //no longer request the resource
                    mRes->release();
                }
            }

            /////////////////////////////////////////////////
            /// \brief Returns true if the resource this handle refers to is loaded,
            /// else returns false
            ///
            /// \note If the resource is set to the default one and the default is not nullptr then
            /// this function will also return true!
            /////////////////////////////////////////////////
            bool isLoaded(){
                if(mRes != nullptr){
                    return mRes->isLoaded();
                }
            }

            /////////////////////////////////////////////////
            /// \brief Returns true if this ResourceHandle refers to an actual Resource,
            /// if it was retrieved from a ResourceDatabase but the Resource did not exist,
            /// this function will return false
            /////////////////////////////////////////////////
            bool isValid(){
                return mRes != nullptr;
            }


            T& operator*(){
                return *(mRes->mInstPtr);
            }

            /////////////////////////////////////////////////
            /// \brief Provides access to the loaded resource
            /////////////////////////////////////////////////
            T* operator->(){
                return mRes->mInstPtr;
            }

            /////////////////////////////////////////////////
            /// \brief Allows a resource handle to be cast to a pointer to
            /// its resource
            /////////////////////////////////////////////////
            operator T*(){
                return this->mRes->mInstPtr;
            }

        private:
            Resource<T>* mRes;
        };

    }
}

#endif // LIE_CORE_RESOURCEHANDLE_HPP
