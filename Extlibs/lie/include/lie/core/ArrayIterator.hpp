#ifndef LIE_CORE_ARRAYITERATOR_HPP
#define LIE_CORE_ARRAYITERATOR_HPP

namespace lie{
    namespace core{

        /////////////////////////////////////////////////
        /// \brief The ArrayIterator is a basic iterator which is returned by
        /// some other lithium engine classes, it allows an array of objects
        /// to be treated like any std container which uses iterators
        ///
        /// \tparam T The type of object stored in the array this iterator refers to
        /////////////////////////////////////////////////
        template <typename T>
        class ArrayIterator{
        public:
            /////////////////////////////////////////////////
            /// \brief Constructs a new iterator from a pointer to an
            /// array's start and optionally the index.
            /// If no index is specified defaults to index 0
            /////////////////////////////////////////////////
            ArrayIterator(T* arrayStart, unsigned int index = 0) : mArrayStart(arrayStart), mCurIndex(index){
                //empty body
            }

            /////////////////////////////////////////////////
            /// \brief Advanced the iterator to the next element of the array
            /////////////////////////////////////////////////
            void operator++(){++mCurIndex;}

            /////////////////////////////////////////////////
            /// \brief Moves the iterator back by one element
            /////////////////////////////////////////////////
            void operator--(){--mCurIndex;}

            /////////////////////////////////////////////////
            /// \brief Returns true if two iterators are equal
            /// Ie: they refer to the same array and the to same index in the array
            /////////////////////////////////////////////////
            bool operator==(const ArrayIterator<T>& other) const{
                return ((this->mArrayStart == other.mArrayStart) &&
                        (this->mCurIndex == other.mCurIndex));
            }

            /////////////////////////////////////////////////
            /// \brief Returns true if two iterators are not equal
            /// Ie: they either refer to different arrays or to different
            /// indexes of the array
            /////////////////////////////////////////////////
            bool operator!=(const ArrayIterator<T>& other) const{
                return !(this->operator==(other));
            }

            /////////////////////////////////////////////////
            /// \brief Returns a pointer to the current element the iterator
            /// refers to
            /////////////////////////////////////////////////
            T* operator->(){
                return &(this->mArrayStart[this->mCurIndex]);
            }

            /////////////////////////////////////////////////
            /// \brief Returns a reference to the current element the iterator
            /// refers to
            /////////////////////////////////////////////////
            T& operator*(){
                return *(this->mArrayStart[this->mCurIndex]);
            }

            /////////////////////////////////////////////////
            /// \brief Allows random access to elements by offset, NOT absolute address.
            /// For example, an iterator refering to the 6th element of an array with [2]
            /// will return the 8th element of the array.
            /// An iterator to the 10th element with [-5] will return the 5th element
            /////////////////////////////////////////////////
            T& operator[](int offset){
                return (this->mArrayStart[this->mCurIndex+offset]);
            }
        private:
            ///pointer to the first element of the array this iterator manages
            T* mArrayStart;

            ///the current index of the array this iterator is refering to
            unsigned int mCurIndex;

        };

    }
}

#endif // LIE_CORE_ARRAYITERATOR_HPP
