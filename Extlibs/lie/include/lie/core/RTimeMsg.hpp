#ifndef LIE_CORE_RTIMEMSG_HPP
#define LIE_CORE_RTIMEMSG_HPP

#include <string>
#include <sstream>
#include <lie/core\Time.hpp>

#define LIE_RTMSG(level, module) lie::core::RTimeMsg(level, module, __FILE__, __LINE__)
#define LIE_RTMSG_TRACE(module) lie::core::RTimeMsg(lie::core::RTimeMsg::Trace, module, __FILE__, __LINE__)
#define LIE_RTMSG_INFO(module) lie::core::RTimeMsg(lie::core::RTimeMsg::Info, module, __FILE__, __LINE__)
#define LIE_RTMSG_REPORT(module) lie::core::RTimeMsg(lie::core::RTimeMsg::Report, module, __FILE__, __LINE__)
#define LIE_RTMSG_WARN(module) lie::core::RTimeMsg(lie::core::RTimeMsg::Warn, module, __FILE__, __LINE__)
#define LIE_RTMSG_ERROR(module) lie::core::RTimeMsg(lie::core::RTimeMsg::Error, module, __FILE__, __LINE__)
#define LIE_RTMSG_FATAL(module) lie::core::RTimeMsg(lie::core::RTimeMsg::Fatal, module, __FILE__, __LINE__)

namespace lie{
    namespace core{

        /////////////////////////////////////////////////
        /// \brief A RTimeMsg contains infomation about something at run time
        /// This can be debug info, errors, warnings, etc
        /// It is not intended that this class be used manually, instead it is used for the log and exceptions
        /// However, it is possible to create instance of it, the macro LIE_RTMSG(level) should be used rather than the constructor
        /// as the macro automaticly fills in the file name and line number
        /////////////////////////////////////////////////
        class RTimeMsg{
            public:
                enum StandardLevels{
                    // Used for messages that do not denote any problems or unusal situations, instead they simply provide context for
                    // surrounding messages, eg when all written to a log file these show where execution is going, things that have succsedded,
                    // etc
                    // eg,  starting main loop
                    //      end of main loop
                    //      begining render
                    //      begining scene update
                    // Trace is for things that occur on a regular basis, Info is for rarer and more significant events
                    Trace = 5,

                    // Gives infomation something has succsedded, more significant than a trace, but still reporting something good rather than bad
                    // Eg: "Rendering System has been initialised, using [API_NAME], version [VERSION]"
                    Info = 55,

                    // Reports an unusual condition, but one which is expected to occur from time to time and causes no problem
                    Report = 105,

                    // Provides a warning that something is wrong but it is unlikely to cause any major problems
                    // eg, there is a missing texture, it will be silently subsituded for a blank one, this will cause graphical
                    // problems but nothing devistaing
                    Warn = 155,

                    // An error has occured that really should not have, eg an invalid value was passed to a function (eg, out of bounds index)
                    // Program execution will continue but garbage data may be produced
                    Error = 205,

                    // Error has occured which means program is unable to continue (eg, unable to find a shader, this obviously means nothing will
                    // be rendered). Alternativly something has occured which may lead to data corrption if execution continues
                    Fatal = 255,
                };

                /////////////////////////////////////////////////
                /// \brief Creates a new run time message with the specified file name,
                /// line number and type
                /////////////////////////////////////////////////
                RTimeMsg(unsigned char level, std::string module, std::string file, unsigned int line);

                /////////////////////////////////////////////////
                /// \brief Copy constructor
                /////////////////////////////////////////////////
                RTimeMsg(const RTimeMsg& other);

                /////////////////////////////////////////////////
                /// \brief Returns the name of the module in which the msg was created
                /////////////////////////////////////////////////
                const std::string& getModule() const;

                /////////////////////////////////////////////////
                /// \brief Returns the name of the file in which the msg was created
                /////////////////////////////////////////////////
                const std::string& getFileName() const;

                /////////////////////////////////////////////////
                /// \brief Returns the line number that the msg was created on
                /////////////////////////////////////////////////
                const unsigned int getLineNumber() const;

                /////////////////////////////////////////////////
                /// \brief Returns the time at which the msg was created
                /////////////////////////////////////////////////
                const lie::core::Time& getTime() const;

                /////////////////////////////////////////////////
                /// \brief Returns the msg level between 0 and 255, see the enum StandardLevels
                /// for the default levels used by the engine
                /////////////////////////////////////////////////
                const unsigned char getLevel() const;

                /////////////////////////////////////////////////
                /// \brief Retunrs the message currently held by the RTimeMsg
                /////////////////////////////////////////////////
                std::string getMessage() const;

                /////////////////////////////////////////////////
                /// \brief Returns a string containing the name of the msg's level
                /// or the level number if the level is not a standard one
                /// eg: "Error", "Info", "Warn", etc
                /////////////////////////////////////////////////
                std::string getLevelString() const;

                /////////////////////////////////////////////////
                /// \brief Retunrns a string containing a formatted name of the msg's level
                /// or the level number if the level is not a standard one, eg:
                /// "[Trace]"
                /// "[ Info]"
                /// "[ Warn]"
                /// "[Reprt]"
                /// "[Error]"
                /// "[Fatal]"
                /// "[# 000]" <- for non standard levels
                /////////////////////////////////////////////////
                std::string getLevelStringFormatted() const;

                template<typename T>
                RTimeMsg& operator<<(T data){
                    mMessage << data;
                    return *this;
                }
            private:
                //namespace/class name
                std::string mModule;

                //name of file where message was created
                std::string mFileName;

                //line number where message was created
                unsigned int mLineNumber;

                //time message was created
                lie::core::Time mTime;

                //level of message, anything between 0 and 255, see enum "StandardLevels"
                unsigned char mLevel;

                //The actual message
                std::stringstream mMessage;
        };

        /////////////////////////////////////////////////
        /// \brief Functor typedef, accepts any function that takes a reference to a RTimeMsg object
        /// and returns a bool.
        /// Used as a filter for RTimeMsg, mainly for logging purposes
        /////////////////////////////////////////////////
        typedef bool (*RTimeMsgFilter)(const RTimeMsg& msg) ;
    }
}

#endif // LIE_CORE_RTIMEMSG_HPP
