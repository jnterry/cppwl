//////////////////////////////////////////////////////////////////
//		      Part of Lithium Engine, by Jamie Terry            //
//////////////////////////////////////////////////////////////////
/// \file Time.h
/// \brief Contains lie::core::Time
///
/// \ingroup core
//////////////////////////////////////////////////////////////////

#ifndef LIE_CORE_TIME_HPP
#define LIE_CORE_TIME_HPP

#include <string>

namespace lie{
    namespace core{

        /////////////////////////////////////////////////
        /// \brief Represents some number of milliseconds since
        /// 1st January 1970.
        /// Very lightweight in terms of memory but contains many
        /// functions for manipulating and retrieving the stored time
        /////////////////////////////////////////////////
        class Time{
        private:
            long long mNanoseconds;
        public:

            Time();

            static const Time Zero;

            ///////////////////////////////////////////////////////////////
            /// When set to true the getDate() function
            /// will return the date in the american format of MM/DD/YY
            /// Otherwise it will use the English format of DD/MM/YY
            //////////////////////////////////////////////////////////////
            static bool useAmericanDateFormat;

            struct Months{
                enum values{
                    January = 1,
                    Febuary = 2,
                    March = 3,
                    April = 4,
                    May = 5,
                    June = 6,
                    July = 7,
                    August = 8,
                    September = 9,
                    October = 10,
                    November = 11,
                    December = 12
                };
            };

            struct Days{
                enum values{
                    Sunday = 1,
                    Monday = 2,
                    Tuesday = 3,
                    Wednesday = 4,
                    Thursday = 5,
                    Friday = 6,
                    Saturday = 7
                };
            };

            //Sets the value based on the number of seconds/microseconds/weeks/etc passed in
			static Time Nanoseconds(long long number);
            static Time Microseconds(double number);
            static Time Milliseconds(double number);
            static Time Seconds(double number);
            static Time Minutes(double number);
            static Time Hours(double number);
            static Time Days(double number);
            static Time Weeks(double number);
            static Time Years(double number);
            ////////////////////////////////////////////////////////////////////////


            ////////////////////////////////////////////////////////////////////////
            //Functions to return the day/minute/seconds etc represented by this
            //object
            //Note: These functions NEVER return fractions, eg if time was 90 seconds
            //get minute would return 1, NOT 1.5. To get fractions use the asMinute()
            //asSecond() etc functions
            short getDayOfMonth() const;
            short getHour24() const;
            short getHour12() const;
            bool isAm() const;
            std::string getAmPm() const;
            short getMonth() const;
            short getMinute() const;
            short getSecond() const;
            short getYear() const;
            short getDayOfWeek() const;
            short getDayOfYear() const;

            ////////////////////////////////////////////////////////////////////////

            ////////////////////////////////////////////////////////////////////////
            //Functions returning a string containing the name of the month or year
            //represented by this instantce
            std::string getDayName() const;
            std::string getMonthName() const;
            std::string getDayNameShort() const;
            std::string getMonthNameShort() const;
            ////////////////////////////////////////////////////////////////////////

            ////////////////////////////////////////////////////////////////////////
            //Returns a string containing the name of the month that is passed in,
            //Month numbers are Jan = 1, Feb = 2 ....  Dec = 12
            //Therefore the enum lie::core::Time::Months::XXXXXXXXX
            //can be used
            static std::string getMonthName(short monthNumber);
            static std::string getMonthNameShort(short monthNumber);
            static std::string getDayName(short dayNumber);
            static std::string getDayNameShort(short dayNumber);
            ////////////////////////////////////////////////////////////////////////

            ///////////////////////////////////////////////////////////
            /// \brief
            ///  returns a string containing the date represented by this
            ///  time object
            ///  Format will depend on whether lie::core::Time::useAmericanDateFormat
            ///  is set to true or false
            ///  The componenets of the date will be seperated by the string passed
            ///  into the function, its defualt is /
            ///  \return
            /// std::string containing date
            ////////////////////////////////////////////////////////////
            std::string getDate(std::string seperatorToken="/") const;

            ////////////////////////////////////////////////////////////////////////
            //Functions to get a string containing the date in various formats
            //Note, lie::core::Time::useAmericanDateFormat is completely igonred and the
            //format in the function name is used instead
            std::string getDateDDMMYYYY(std::string seperatorToken="/") const;
            std::string getDateMMDDYYYY(std::string seperatorToken="/") const;
            std::string getDateDDMMYY(std::string seperatorToken="/") const;
            std::string getDateMMDDYY(std::string seperatorToken="/") const;
            std::string getDateYYYYMMDD(std::string seperatorToken="/") const;
            ///////////////////////////////////////////////////////////////////////

            std::string getTimeHHMMSS(bool use24Hour = true, std::string seperatorToken=":") const;
            std::string getTimeHHMM(bool use24Hour = true, std::string seperatorToken=":") const;



            ////////////////////////////////////////////////////////////////////////
            // Functions to return the value stored in this object as a number of
            // microseconds, milliseconds, seconds, etc
            // Note: These return the exact number, eg 7.345 weeks. The getXXXX()
            // functions return the number of seconds past the mintute, minutes past the
            // hour, hour of day etc and so NEVER return a fraction
            long long asNanoseconds() const;
            double asMicroseconds() const;
            double asMilliseconds() const;
            double asSeconds() const;
            double asMinutes() const;
            double asHours() const;
            double asDays() const;
            double asWeeks() const;
            double asYears() const;
            ////////////////////////////////////////////////////////////////////////

            /////////////////////////////////////////////////////////////////
            /// \brief
            /// Returns a time object with the value of the current system
            ///  time, date, month etc can then be obtained
            ///
            /// \return
            /// Instance of lie::core::Time class with the time set to
            /// the current system time
            /////////////////////////////////////////////////////////////////
            static Time now();

            ////////////////////////////////////////////////////////////////////
            //Functions used to set the time based on a number of seconds, days, milliseconds,
            //weeks etc
            void setAsNanoseconds(long long number);
            void setAsMicroseconds(long double number);
            void setAsMilliseconds(long double number);
            void setAsSeconds(long double number);
            void setAsMinutes(long double number);
            void setAsHours(long double number);
            void setAsDays(long double number);
            void setAsWeeks(long double number);
            void setAsYears(long double number);
            ////////////////////////////////////////////////////////////////////

            Time& operator=(const Time& rhs);
            Time& operator+=(const Time& rhs);
            Time& operator-=(const Time& rhs);
            Time& operator*=(const time_t rhs);
            Time& operator/=(const time_t rhs);
            Time operator+(const Time& rhs);
            Time operator-(const Time& rhs);
            Time operator*(const time_t rhs);
            Time operator/(const time_t rhs);
            bool operator==(const Time& rhs);
            bool operator!=(const Time& rhs);
            bool operator<(const Time& rhs);
            bool operator>(const Time& rhs);
            bool operator<=(const Time& rhs);
            bool operator>=(const Time& rhs);
        };
    }
}




#endif
