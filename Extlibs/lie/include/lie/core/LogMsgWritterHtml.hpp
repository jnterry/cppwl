#ifndef LIE_CORE_LOGMSGWRITTERHTML_HPP
#define LIE_CORE_LOGMSGWRITTERHTML_HPP

#include <lie/core\LogMsgWritterBase.hpp>
#include <fstream>

namespace lie{
    namespace core{

        class LogMsgWritterHtml : public LogMsgWritterBase{
        public:
            void onWrite(const RTimeMsg& msg);

            /////////////////////////////////////////////////
            /// \brief Opens specified file and prepares it for logging
            /////////////////////////////////////////////////
            LogMsgWritterHtml(std::string filename);

            /////////////////////////////////////////////////
            /// \brief Closes the file
            /////////////////////////////////////////////////
            ~LogMsgWritterHtml();
        private:
            std::ofstream mFile;
        };

    }
}

#endif // LIE_CORE_LogMsgWritterHTML_HPP
