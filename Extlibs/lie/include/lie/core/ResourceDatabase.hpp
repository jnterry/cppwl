#ifndef LIE_CORE_RESOURCEDATABASE_HPP
#define LIE_CORE_RESOURCEDATABASE_HPP

#include "ResourceHandle.hpp"
#include "Logger.hpp"
#include <map>
#include <functional>
#include <typeinfo>
#include <unordered_set>


namespace lie{
    namespace core{

        //forward decleration
        template <typename T>
        class Resource;

        /////////////////////////////////////////////////
        /// \brief A ResourceDatabase manages a set of instances of the Resource class and
        /// allows client code to retrieve resources by name.
        /// It is also responcible for keeping track of which resources need to be loaded and which unloaded
        /// <br><br>
        /// In the majority of applications only one instance of this class will ever be needed as resources should
        /// only ever be loaded once at a time, however this class is NOT a singleton and does not have static state so
        /// multiple instances can be created if desired. For example, it may be useful to have a ResourceDatabase
		/// that loads global resources used for the entire application's life time and another for loading
		/// resources specific to a level/area of the game
        /// <br><br>
        /////////////////////////////////////////////////
        class ResourceDatabase{
        public:
            /////////////////////////////////////////////////
            /// \brief A ResourceLoader is any callable object (lambda, function, static method, etc)
            /// which takes a const File& and unsigned int byteOffset.
            /// It should return a pointer to the type it loads, if loading fails it should return nullptr
            /////////////////////////////////////////////////
            template <typename T>
            using ResourceLoader = std::function<T(lie::core::File& file, unsigned int byteOffset)>;

            /////////////////////////////////////////////////
            /// \brief Seaches for the Resource of the specified type and of the specifed name and returns a handle to it
            ///
            /// If the resource does not exist the returned handle will not be valid, use ResourceHandle::isValid() to check
            ///
            /// \tparam T The type of resource to be found, this must be exactly the type added to the ResourceDatabase, normal
            /// implicit casting will not work, eg: a short cannot be retrieved as an int
            ///
            /// \param name A string holding the name of the resource to be retrieved
            ///
            /// \return ResourceHandle<T> refering to the requested resource
            ///
            /// \note If the resource name is known at compile time then you should hash the name
            /// at compile time and pass it to the overloaded version taking a lie::core::HashValue
            /// Eg:
            /// \code getHandle<lie::rend::Mesh>("awesome-mesh");
            /// \endcode
            /// should become
            /// \code getHandle<lie::rend::Mesh>(lie::core::Hash("awesome-mesh"));
            /// \endcode
            /// as the second example will find the hash at compile time and so will be faster
            /////////////////////////////////////////////////
            template <typename T>
            ResourceHandle<T> getHandle(std::string name){
                return this->getHandle<T>(this->getResource<T>(name));
            }

            /////////////////////////////////////////////////
            /// \brief Searches for the Resource of the specified type and with the specifed hash of the resource's name
            /// and returns a handle to it
            ///
            /// If the resource does not exist the returned handle will not be valid, use ResourceHandle::isValid() to check
            ///
            /// \tparam T The type of resource to be found, this must be exactly the type added to the ResourceDatabase, normal
            /// implicit casting will not work, eg: a short cannot be retrieved as an int
            ///
            /// \param name A string holding the name of the resource to be retrieved
            ///
            /// \return ResourceHandle<T> refering to the requested resource
            /////////////////////////////////////////////////
            template <typename T>
            ResourceHandle<T> getHandle(lie::core::HashValue nameHash){
                return ResourceHandle<T>(this->getResource<T>(nameHash));
            }

            /////////////////////////////////////////////////
            /// \brief Returns a pointer to the instance of Resource with the specified name and type
            ///
            /// If the resource does not exist, returns nullptr
            ///
            /// \tparam T The type of resource to retrieve
            ///
            /// \param name A string holding the name of the resource to retrieve
            ///
            /// \return A pointer to the Resource<T> object holding details on the resource
            ///
            /// \note In any instance where you wish to actually use the resource you should call getHandle,
            /// The main use for this function being exposed publically is so you can retrieve a Resource object
            /// and cache, you can then later make handles from it. Searching for a resource is a slowish operation
            /// so this can be used to find it in advance without loading it, retrieving a handle will also request that the
            /// resource be loaded
            ///
            /// \note If the resource name is known at compile time then you should hash the name
            /// at compile time and pass it to the overloaded version taking a lie::core::HashValue
            /// Eg:
            /// \code getResource<lie::rend::Mesh>("awesome-mesh");
            /// \endcode
            /// should become
            /// \code getResource<lie::rend::Mesh>(lie::core::Hash("awesome-mesh"));
            /// \endcode
            /// as the second example will find the hash at compile time and so will be faster
            /// \see getHandle
            /////////////////////////////////////////////////
            template <typename T>
            Resource<T>* getResource(std::string name){
                return this->getResource<T>(lie::core::hashString(name.c_str()));
            }

            /////////////////////////////////////////////////
            /// \brief Returns a pointer to the instance of Resource with the specified name and type
            ///
            /// If the resource does not exist, returns nullptr
            ///
            /// \tparam T The type of resource to retrieve
            ///
            /// \param nameHash The hash of the resource's name, can be retrived with lie::core::hashString(...)
            ///
            /// \return A pointer to the Resource<T> object holding details on the resource
            /////////////////////////////////////////////////
            template <typename T>
            Resource<T>* getResource(lie::core::HashValue nameHash){
                DatabaseOuterType::iterator typeMapIter = this->mDatabase.find(typeid(T).hash_code());
                if(typeMapIter != this->mDatabase.end()){
                    DatabaseInnerType::iterator resIter = typeMapIter->second.find(nameHash);
                    if(resIter != typeMapIter->second.end()){
                        //then we have found the resource, cast it from a Resource<void>* into a Resource<T>*
                        return reinterpret_cast<Resource<T>* >(resIter->second);
                    } else {
                        //there is a map for resources of type T, but there is not one with the name-hash of "nameHash"
                        LIE_DLOG_WARN("ResourceDatabase", "Tried to retrieve resource of type name: " << typeid(T).name() << " (hash: " <<
                                      typeid(T).hash_code() <<") with a name hash of " << nameHash << " but it does not exist. Returning invalid ResourceHandle");
                        return nullptr;
                    }
                } else {
                    //then there is no map for resources of type T, none have been added to this resource database
                    LIE_DLOG_WARN("ResourceDatabase", "Tried to retrieve resource of type name: " << typeid(T).name() << " (hash: " <<
                                      typeid(T).hash_code() <<") with a name hash of " << nameHash << " but it does not exist. Returning invalid ResourceHandle");
                    return nullptr;
                }
            }

            /////////////////////////////////////////////////
            /// \brief Adds the specified Resource to this ResourceDatabase
            /// The loading and unloading of the resource will now be handled
            /// by the ResourceDatabase, a handle to it can be retrieved with getResource
            ///
            /// \note ResourceLoad policies not currently implemented
            /// Hence this function will also force load the added resource
            /// :TODO:
            /////////////////////////////////////////////////
            template <typename T>
            void addResource(Resource<T>* res){
                //....get the map for that type........and then the location for that res name
                this->mDatabase[typeid(T).hash_code()][res->getHash()] =
                    reinterpret_cast<Resource<void>* >(res); //set it to the res passed in
                this->forceLoad(res);
            }

            /////////////////////////////////////////////////
            /// \brief Registers a function which will load and return the resource type given as the template parameter
            ///
            /// \param name The name of the loader to be registered, it is suggested this should be the file extension
            /// eg: a image loader that loads .pngs should have the name "png" however it does not have to be. It must however
            /// match the name of the loader supplied to Resource objects
            ///
            /// \param loader A function which takes a lie::core::File and a byteOffset and returns a pointer to a new instance of the
            /// resource type. The function should call new to create the instance and then return a pointer, upon return the lifetime of
            /// the instance is controlled by the ResourceDatabase. If nullptr is returned then it is assumed the load failed.
            ///
            /// \return T* pointer to the new instance of type T
            /////////////////////////////////////////////////
            template <typename T>
            void registerLoader(std::string name, ResourceLoader<T*> loader){
                lie::core::HashValue nameHash = lie::core::hashString(name.c_str());
                auto loadIter = this->mLoaders.find(nameHash);
                if(loadIter != this->mLoaders.end()){
                    LIE_DLOG_REPORT("ResourceDatabase", "Tried to register resource loader with the name \"" << name << "\" but an existing loader \
                                    with the same hash exists, replacing old one. Either you are registering the loader twice or it is a hash collision, \
                                    if so expect resource loading errors");
                    //we make a copy when setting, so remember to delete the old copy which we control!
                    //we need to cast it to the correct type though..., if the above log msg is the last thing shown before crash
                    //then the cast and delete may be doing so odd, this would happen if we cast to the wrong type because this is a
                    //hash collision and the previously registered loader was not of type T in this func
                    delete reinterpret_cast<ResourceLoader<T*>*>(loadIter->second);
                }
                ResourceLoader<T*>* loaderCopy = new ResourceLoader<T*>(loader);
                this->mLoaders[nameHash] = reinterpret_cast<void*>(loaderCopy);
                LIE_DLOG_INFO("ResourceDatabase", "New resource loader registered for file extension \"" << name
                              << "\", resource type name: " << typeid(T).name() << ", resource typename hash: " << typeid(T).hash_code());

                //if this is the first loader for this resource type then set the unloaded resource instance to nullptr
                if(this->mDefaultResources.find(typeid(T).hash_code()) == this->mDefaultResources.end()){
                    LIE_DLOG_INFO("ResourceDatabase", "New resource type registered, type name: " << typeid(T).name() << ", hash: " << typeid(T).hash_code());
                    this->mDefaultResources[typeid(T).hash_code()] = nullptr;
                }
            }

            /////////////////////////////////////////////////
            /// \brief If a resource is accessed when unloaded we will try to dereference a nullptr
            /// which will crash the program, instead it may be prefered to use some other instance of the class
            /// Eg: a texture could be replaced with a texture containing the text "Not Loaded" to indicate something is
            /// wrong while allowing the application to continue. If it then loaded a couple of seconds later it will automatically
            /// switch to the actual resource.
            /// With this function you can set the unloaded instance for a resource type. By defualt it is set to "nullptr", which will
            /// crash the application
            /// Some lithium engine classes have a static "default" instance that can be used here
            ///
            /// \tparam The type of resource to specifiy the default for
            ///
            /// \param A pointer to the instance of the resource to use as the default
            ///
            /// \note YOU are responcible for the lifetime of the passed in instance, NOT the ResourceDatabase!
            /////////////////////////////////////////////////
            template <typename T>
            void setUnloadedInstance(T* defaultInst){
                this->mDefaultResources[typeid(T).hash_code()] = defaultInst;
            }

            /////////////////////////////////////////////////
            /// \brief Immediately attempts loads the specified resource in the calling thread
            ///
            /// \return True if the resource was succsessfully loaded, else false
            /////////////////////////////////////////////////
            template<typename T>
            bool forceLoad(Resource<T>* res){
                if(!res->isLoaded()){
                    //then resource is not loaded, see if we can find an appropriate loader
                    LoaderMapType::iterator loadIter = this->mLoaders.find(res->mLoaderNameHash);
                    if(loadIter == this->mLoaders.end()){
                        //then we failed to find an appropiate loader
                        LIE_DLOG_REPORT("ResourceDatabase", "Tried to load the resource \"" << res->getName() << "\" (hash: " << res->getHash() <<
                                        ") but no appropriate loader was found to load the file " << res->mFile.getFullPath());
                        return false;
                    } else {
                        //set the instance pointer to.......................................................the result of calling the loader
                        //..............get the loader by casting it from void* to get a pointer to a lie::core::ResourceDatabase::ResourceLoader
                        //..................................................................call it with this weird syntax of ->operator()(params)
                        res->mInstPtr = reinterpret_cast<ResourceLoader<T*>*>(loadIter->second)->operator()(res->mFile, res->mFileByteOffset);

                        //recheck if the resource is now loaded, the above loader may have returned nullptr if the resource could not be loaded
                        if(res->isLoaded()){
                            return true;
                        } else {
                            DefaultResMapType::iterator defIter = this->mDefaultResources.find(typeid(T).hash_code());
                            if(defIter->second == nullptr){
                                LIE_DLOG_WARN("ResourceDatabase", "Failed to load the resource \"" << res->getName() << "\" (hash: " << res->getHash() <<
                                        ") from \"" << res->mFile.getFullPath() << "\", byte offset: " << res->mFileByteOffset <<
                                        ", no default set, setting resource to nullptr");
                            } else {
                                LIE_DLOG_WARN("ResourceDatabase", "Failed to load the resource \"" << res->getName() << "\" (hash: " << res->getHash() <<
                                        ") from \"" << res->mFile.getFullPath() << "\", byte offset: " << res->mFileByteOffset <<
                                        ", substituting for default resource");
                                res->mInstPtr = reinterpret_cast<T*>(defIter->second);
                            }
                            return false;
                        }
                    }
                } else {
                    //already loaded
                    return true;
                }
            }
        private:
            //save some typing...
            using DatabaseInnerType = std::map<lie::core::HashValue, Resource<void>*>;
            using DatabaseOuterType = std::map<size_t, DatabaseInnerType>;
            using LoaderMapType = std::map<lie::core::HashValue, void*>;
            using DefaultResMapType = std::map<size_t, void*>;

            /////////////////////////////////////////////////
            /// \brief Outer map maps a type_info::hash_code to a std::map
            /// the inner map then maps hashed resource names into the actual resource object
            /// we cast from the actual type to Resource<void> and back when retrieving/storing resources
            /////////////////////////////////////////////////
            DatabaseOuterType mDatabase;

            ///< Maps a string holding a file extension (txt, png, doc, etc) to a ResourceLoader of any type (void*)
            LoaderMapType mLoaders;

            ///< Maps a typeid().hash_code() to a pointer to the default instance of that resource type
            /// the default is used whenever an unloaded resource is accsessed
            DefaultResMapType mDefaultResources;
        };

    }
}

#endif // LIE_CORE_RESOURCEDATABASE_HPP

