#ifndef LIE_CORE_TIMER_HPP
#define LIE_CORE_TIMER_HPP

#include "Time.hpp"

namespace lie{
    namespace core{


        /////////////////////////////////////////////////
        /// \brief Very simple utility class which allows you to time how
        /// long something takes
        /// Resolution of timer depends on OS and system
        /////////////////////////////////////////////////
        class Timer{
        public:
            /////////////////////////////////////////////////
            /// \brief Creates new timer object, immediately resets the timer
            /////////////////////////////////////////////////
            Timer(){
                restart();
            }

            /////////////////////////////////////////////////
            /// \brief Resets the timer, the elapsed time will be 0 (ish)
            /////////////////////////////////////////////////
            void restart(){
                mStartTime = lie::core::Time::now();
            }

            /////////////////////////////////////////////////
            /// \brief Returns the time elapsed since the last call to getElapsedTime
            /////////////////////////////////////////////////
            lie::core::Time getElapsedTime() const{
                return lie::core::Time::now() - this->mStartTime;
            }

        private:
            lie::core::Time mStartTime;
        };

    }
}

#endif // LIE_CORE_TIMER_HPP
