#ifndef LIE_CORE_LOGMSGWRITTERCOUT_HPP
#define LIE_CORE_LOGMSGWRITTERCOUT_HPP

#include <lie/core\LogMsgWritterBase.hpp>

namespace lie{
    namespace core{

        class LogMsgWritterCout : public LogMsgWritterBase{
        public:
            void onWrite(const RTimeMsg& msg);
            ~LogMsgWritterCout(){}
        };
    }
}


#endif // LIE_CORE_LogMsgWritterCOUT_HPP
