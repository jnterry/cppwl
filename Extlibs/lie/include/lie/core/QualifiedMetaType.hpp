#ifndef LIE_CORE_QUALIFIEDMETATYPE_HPP
#define LIE_CORE_QUALIFIEDMETATYPE_HPP

#include <cstddef>

namespace lie{
    namespace core{

        //forward decleration
        class MetaType;

        /////////////////////////////////////////////////
        /// \brief This class stores both a meta type and qualifiers for it, ie:
        /// its level of pointer indirection and if it is a reference.
        ///
        /// It is needed because MetaType is immutiable and only one instance should exist
        /// for any type.
        /// Pointers/references to a type do not change its layout in memory.
        /// Eg a struct Vec3{int x,y,z;}; is comprised of 3 int fields regardless of whether
        /// we have a Vec3, Vec3*, Vec3**, Vec3& etc.
        /// The do however need to be treated differently when getting their value/during serialisation/etc
        /// Therefore to avoid having a MetaType for every quallification of a type we instead make new instances
        /// of the "QualifiedMetaType" class which will automatically handle the differences between pointers, references,
        /// pointers to pointers etc
        /////////////////////////////////////////////////
        class QualifiedMetaType{
        public:
            /////////////////////////////////////////////////
            /// \brief Constructor that creates a new QualifiedMetaType from a base type,
            /// pointer indirection level and bool as to whether the type is a reference or not
            /// pointer indirecion level defaults to 0 and is ref default to false
            /////////////////////////////////////////////////
            QualifiedMetaType(const MetaType& type, unsigned char ptrLevel = 0, bool isRef = false);

            const MetaType& getBaseType() const;

            /////////////////////////////////////////////////
            /// \brief Returns the level of pointer indirection of this type, eg:
            /// int -> 0
            /// int* -> 1
            /// int** -> 2
            /// \return unsigned char representing the indirection level
            /////////////////////////////////////////////////
            unsigned char getPtrIndirection() const;

            /////////////////////////////////////////////////
            /// \brief Returns true if this is a reference or reference to pointer
            /////////////////////////////////////////////////
            bool isReference() const;

			//////////////////////////////////////////////////////////////////////////
			/// \brief returns the compile time known size of this type, if is a pointer or reference will
			/// return the size of a pointer/reference, returns the size of the underlying MetaType
			//////////////////////////////////////////////////////////////////////////
            size_t getStaticSize() const;

			//////////////////////////////////////////////////////////////////////////
			/// \brief Returns true if two QualifiedMetaType instances represent the same type,
			/// else returns false
			//////////////////////////////////////////////////////////////////////////
			bool operator==(const QualifiedMetaType& other) const;

			//////////////////////////////////////////////////////////////////////////
			/// \brief Returns true if two QualifiedMetaType instances do not represent the same type,
			/// else returns false
			//////////////////////////////////////////////////////////////////////////
			bool operator!=(const QualifiedMetaType& other) const{ return !this->operator==(other); }
		private:
            //the actual type that this qualifiers
            const MetaType& mBaseType;

            //unsigned char representing the number of pointer indirections and whether the type is
            //a reference
            //top bit used to store if it is a reference
            //bottom 7 bits used to store pointer indirection level,
            //if anyone goes above 127 layers of indirection then this wont work... but why would you?
            unsigned char mQualifiers;
        };

    }
}

#endif // LIE_CORE_QUALIFIEDMETATYPE_HPP
