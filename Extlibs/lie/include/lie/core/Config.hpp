/////////////////////////////////////////////////
//Part of Lithium Engine - By Jamie Terry
/////////////////////////////////////////////////
/// \file Config.hpp
/// Contains preprocessor ifs etc to determine various properties about the
/// platform you are building lithium engine for (eg, OS, endianess, etc)
/// and about the build type (debug or not)
///
/// In some cases the build type and/or platform infomation cannot be automatically
/// deterimed, in these cases a compile time error will occur and will need to define
/// something yourself.
///
///
///
/// \ingroup core
/////////////////////////////////////////////////

#ifndef LIE_CORE_CONFIG_HPP
#define LIE_CORE_CONFIG_HPP

    #if defined(_WIN32) || defined(__WIN32__)
        // Windows
        #define LIE_OS_WINDOWS
    #elif defined(linux) || defined(__linux)
        // Linux
        #define LIE_OS_LINUX
    #elif defined(__APPLE__) || defined(MACOSX) || defined(macintosh) || defined(Macintosh)
        // MacOS
        #define LIE_OS_MAC
    #elif defined(__FreeBSD__) || defined(__FreeBSD_kernel__)
        // FreeBSD
        #define LIE_OS_FREEBSD
    #else
        // Unsupported system
        #error This operating system is not supported by Lithium Engine
    #endif

    #if !defined(LIE_DEBUG) && !defined(LIE_NDEBUG)
        //then try to determine which build mode we are in
        #if defined(_DEBUG) || defined(DEBUG)
            #define LIE_DEBUG
        #elif defined(NDEBUG)
            #define LIE_NDEBUG
        #else
            #error Cannot determine build type, please define either LIE_DEBUG or LIE_NDEBUG
        #endif
    #elif defined(LIE_DEBUG) && defined(LIE_NDEBUG)
        #error You must only define one of LIE_DEBUG or LIE_NDEBUG
    #endif

    #if !defined(LIE_BIG_ENDIAN) && !defined(LIE_LITTLE_ENDIAN)
        //#error You must define one of "LIE_BIG_ENDIAN" and "LIE_LITTLE_ENDIAN"
    #elif defined(LIE_DEBUG) && defined(LIE_NDEBUG)
        #error You must only define one of "LIE_BIG_ENDIAN" and "LIE_LITTLE_ENDIAN"
    #endif


    //namespace lie{
        //#ifdef LIE_DEBUG
        //std::string buildType = "Debug";
        //#elif defined(LIE_NDEBUG)
        //std::string::buildType = "NDebug";
        //#endif
    //}



#endif // LIE_CORE_CONFIG_HPP


