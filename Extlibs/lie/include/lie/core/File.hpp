#ifndef LIE_CORE_FILE_HPP
#define LIE_CORE_FILE_HPP
#include <lie/core\Folder.hpp>

#include <string>

namespace lie{
    namespace core{

        /////////////////////////////////////////////////
        /// \brief Represents some file that may or may not physically exist
        /// Made up of a lie::core::Folder and a filename
        ///
        /// \param
        /// \param
        /// \return
        ///
        /////////////////////////////////////////////////

        class File{
        public:
            /////////////////////////////////////////////////
            /// \brief Creates a new file object from a Folder and full
            /// file name (eg: Name.extension)
            /////////////////////////////////////////////////
            File(const Folder& dir, std::string fullname);

            /////////////////////////////////////////////////
            /// \brief Creates a new file object from a Folder and a seperate
            /// file name and file extension
            /////////////////////////////////////////////////
            File(const Folder& dir, std::string filename, std::string extension);

            /////////////////////////////////////////////////
            /// \brief Creates a new file object from a string containing the full path to the
            /// file
            /////////////////////////////////////////////////
            File(const std::string& path);

            /////////////////////////////////////////////////
            /// \brief Returns a file object refering to the currently running executable file
            /////////////////////////////////////////////////
            static File getExecutable();

            /////////////////////////////////////////////////
            /// \brief returns a string holding the name of the file and then the extension,
            /// seperated by a dot (NAME.EXTENSION)
            /////////////////////////////////////////////////
            std::string getFullName() const;

            /////////////////////////////////////////////////
            /// \brief returns the files name without the extension
            /////////////////////////////////////////////////
            std::string getName() const;

            /////////////////////////////////////////////////
            /// \brief returns the extension of the file
            /////////////////////////////////////////////////
            std::string getExtension() const;

            /////////////////////////////////////////////////
            /// \brief Sets the name of the file without modifing the extension
            /////////////////////////////////////////////////
            void setName(std::string name);

            /////////////////////////////////////////////////
            /// \brief Sets the extension of the file without modifing the name
            /////////////////////////////////////////////////
            void setExtension(std::string ext);

            /////////////////////////////////////////////////
            /// \brief Sets the full name (ie: name and extension) of the file
            /// Expects a string containing the file's name followed by a dot and then
            /// the extension
            /// \note The last dot is interpreted as the extension seperator. Eg: test.abc.txt
            /// will be interpreted as the name "test.abc" with the extension "txt"
            /////////////////////////////////////////////////
            void setFullName(std::string name);

            /////////////////////////////////////////////////
            /// \brief Returns a non const reference to the Folder of this file,
            /// use it to edit the Folder of the file
            /////////////////////////////////////////////////
            Folder& getFolder();

            /////////////////////////////////////////////////
            /// \brief Sets the Folder of this instance of file to a deep copy of the passed in
            /// Folder object
            /////////////////////////////////////////////////
            void setFolder(Folder dir);

            /////////////////////////////////////////////////
            /// \brief Returns the full path of the file as a string in the specfied path style,
            /// style defaults to the native system's path style
            /////////////////////////////////////////////////
            std::string getFullPath(lie::core::PathStyle style = lie::core::PathStyle::Native) const;

            /////////////////////////////////////////////////
            /// \brief Returns true if this file actually exists on the system
            ///
            /// \return true The file exists
            /// false The file does not physically exist
            ///
            /// \note If a folder with the same path and name exists this function will still return false!
            /////////////////////////////////////////////////
            bool exists();

            /////////////////////////////////////////////////
            /// \brief If the file does not exist attempts to create it
            ///
            /// \param createParents If true then will also attempt to create any non existant
            /// parent folders
            ///
            /// \return false The file or its parent folders could not be created
            /// true The file was succsessfully created
            ///
            /// \note It is possible for some or all of the parent folders to be created and then the file
            /// to not be, in this case false will be returned  however the created parent folders will NOT be
            /// deleted
            /////////////////////////////////////////////////
            bool create(bool createParents = true);

            /////////////////////////////////////////////////
            /// \brief Returns true if two instances of File refer to the same file path
            /////////////////////////////////////////////////
            bool operator==(const File& other) const;

            /////////////////////////////////////////////////
            /// \brief Returns true if two instances of File refer to different file paths
            /////////////////////////////////////////////////
            bool operator!=(const File& other) const {return !this->operator==(other);}
        private:
            lie::core::Folder mDir;
            std::string mName;
            std::string mExtension;
        };
    }
}

#endif // LIE_CORE_FILE_HPP
