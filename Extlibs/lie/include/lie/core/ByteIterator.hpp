#ifndef LIE_CORE_BYTEITERATOR_HPP
#define LIE_CORE_BYTEITERATOR_HPP

namespace lie{
    namespace core{

        ///A unsigned type guarrentied to hold 8 bits, used by ByteIterator,
        ///ByteBuffer and bytesToType
        typedef uint8_t byte_t;

        /////////////////////////////////////////////////
        /// \brief Takes a pointer to an array of bytes and converts that
        /// and the next sizeof(T_Type)-1 bytes into some type.
        ///
        /// Returns a reference, hence bytesToType<int32>(byteArray) = 6
        /// will modify 4 bytes in the byteArray
        ///
        /// \note This function is only guarrentied to work on primative types
        /// and types which contain only primative types, pointers and references
        /// are not gaurrentied to work, even if they do they will obviously  be
        /// invalid if what they refered to has been deleted
        /////////////////////////////////////////////////
        template<typename T_Type>
        T_Type& bytesToType(byte_t* bytes){
            //:TODO: Not sure if this is portable...
            return *((T_Type*)bytes);
		}

		/////////////////////////////////////////////////
		/// \brief Takes a pointer to an array of bytes and converts that
		/// and the next sizeof(T_Type)-1 bytes into some type.
		///
		/// Returns a reference, hence bytesToType<int32>(byteArray) = 6
		/// will modify 4 bytes in the byteArray
		///
		/// \note This function is only guarrentied to work on primative types
		/// and types which contain only primative types, pointers and references
		/// are not gaurrentied to work, even if they do they will obviously  be
		/// invalid if what they refered to has been deleted
		/////////////////////////////////////////////////
		template<typename T_Type>
		const T_Type& bytesToType(const byte_t* bytes){
			//:TODO: Not sure if this is portable...
			return *((T_Type*)bytes);
		}

        /////////////////////////////////////////////////
        /// \brief Used to extract instances of a certain type from an array of bytes (ie: uint8_t).
        /// This is suitible for traversing a ByteBuffer or an array of uint8_t.
        ///
        /// \tparam T The type of element that will be read by the iterator,
        /// eg, an iterator<float> will interpret the data in the buffer as an
        /// array of floats.
        ///
        /// \tparam T_Stride This parameter specifies how many bytes the iterator should move
        /// by when using operator++, --, [], etc
        /// By defualt this is set to sizeof(T), hence for an iterator reading int32s it will advance
        /// by 4 bytes per call to ++ or --
        /// However, in some cases it may be helpful to advance by some other number of bytes.
        /// There are two main use cases for this.
        /// - Use 1: You want to search for an x byte pattern which may not start on an x-byte interval,
        ///   A eg, in a ByteBuffer that has a short and then int appended to it the int will NOT start
        ///   at a multiple of sizeof(int), hence to find it you must advance by single bytes
        /// - Use 2: You want to skip some number of bytes between reading elements. For example, in a
        ///   ByteBuffer which has had an int32 appended, then a int16, then int32, int16, int32, int16, etc
        ///   all of the int32s can be extracted with an iterator<int32, 6>. The iterator will advance by
        ///   6 bytes per ++, -- etc, hence the 2 byte int16s will be skipped and only the ints will be read
        ///   In other words, this allows for a custom stride between elements, see
        ///   http://en.wikipedia.org/wiki/Stride_of_an_array
        /////////////////////////////////////////////////T_Type
        template<typename T_Type, unsigned int T_Stride = sizeof(T_Type)>
        class ByteIterator{
        public:
            /////////////////////////////////////////////////
            /// \brief Constructs a new iterator from a reference to the
            /// buffer that should be read from an the index at which to start
            /////////////////////////////////////////////////
            ByteIterator(byte_t* byteArray, unsigned int index = 0) : mByteArray(byteArray), mCurIndex(index){
                //empty body
            }

            /////////////////////////////////////////////////
            /// \brief Advanced the iterator to the next element of type T
            /// \note The amount of bytes moved depends on the iterators type
            /// eg, for an int32 a single ++ will move forward by 4 bytes,
            /// for an int16 it will only move forward by 2 bytes
            /////////////////////////////////////////////////
            ByteIterator<T_Type, T_Stride>& operator++(){
                mCurIndex+=T_Stride;
                return *this;
            }

            /////////////////////////////////////////////////
            /// \brief Advanced the iterator to the next element of type T
            /// \note The amount of bytes moved depends on the iterators type
            /// eg, for an int32 a single ++ will move forward by 4 bytes,
            /// for an int16 it will only move forward by 2 bytes
            /////////////////////////////////////////////////
            ByteIterator<T_Type, T_Stride> operator++(int dummyParamForPostfix){
                ByteIterator<T_Type, T_Stride> iter = *this;
                ++(*this);
                return iter;
            }

            /////////////////////////////////////////////////
            /// \brief Moves the iterator back by one element of type T
            /// \note The amount of bytes moved depends on the iterators type
            /// eg, for an int32 a single -- will move back by 4 bytes,
            /// for an int16 it will only move back by 2 bytes
            /////////////////////////////////////////////////
            ByteIterator<T_Type, T_Stride>& operator--(){
                mCurIndex-=T_Stride;
                return *this;
            }

            /////////////////////////////////////////////////
            /// \brief Moves the iterator back by one element of type T
            /// \note The amount of bytes moved depends on the iterators type
            /// eg, for an int32 a single -- will move back by 4 bytes,
            /// for an int16 it will only move back by 2 bytes
            /////////////////////////////////////////////////
            ByteIterator<T_Type, T_Stride> operator--(int dummyParamForPostfix){
                ByteIterator<T_Type, T_Stride> iter = *this;
                --(*this);
                return iter;
            }

            /////////////////////////////////////////////////
            /// \brief Returns true if two iterators are equal
            /// Ie: they refer to the same array and the to same index in the array
            /////////////////////////////////////////////////
            bool operator==(const ByteIterator<T_Type, T_Stride>& other) const{
                return ((this->mByteArray == other.mByteArray) &&
                        (this->mCurIndex == other.mCurIndex));
            }

            /////////////////////////////////////////////////
            /// \brief Returns true if two iterators are not equal
            /// Ie: they either refer to different arrays or to different
            /// indexes of the array
            /////////////////////////////////////////////////
            bool operator!=(const ByteIterator<T_Type, T_Stride>& other) const{
                return !(this->operator==(other));
            }

            /////////////////////////////////////////////////
            /// \brief Returns a pointer to the current element the iterator
            /// refers to
            /////////////////////////////////////////////////
            T_Type* operator->(){
                return &(bytesToType<T_Type>(this->mByteArray[this->mCurIndex]));
            }

            /////////////////////////////////////////////////
            /// \brief Returns a reference to the current element the iterator
            /// refers to
            /////////////////////////////////////////////////
            T_Type& operator*(){
                return (bytesToType<T_Type>(&this->mByteArray[this->mCurIndex]));
            }

            /////////////////////////////////////////////////
            /// \brief Allows random access to elements by offset, NOT by absolute indexes.
            /// \note offset is in terms of elements, NOT bytes, hence the absolute byte index
            /// is moved by offset*stride
            /////////////////////////////////////////////////
            T_Type& operator[](int offset){
                return (bytesToType<T_Type>(&this->mByteArray[this->mCurIndex+(offset*T_Stride)]));
            }

            /////////////////////////////////////////////////
            /// \brief Returns the byte index of the current element the iterator
            /// is reading
            /////////////////////////////////////////////////
            size_t getByteIndex(int offset = 0){
                return mCurIndex;
            }
        private:
            ///The current absolute BYTE index being read from the array
            size_t mCurIndex;

            ///Pointer to the byte array being read
            byte_t* mByteArray;

        };



    }
}

#endif // LIE_CORE_BYTEREADER_HPP
