/////////////////////////////////////////////////
//Part of Lithium Engine - By Jamie Terry
/////////////////////////////////////////////////
/// \file test.hpp
/// \author Jamie Terry
/// \brief
/// Convinence header that includes all other header files in the
/// test module
///
/// \defgroup test Testing Module
/// \brief This module contains classes used for running unit tests and
/// then generating a report on the tests run.
///
/// \note Depends on the core module
///
/// \detail A previous version of the unit testing classes were in the core
/// module, however no part of the engine relies on the testing of
/// the engine, instead the application can choose to seperately
/// include the test module.
/// This makes more sense and allows final release version to not
/// include the testing code as a part of the core module
/////////////////////////////////////////////////

#ifndef LIE_TEST_HPP
#define LIE_TEST_HPP

#include "test\TestObject.hpp"
#include "test\Comment.hpp"
#include "test\TestGroup.hpp"
#include "test\ValueTest.hpp"
#include "test\ExceptionTest.hpp"
#include "test\NoExceptionTest.hpp"

namespace lie{
    /////////////////////////////////////////////////
    /// \brief Contains all the classes in the Testing Module
    /// These classes run and generate reports on unit tests
    /////////////////////////////////////////////////
    namespace test{

    }
}




#endif // LIE_TEST_HPP
