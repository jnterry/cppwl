/////////////////////////////////////////////////
//Part of Lithium Engine - By Jamie Terry
/////////////////////////////////////////////////
/// \file core.hpp
/// \author Jamie Terry
/// \brief Convinence header that includes all other header files in the
/// core module
///
/// \defgroup core Core Module
/// \brief Contains core classes which are depended on by all other modules.
/// These classes provide a solid foundation even for non game applications
///
/// \ingroup core
/////////////////////////////////////////////////

#ifndef LIE_CORE_HPP
#define LIE_CORE_HPP

#include "core\Config.hpp"

#include "core\Hash.hpp"

#include "core\Exception.hpp"
#include "core\ExcepFileIO.hpp"
#include "core\ExcepOutOfRange.hpp"
#include "core\ExcepParseError.hpp"

#include "core/Handle.hpp"

#include "core\Types.hpp"
#include "core\MetaType.hpp"
#include "core\MetaTypeField.hpp"
#include "core\QualifiedMetaType.hpp"
#include "core\TypeInfo.hpp"

#include "core\Resource.hpp"
#include "core\ResourceHandle.hpp"
#include "core\ResourceDatabase.hpp"

#include "core\RTimeMsg.hpp"
#include "core\LogMsgWritterBase.hpp"
#include "core\LogMsgWritterCout.hpp"
#include "core\LogMsgWritterTxt.hpp"
#include "core\LogMsgWritterHtml.hpp"
#include "core\Logger.hpp"

#include "core\Time.hpp"
#include "core\Timer.hpp"
#include "core\Convert.hpp"
#include "core\Chars.hpp"
#include "core/StringUtils.hpp"
#include "core\PathStyle.hpp"
#include "core\Folder.hpp"
#include "core\File.hpp"
#include "core\ArrayIterator.hpp"
#include "core\ByteBuffer.hpp"


/////////////////////////////////////////////////
/// \brief Root Lithium Engine namespace containing all classes and
/// module namespaces
/////////////////////////////////////////////////
namespace lie{
    /////////////////////////////////////////////////
    /// \brief Contains foundation classes that other modules in the
    /// Lithium Engine depend on. None are specified to games or multimedia
    /// applications and could be useful in any program
    /////////////////////////////////////////////////
    namespace core{

    }
}


#endif // LIE_CORE_H
