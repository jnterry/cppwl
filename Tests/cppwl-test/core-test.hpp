/////////////////////////////////////////////////
/// Part of C++ Web Library - By Jamie Terry  ///
/////////////////////////////////////////////////
/// \file core.hpp
/// \author Jamie Terry
/// \date 2015/08/01
/// \brief Includes all unit test files for the cppwl-core module,
/// also includes a function to run all these tests
/// 
/// \ingroup unit-tests
/////////////////////////////////////////////////

#ifndef CPPWL_TEST_CORE_HPP
#define CPPWL_TEST_CORE_HPP

#include <lie/test.hpp>
#include "core/IPv4-test.hpp"
#include "core/Url-test.hpp"

namespace cppwl{
	void Test_Core(lie::test::TestGroup* group){
		lie::test::TestGroup* coreGroup = group->createSubgroup("core");

		Test_IPv4(coreGroup);
		Test_Url(coreGroup);
	}
}

#endif