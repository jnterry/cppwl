/////////////////////////////////////////////////
///  Part of C++ Web Library - By Jamie Terry ///
/////////////////////////////////////////////////
/// \file Router-test.hpp
/// \author Jamie Terry
/// \date 2015/08/26
/// \brief Includes tests for the Router class
/// \ingroup route-test
/////////////////////////////////////////////////

#ifndef CPPWL_TEST_ROUTE_ROUTER_HPP
#define CPPWL_TEST_ROUTE_ROUTER_HPP

#include <cppwl\route\Router.hpp>
#include <cppwl\route\RouteClassMethod.hpp>
#include <cppwl\core\Response.hpp>
#include <cppwl\core\Url.hpp>
#include <cppwl\core\DummyRequest.hpp>
#include <lie\test.hpp>

//////////////////////////////////////////////////////////////////////////
/// \brief A test class which represents a dummy route, simply returns a response
/// with some specified status code - good for testing Router as the status
/// code can be easily checked
//////////////////////////////////////////////////////////////////////////
class DummyRoute : public cppwl::Route{
public:
	DummyRoute(cppwl::HttpMethodType methods, int code) 
		: Route(methods), mCode(code){}

	cppwl::Response& go(const cppwl::ReceivedRequest&){
		return *(new cppwl::Response(mCode));
	}

private:
	int mCode;
};

namespace cppwl{

	void Test_Router(lie::test::TestGroup* group){
		//:TODO: these tests all leak memory as no-one deletes the responses returned from
		//the Router in dispatch

		lie::test::TestGroup* routerGroup = group->createSubgroup("Router");

		{
			routerGroup->add(new lie::test::Comment("Setting up Router - Basic Tests"));
			Router r;
			r.registerRoute("/", new DummyRoute(HttpMethod::GET, 1));
			r.registerRoute("/user/", new DummyRoute(HttpMethod::GET, 2));
			r.registerRoute("/user/", new DummyRoute(HttpMethod::POST, 3));
			r.registerRoute("/user/{Integer}/", new DummyRoute(HttpMethod::GET, 4));
			r.registerRoute("/user/{*}/", new DummyRoute(HttpMethod::GET, 5));
			r.registerRoute("/dashboard/", new DummyRoute(HttpMethod::GET, 6));
			r.registerRoute("/dashboard/{**}", new DummyRoute(HttpMethod::GET, 7));

			routerGroup->add(new lie::test::ValueTest<int>("dispatch GET /", 1, 
				r.dispatch(DummyRequest(HttpMethod::GET, Url("www.test.com/"))).getStatus().code));
			routerGroup->add(new lie::test::ValueTest<int>("dispatch GET /user", 2,
				r.dispatch(DummyRequest(HttpMethod::GET, Url("www.test.com/user"))).getStatus().code));
			routerGroup->add(new lie::test::ValueTest<int>("dispatch GET /user/", 2,
				r.dispatch(DummyRequest(HttpMethod::GET, Url("www.test.com/user/"))).getStatus().code));
			routerGroup->add(new lie::test::ValueTest<int>("dispatch POST /user/", 3,
				r.dispatch(DummyRequest(HttpMethod::POST, Url("www.test.com/user/"))).getStatus().code));
			routerGroup->add(new lie::test::ValueTest<int>("dispatch GET /user/1", 4,
				r.dispatch(DummyRequest(HttpMethod::GET, Url("www.test.com/user/1"))).getStatus().code));
			routerGroup->add(new lie::test::ValueTest<int>("dispatch GET /user/200", 4,
				r.dispatch(DummyRequest(HttpMethod::GET, Url("www.test.com/user/200"))).getStatus().code));
			routerGroup->add(new lie::test::ValueTest<int>("dispatch GET /user/-1000", 4,
				r.dispatch(DummyRequest(HttpMethod::GET, Url("www.test.com/user/-1000"))).getStatus().code));
			routerGroup->add(new lie::test::ValueTest<int>("dispatch GET /user/thing/", 5,
				r.dispatch(DummyRequest(HttpMethod::GET, Url("www.test.com/user/thing/"))).getStatus().code));
			routerGroup->add(new lie::test::ValueTest<int>("dispatch GET /user/stuff/", 5,
				r.dispatch(DummyRequest(HttpMethod::GET, Url("www.test.com/user/stuff/"))).getStatus().code));
			routerGroup->add(new lie::test::ValueTest<int>("dispatch GET /user/hello/world/", 404,
				r.dispatch(DummyRequest(HttpMethod::GET, Url("www.test.com/user/hello/world/"))).getStatus().code));
			routerGroup->add(new lie::test::ValueTest<int>("dispatch GET /dashboard/", 6,
				r.dispatch(DummyRequest(HttpMethod::GET, Url("www.test.com/dashboard/"))).getStatus().code));
			routerGroup->add(new lie::test::ValueTest<int>("dispatch POST /dashboard/", 404,
				r.dispatch(DummyRequest(HttpMethod::POST, Url("www.test.com/dashboard/"))).getStatus().code));
			routerGroup->add(new lie::test::ValueTest<int>("dispatch GET /dashboard/1/", 7,
				r.dispatch(DummyRequest(HttpMethod::GET, Url("www.test.com/dashboard/1/"))).getStatus().code));
			routerGroup->add(new lie::test::ValueTest<int>("dispatch GET /dashboard/abc/", 7,
				r.dispatch(DummyRequest(HttpMethod::GET, Url("www.test.com/dashboard/abc/"))).getStatus().code));
			routerGroup->add(new lie::test::ValueTest<int>("dispatch GET /dashboard/abc/stuff/", 7,
				r.dispatch(DummyRequest(HttpMethod::GET, Url("www.test.com/dashboard/abc/stuff/"))).getStatus().code));
			routerGroup->add(new lie::test::ValueTest<int>("dispatch GET /dashboard/abc/stuff/more/things/", 7,
				r.dispatch(DummyRequest(HttpMethod::GET, Url("www.test.com/dashboard/abc/stuff/more/things/"))).getStatus().code));
			routerGroup->add(new lie::test::ValueTest<int>("dispatch GET /hi/", 404,
				r.dispatch(DummyRequest(HttpMethod::GET, Url("www.test.com/hi/"))).getStatus().code));
			routerGroup->add(new lie::test::ValueTest<int>("dispatch GET /1/", 404,
				r.dispatch(DummyRequest(HttpMethod::GET, Url("www.test.com/1/"))).getStatus().code));
		}

		{
			routerGroup->add(new lie::test::Comment("Setting up Router - If more general route is registered first it should be used"));
			Router r;
			r.registerRoute("/user/{*}/", new DummyRoute(HttpMethod::GET, 1)); //* more general than Integer
			r.registerRoute("/user/{Integer}/", new DummyRoute(HttpMethod::GET, 2));

			routerGroup->add(new lie::test::ValueTest<int>("dispatch GET /user/", 404,
				r.dispatch(DummyRequest(HttpMethod::GET, Url("www.test.com/user/"))).getStatus().code));
			routerGroup->add(new lie::test::ValueTest<int>("dispatch GET /user/1", 1,
				r.dispatch(DummyRequest(HttpMethod::GET, Url("www.test.com/user/1"))).getStatus().code));
			routerGroup->add(new lie::test::ValueTest<int>("dispatch GET /user/200", 1,
				r.dispatch(DummyRequest(HttpMethod::GET, Url("www.test.com/user/200"))).getStatus().code));
			routerGroup->add(new lie::test::ValueTest<int>("dispatch GET /user/-1000", 1,
				r.dispatch(DummyRequest(HttpMethod::GET, Url("www.test.com/user/-1000"))).getStatus().code));
			routerGroup->add(new lie::test::ValueTest<int>("dispatch GET /user/thing/", 1,
				r.dispatch(DummyRequest(HttpMethod::GET, Url("www.test.com/user/thing/"))).getStatus().code));
			routerGroup->add(new lie::test::ValueTest<int>("dispatch GET /user/stuff/", 1,
				r.dispatch(DummyRequest(HttpMethod::GET, Url("www.test.com/user/stuff/"))).getStatus().code));
		}

		{
			routerGroup->add(new lie::test::Comment("Setting up Router - Routes may respond to multiple HTTP methods"));
			Router r;
			r.registerRoute("/", new DummyRoute(HttpMethod::GET, 1));
			r.registerRoute("/", new DummyRoute(HttpMethod::POST | HttpMethod::PUT, 2));
			r.registerRoute("/", new DummyRoute(HttpMethod::DELETE, 3));

			routerGroup->add(new lie::test::ValueTest<int>("dispatch GET /", 1,
				r.dispatch(DummyRequest(HttpMethod::GET, Url("www.test.com/"))).getStatus().code));
			routerGroup->add(new lie::test::ValueTest<int>("dispatch POST /", 2,
				r.dispatch(DummyRequest(HttpMethod::POST, Url("www.test.com/"))).getStatus().code));
			routerGroup->add(new lie::test::ValueTest<int>("dispatch PUT /", 2,
				r.dispatch(DummyRequest(HttpMethod::PUT, Url("www.test.com/"))).getStatus().code));
			routerGroup->add(new lie::test::ValueTest<int>("dispatch DELETE /", 3,
				r.dispatch(DummyRequest(HttpMethod::DELETE, Url("www.test.com/"))).getStatus().code));
			routerGroup->add(new lie::test::ValueTest<int>("dispatch PATCH /", 404,
				r.dispatch(DummyRequest(HttpMethod::PATCH, Url("www.test.com/"))).getStatus().code));

		}

		{
			lie::test::TestGroup* excepGrp = routerGroup->createSubgroup("Expect Exceptions");
			{
				Router r;
				r.registerRoute("/", new DummyRoute(HttpMethod::GET, 1));
				excepGrp->add(new lie::test::ExceptionTest<cppwl::ExcepDuplicateRoute, int>("registering duplicates",
					[&r]{r.registerRoute("/", new DummyRoute(HttpMethod::GET, 1)); return 1; }));
			}
			{
				Router r;
				r.registerRoute("/", new DummyRoute(HttpMethod::PUT | HttpMethod::POST, 1));
				excepGrp->add(new lie::test::ExceptionTest<cppwl::ExcepDuplicateRoute, int>("registering duplicates",
					[&r]{r.registerRoute("/", new DummyRoute(HttpMethod::POST, 1)); return 1; }));
			}
			{
				Router r;
				r.registerRoute("/", new DummyRoute(HttpMethod::DELETE, 1));
				excepGrp->add(new lie::test::ExceptionTest<cppwl::ExcepDuplicateRoute, int>("registering duplicates",
					[&r]{r.registerRoute("/", new DummyRoute(HttpMethod::DELETE | HttpMethod::PATCH, 1)); return 1; }));
			}
			{
				Router r;
				excepGrp->add(new lie::test::ExceptionTest<std::regex_error, int>("registering bad regex ^*$",
					[&r]{r.registerRoute("/{^*$}", new DummyRoute(HttpMethod::GET, 1)); return 1; }));
			}
			

		}

	}
}

#endif