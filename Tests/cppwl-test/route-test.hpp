/////////////////////////////////////////////////
///  Part of C++ Web Library - By Jamie Terry ///
/////////////////////////////////////////////////
/// \file route-test.hpp
/// \author Jamie Terry
/// \date 2015/08/26
/// \brief #includes the other files in the route test group
/// \ingroup route-test
/////////////////////////////////////////////////

#ifndef CPPWL_TEST_ROUTE_HPP
#define CPPWL_TEST_ROUTE_HPP

#include <lie\test\TestGroup.hpp>

#include "route\Router-test.hpp"

namespace cppwl{
	void Test_Route(lie::test::TestGroup* group){
		lie::test::TestGroup* routeGroup = group->createSubgroup("Route");
		
		cppwl::Test_Router(routeGroup);
	}
}

#endif