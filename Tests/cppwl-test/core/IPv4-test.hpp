/////////////////////////////////////////////////
/// Part of C++ Web Library - By Jamie Terry  ///
/////////////////////////////////////////////////
/// \file IPv4-test.hpp
/// \author Jamie Terry
/// \date 2015/08/01
/// \brief Contains unit tests for the cppwl::IPv4 class
/// 
/// \ingroup unit-tests
/////////////////////////////////////////////////

#ifndef CPPWL_TEST_CORE_IPV4_HPP
#define CPPWL_TEST_CORE_IPV4_HPP

#include <lie\test.hpp>
#include <cppwl\core\IPv4.hpp>
#include <stdint.h>

namespace cppwl{
	void Test_IPv4(lie::test::TestGroup* group){
		lie::test::TestGroup* ipGroup = group->createSubgroup("IPv4");

		lie::test::TestGroup* constructGroup = ipGroup->createSubgroup("Constructors");
		IPv4 ip1;
		constructGroup->add(new lie::test::ValueTest<unsigned int>("Default Constructor - Component0", 0, ip1.getComponent(0)));
		constructGroup->add(new lie::test::ValueTest<unsigned int>("Default Constructor - Component1", 0, ip1.getComponent(1)));
		constructGroup->add(new lie::test::ValueTest<unsigned int>("Default Constructor - Component2", 0, ip1.getComponent(2)));
		constructGroup->add(new lie::test::ValueTest<unsigned int>("Default Constructor - Component3", 0, ip1.getComponent(3)));

		IPv4 ip2(50, 100, 150, 200);
		constructGroup->add(new lie::test::ValueTest<unsigned int>("Components Constructor - Component0", 50, ip2.getComponent(0)));
		constructGroup->add(new lie::test::ValueTest<unsigned int>("Components Constructor - Component1", 100, ip2.getComponent(1)));
		constructGroup->add(new lie::test::ValueTest<unsigned int>("Components Constructor - Component2", 150, ip2.getComponent(2)));
		constructGroup->add(new lie::test::ValueTest<unsigned int>("Components Constructor - Component3", 200, ip2.getComponent(3)));

		ipGroup->add(new lie::test::ValueTest<unsigned int>("Localhost - Component0", 127, IPv4::LOCALHOST.getComponent(0)));
		ipGroup->add(new lie::test::ValueTest<unsigned int>("Localhost - Component1", 0, IPv4::LOCALHOST.getComponent(1)));
		ipGroup->add(new lie::test::ValueTest<unsigned int>("Localhost - Component2", 0, IPv4::LOCALHOST.getComponent(2)));
		ipGroup->add(new lie::test::ValueTest<unsigned int>("Localhost - Component3", 1, IPv4::LOCALHOST.getComponent(3)));

		lie::test::TestGroup* setGetGroup = ipGroup->createSubgroup("Setters/Getters");
		IPv4 ip3;
		ip3.set(10, 20, 30, 40);
		setGetGroup->add(new lie::test::ValueTest<unsigned int>("Set - Component0", 10, ip3.getComponent(0)));
		setGetGroup->add(new lie::test::ValueTest<unsigned int>("Set - Component1", 20, ip3.getComponent(1)));
		setGetGroup->add(new lie::test::ValueTest<unsigned int>("Set - Component2", 30, ip3.getComponent(2)));
		setGetGroup->add(new lie::test::ValueTest<unsigned int>("Set - Component3", 40, ip3.getComponent(3)));

		ip3.setComponent(0, 100);
		setGetGroup->add(new lie::test::ValueTest<unsigned int>("Set Comp0 - Component0", 100, ip3.getComponent(0)));
		setGetGroup->add(new lie::test::ValueTest<unsigned int>("Set Comp0 - Component1", 20, ip3.getComponent(1)));
		setGetGroup->add(new lie::test::ValueTest<unsigned int>("Set Comp0 - Component2", 30, ip3.getComponent(2)));
		setGetGroup->add(new lie::test::ValueTest<unsigned int>("Set Comp0 - Component3", 40, ip3.getComponent(3)));

		ip3.setComponent(1, 110);
		setGetGroup->add(new lie::test::ValueTest<unsigned int>("Set Comp1 - Component0", 100, ip3.getComponent(0)));
		setGetGroup->add(new lie::test::ValueTest<unsigned int>("Set Comp1 - Component1", 110, ip3.getComponent(1)));
		setGetGroup->add(new lie::test::ValueTest<unsigned int>("Set Comp1 - Component2", 30, ip3.getComponent(2)));
		setGetGroup->add(new lie::test::ValueTest<unsigned int>("Set Comp1 - Component3", 40, ip3.getComponent(3)));

		ip3.setComponent(2, 120);
		setGetGroup->add(new lie::test::ValueTest<unsigned int>("Set Comp2 - Component0", 100, ip3.getComponent(0)));
		setGetGroup->add(new lie::test::ValueTest<unsigned int>("Set Comp2 - Component1", 110, ip3.getComponent(1)));
		setGetGroup->add(new lie::test::ValueTest<unsigned int>("Set Comp2 - Component2", 120, ip3.getComponent(2)));
		setGetGroup->add(new lie::test::ValueTest<unsigned int>("Set Comp2 - Component3", 40, ip3.getComponent(3)));

		ip3.setComponent(3, 130);
		setGetGroup->add(new lie::test::ValueTest<unsigned int>("Set Comp3 - Component0", 100, ip3.getComponent(0)));
		setGetGroup->add(new lie::test::ValueTest<unsigned int>("Set Comp3 - Component1", 110, ip3.getComponent(1)));
		setGetGroup->add(new lie::test::ValueTest<unsigned int>("Set Comp3 - Component2", 120, ip3.getComponent(2)));
		setGetGroup->add(new lie::test::ValueTest<unsigned int>("Set Comp3 - Component3", 130, ip3.getComponent(3)));

		lie::test::TestGroup* parseGroup = ipGroup->createSubgroup("Parsing/To String");
		IPv4 ip4("50.51.52.53");
		parseGroup->add(new lie::test::ValueTest<unsigned int>("Parse Constructor - Component0", 50, ip4.getComponent(0)));
		parseGroup->add(new lie::test::ValueTest<unsigned int>("Parse Constructor - Component1", 51, ip4.getComponent(1)));
		parseGroup->add(new lie::test::ValueTest<unsigned int>("Parse Constructor - Component2", 52, ip4.getComponent(2)));
		parseGroup->add(new lie::test::ValueTest<unsigned int>("Parse Constructor - Component3", 53, ip4.getComponent(3)));
		parseGroup->add(new lie::test::ValueTest<std::string>("Parse Constructor - toString", "50.51.52.53", ip4.toString()));

		ip4.parse("0.1.2.3");
		parseGroup->add(new lie::test::ValueTest<unsigned int>("Parse 0.1.2.3 - Component0", 0, ip4.getComponent(0)));
		parseGroup->add(new lie::test::ValueTest<unsigned int>("Parse 0.1.2.3 - Component1", 1, ip4.getComponent(1)));
		parseGroup->add(new lie::test::ValueTest<unsigned int>("Parse 0.1.2.3 - Component2", 2, ip4.getComponent(2)));
		parseGroup->add(new lie::test::ValueTest<unsigned int>("Parse 0.1.2.3 - Component3", 3, ip4.getComponent(3)));
		parseGroup->add(new lie::test::ValueTest<std::string>("Parse 0.1.2.3 - toString", "0.1.2.3", ip4.toString()));

		ip4.parse("0.10.200.255");
		parseGroup->add(new lie::test::ValueTest<unsigned int>("Parse 0.10.200.255 - Component0", 000, ip4.getComponent(0)));
		parseGroup->add(new lie::test::ValueTest<unsigned int>("Parse 0.10.200.255 - Component1", 10, ip4.getComponent(1)));
		parseGroup->add(new lie::test::ValueTest<unsigned int>("Parse 0.10.200.255 - Component2", 200, ip4.getComponent(2)));
		parseGroup->add(new lie::test::ValueTest<unsigned int>("Parse 0.10.200.255 - Component3", 255, ip4.getComponent(3)));
		parseGroup->add(new lie::test::ValueTest<std::string>("Parse 0.10.200.255 - toString", "0.10.200.255", ip4.toString()));

		ip4.parse("9.8.7.6:80");
		parseGroup->add(new lie::test::ValueTest<unsigned int>("Parse 9.8.7.6:80 - Component0", 9, ip4.getComponent(0)));
		parseGroup->add(new lie::test::ValueTest<unsigned int>("Parse 9.8.7.6:80 - Component1", 8, ip4.getComponent(1)));
		parseGroup->add(new lie::test::ValueTest<unsigned int>("Parse 9.8.7.6:80 - Component2", 7, ip4.getComponent(2)));
		parseGroup->add(new lie::test::ValueTest<unsigned int>("Parse 9.8.7.6:80 - Component3", 6, ip4.getComponent(3)));
		parseGroup->add(new lie::test::ValueTest<std::string>("Parse 9.8.7.6:80 - toString", "9.8.7.6", ip4.toString()));

		{ //ensuring exception is thrown for invalid strings
			auto funcMakeIPv4 = [](std::string val){
				IPv4 ip(val); return ip.toString();
			};

			parseGroup->add(new lie::test::ExceptionTest<lie::core::ExcepParseError, std::string, std::string>(
				"Parse \"abc\"", funcMakeIPv4, "abc"));
			parseGroup->add(new lie::test::ExceptionTest<lie::core::ExcepParseError, std::string, std::string>(
				"Parse \"a.2.3.4\"", funcMakeIPv4, "a.2.3.4"));
			parseGroup->add(new lie::test::ExceptionTest<lie::core::ExcepParseError, std::string, std::string>(
				"Parse \"1.a.3.4\"", funcMakeIPv4, "1.a.3.4"));
			parseGroup->add(new lie::test::ExceptionTest<lie::core::ExcepParseError, std::string, std::string>(
				"Parse \"1.2.a.4\"", funcMakeIPv4, "1.2.a.4"));
			parseGroup->add(new lie::test::ExceptionTest<lie::core::ExcepParseError, std::string, std::string>(
				"Parse \"1.2.3.a\"", funcMakeIPv4, "1.2.3.a"));
			parseGroup->add(new lie::test::ExceptionTest<lie::core::ExcepParseError, std::string, std::string>(
				"Parse \"1.2.3.400\"", funcMakeIPv4, "1.2.3.400"));
			parseGroup->add(new lie::test::ExceptionTest<lie::core::ExcepParseError, std::string, std::string>(
				"Parse \"1.2.300.4\"", funcMakeIPv4, "1.2.300.4"));
			parseGroup->add(new lie::test::ExceptionTest<lie::core::ExcepParseError, std::string, std::string>(
				"Parse \"1.2.-10.4\"", funcMakeIPv4, "1.2.-10.4"));
			parseGroup->add(new lie::test::ExceptionTest<lie::core::ExcepParseError, std::string, std::string>(
				"Parse \"1.2.3.4.5\"", funcMakeIPv4, "1.2.3.4.5"));
			parseGroup->add(new lie::test::ExceptionTest<lie::core::ExcepParseError, std::string, std::string>(
				"Parse \"1.2.3.\"", funcMakeIPv4, "1.2.3."));
			parseGroup->add(new lie::test::ExceptionTest<lie::core::ExcepParseError, std::string, std::string>(
				"Parse \"1.2..\"", funcMakeIPv4, "1.2.."));
			parseGroup->add(new lie::test::ExceptionTest<lie::core::ExcepParseError, std::string, std::string>(
				"Parse \"1...\"", funcMakeIPv4, "1..."));
			parseGroup->add(new lie::test::ExceptionTest<lie::core::ExcepParseError, std::string, std::string>(
				"Parse \"...\"", funcMakeIPv4, "..."));
			parseGroup->add(new lie::test::ExceptionTest<lie::core::ExcepParseError, std::string, std::string>(
				"Parse \"1.2.3.4:50.1\"", funcMakeIPv4, "1.2.3.4:50.1"));
			parseGroup->add(new lie::test::ExceptionTest<lie::core::ExcepParseError, std::string, std::string>(
				"Parse \"1.2.3.4:a\"", funcMakeIPv4, "1.2.3.4:a"));

		} //end of exception tests
	}
}

#endif