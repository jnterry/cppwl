/////////////////////////////////////////////////
///  Part of C++ Web Library - By Jamie Terry ///
/////////////////////////////////////////////////
/// \file Url-test.hpp
/// \author Jamie Terry
/// \date 2015/08/19
/// \brief Contains unit tests for the class cppwl::Url
/// \ingroup unit-tests
/////////////////////////////////////////////////

#ifndef CPPWL_TEST_CORE_URL_HPP
#define CPPWL_TEST_CORE_URL_HPP

#include <lie\test.hpp>
#include <lie\core\ExcepParseError.hpp>
#include <cppwl\core\Url.hpp>


namespace cppwl{

	void Test_Url(lie::test::TestGroup* group){
		lie::test::TestGroup* urlGroup = group->createSubgroup("Url");

		lie::test::TestGroup* compConstGroup = urlGroup->createSubgroup("Component Constructor + Getters");

		{
			lie::test::TestGroup* grp = compConstGroup->createSubgroup("Url(\"\", \"www.example.com\")");
			cppwl::Url url("", "www.example.com");
			grp->add(new lie::test::ValueTest<std::string>("Protocol", "", url.getProtocol()));
			grp->add(new lie::test::ValueTest<std::string>("Domain", "www.example.com", url.getDomain()));
			grp->add(new lie::test::ValueTest<std::string>("Resource", "", url.getResource()));
			grp->add(new lie::test::ValueTest<std::string>("Query String", "", url.getQueryString()));
			grp->add(new lie::test::ValueTest<int>("Query Param Count", 0, url.getQueryParamCount()));
			grp->add(new lie::test::ValueTest<std::string>("Fragment Identifier", "", url.getFragmentIdentifier()));
			grp->add(new lie::test::ValueTest<std::string>("To String", "www.example.com", url));
		}

		{
			lie::test::TestGroup* grp = compConstGroup->createSubgroup("Url(\"http\", \"www.example.com\", \"thing.php\")");
			cppwl::Url url("http", "www.example.com", "thing.php");
			grp->add(new lie::test::ValueTest<std::string>("Protocol", "http", url.getProtocol()));
			grp->add(new lie::test::ValueTest<std::string>("Domain", "www.example.com", url.getDomain()));
			grp->add(new lie::test::ValueTest<std::string>("Resource", "thing.php", url.getResource()));
			grp->add(new lie::test::ValueTest<std::string>("Query String", "", url.getQueryString()));
			grp->add(new lie::test::ValueTest<int>("Query Param Count", 0, url.getQueryParamCount()));
			grp->add(new lie::test::ValueTest<std::string>("Fragment Identifier", "", url.getFragmentIdentifier()));
			grp->add(new lie::test::ValueTest<std::string>("To String", "http://www.example.com/thing.php", url));
		}

		{
			lie::test::TestGroup* grp = compConstGroup->createSubgroup("Url(\"https\", \"www.example.com\", \"thing.php\", \"key1=val1&key2=val2\", \"test\")");
			cppwl::Url url("https", "www.example.com", "thing.php", "key1=val1&key2=value2", "test");
			grp->add(new lie::test::ValueTest<std::string>("Protocol", "https", url.getProtocol()));
			grp->add(new lie::test::ValueTest<std::string>("Domain", "www.example.com", url.getDomain()));
			grp->add(new lie::test::ValueTest<std::string>("Resource", "thing.php", url.getResource()));
			grp->add(new lie::test::ValueTest<std::string>("Query Param 'key1'", "val1", url.getQueryParam("key1")));
			grp->add(new lie::test::ValueTest<std::string>("Query Param 'key2'", "value2", url.getQueryParam("key2")));
			grp->add(new lie::test::ValueTest<std::string>("Query Param 'key3'", "", url.getQueryParam("key3")));
			grp->add(new lie::test::ValueTest<bool>("Has Query Param 'key1'", true, url.hasQueryParam("key1")));
			grp->add(new lie::test::ValueTest<bool>("Has Query Param 'key2'", true, url.hasQueryParam("key2")));
			grp->add(new lie::test::ValueTest<bool>("Has Query Param 'key3'", false, url.hasQueryParam("key3")));
			grp->add(new lie::test::ValueTest<int>("Query Param Count", 2, url.getQueryParamCount()));
			grp->add(new lie::test::ValueTest<std::string>("Fragment Identifier", "test", url.getFragmentIdentifier()));
			grp->add(new lie::test::ValueTest<std::string>("To String", "https://www.example.com/thing.php?key1=val1&key2=value2#test", url));
		}

		{
			lie::test::TestGroup* grp = compConstGroup->createSubgroup("Url(\"http\", \"www.example.com\", \"\", \"\", \"test\")");
			cppwl::Url url("http", "www.example.com", "", "", "test");
			grp->add(new lie::test::ValueTest<std::string>("Protocol", "http", url.getProtocol()));
			grp->add(new lie::test::ValueTest<std::string>("Domain", "www.example.com", url.getDomain()));
			grp->add(new lie::test::ValueTest<std::string>("Resource", "", url.getResource()));
			grp->add(new lie::test::ValueTest<int>("Query Param Count", 0, url.getQueryParamCount()));
			grp->add(new lie::test::ValueTest<std::string>("Fragment Identifier", "test", url.getFragmentIdentifier()));
			grp->add(new lie::test::ValueTest<std::string>("To String", "http://www.example.com#test", url));
		}

		{
			lie::test::TestGroup* grp = compConstGroup->createSubgroup("Url(\"http\", \"www.bbc.co.uk\", \"\", \"blob=1/2/3\", \"\")");
			cppwl::Url url("http", "www.bbc.co.uk", "", "blob=1/2/3", "");
			grp->add(new lie::test::ValueTest<std::string>("Protocol", "http", url.getProtocol()));
			grp->add(new lie::test::ValueTest<std::string>("Domain", "www.bbc.co.uk", url.getDomain()));
			grp->add(new lie::test::ValueTest<std::string>("Resource", "", url.getResource()));
			grp->add(new lie::test::ValueTest<std::string>("Query String", "blob=1/2/3", url.getQueryString()));
			grp->add(new lie::test::ValueTest<std::string>("Query Param 'blob'", "1/2/3", url.getQueryParam("blob")));
			grp->add(new lie::test::ValueTest<int>("Query Param Count", 1, url.getQueryParamCount()));
			grp->add(new lie::test::ValueTest<std::string>("Fragment Identifier", "", url.getFragmentIdentifier()));
			grp->add(new lie::test::ValueTest<std::string>("To String", "http://www.bbc.co.uk?blob=1/2/3", url));
		}

		{
			lie::test::TestGroup* grp = compConstGroup->createSubgroup("Url(\"http\", \"www.test.com\", \"\", \"a=1&b=2&c=3\", \"place\")");
			cppwl::Url url("http", "www.test.com", "", "a=1&b=2&c=3", "place");
			grp->add(new lie::test::ValueTest<std::string>("Protocol", "http", url.getProtocol()));
			grp->add(new lie::test::ValueTest<std::string>("Domain", "www.test.com", url.getDomain()));
			grp->add(new lie::test::ValueTest<std::string>("Resource", "", url.getResource()));
			grp->add(new lie::test::ValueTest<std::string>("Query String", "a=1&b=2&c=3", url.getQueryString()));
			grp->add(new lie::test::ValueTest<int>("Query Param Count", 3, url.getQueryParamCount()));
			grp->add(new lie::test::ValueTest<std::string>("Fragment Identifier", "place", url.getFragmentIdentifier()));
			grp->add(new lie::test::ValueTest<std::string>("To String", "http://www.test.com?a=1&b=2&c=3#place", url));
		}

		{
			lie::test::TestGroup* grp = urlGroup->createSubgroup("Setters/Getters");
			cppwl::Url url("http", "www.example.com", "thing.php", "a=1", "anchor");
			url.setProtocol("https");
			url.setFragmentIdentifier("here");
			grp->add(new lie::test::ValueTest<std::string>("Protocol", "https", url.getProtocol()));
			grp->add(new lie::test::ValueTest<std::string>("Domain", "www.example.com", url.getDomain()));
			grp->add(new lie::test::ValueTest<std::string>("Resource", "thing.php", url.getResource()));
			grp->add(new lie::test::ValueTest<std::string>("Query String", "a=1", url.getQueryString()));
			grp->add(new lie::test::ValueTest<int>("Query Param Count", 1, url.getQueryParamCount()));
			grp->add(new lie::test::ValueTest<std::string>("Fragment Identifier", "here", url.getFragmentIdentifier()));
			grp->add(new lie::test::ValueTest<std::string>("To String", "https://www.example.com/thing.php?a=1#here", url));

			url.setQueryParam("b", "2");
			url.setDomain("www.test.com");

			grp->add(new lie::test::ValueTest<std::string>("Protocol", "https", url.getProtocol()));
			grp->add(new lie::test::ValueTest<std::string>("Domain", "www.test.com", url.getDomain()));
			grp->add(new lie::test::ValueTest<std::string>("Resource", "thing.php", url.getResource()));
			grp->add(new lie::test::ValueTest<std::string>("Query String", "a=1&b=2", url.getQueryString()));
			grp->add(new lie::test::ValueTest<int>("Query Param Count", 2, url.getQueryParamCount()));
			grp->add(new lie::test::ValueTest<std::string>("Fragment Identifier", "here", url.getFragmentIdentifier()));
			grp->add(new lie::test::ValueTest<std::string>("To String", "https://www.test.com/thing.php?a=1&b=2#here", url));

			url.unsetQueryParam("a");
			url.setQueryParam("b", "10");
			url.setResource("here/we/are");
			grp->add(new lie::test::ValueTest<std::string>("Protocol", "https", url.getProtocol()));
			grp->add(new lie::test::ValueTest<std::string>("Domain", "www.test.com", url.getDomain()));
			grp->add(new lie::test::ValueTest<std::string>("Resource", "here/we/are", url.getResource()));
			grp->add(new lie::test::ValueTest<std::string>("Query String", "b=10", url.getQueryString()));
			grp->add(new lie::test::ValueTest<int>("Query Param Count", 1, url.getQueryParamCount()));
			grp->add(new lie::test::ValueTest<std::string>("Fragment Identifier", "here", url.getFragmentIdentifier()));
			grp->add(new lie::test::ValueTest<std::string>("To String", "https://www.test.com/here/we/are?b=10#here", url));

			url.setQueryParam("b", "");
			grp->add(new lie::test::ValueTest<std::string>("Protocol", "https", url.getProtocol()));
			grp->add(new lie::test::ValueTest<std::string>("Domain", "www.test.com", url.getDomain()));
			grp->add(new lie::test::ValueTest<std::string>("Resource", "here/we/are", url.getResource()));
			grp->add(new lie::test::ValueTest<std::string>("Query String", "", url.getQueryString()));
			grp->add(new lie::test::ValueTest<int>("Query Param Count", 0, url.getQueryParamCount()));
			grp->add(new lie::test::ValueTest<std::string>("Fragment Identifier", "here", url.getFragmentIdentifier()));
			grp->add(new lie::test::ValueTest<std::string>("To String", "https://www.test.com/here/we/are#here", url));
		}

		{
			lie::test::TestGroup* resCompGroup = urlGroup->createSubgroup("Component Wise Resource Manipulation");
			Url url("", "www.example.com", "hello/world");
			resCompGroup->add(new lie::test::ValueTest<std::string>("Resource", "hello/world", url.getResource()));
			resCompGroup->add(new lie::test::ValueTest<int>("Resource Comp Count", 2, url.getResourceComponentCount()));
			resCompGroup->add(new lie::test::ValueTest<std::string>("Resource Comp0", "hello", url.getResourceComponent(0)));
			resCompGroup->add(new lie::test::ValueTest<std::string>("Resource Comp1", "world", url.getResourceComponent(1)));
			
			resCompGroup->add(new lie::test::Comment("pushed 'test/'"));
			url.pushResourceComponent("test/");
			resCompGroup->add(new lie::test::ValueTest<std::string>("Resource", "hello/world/test/", url.getResource()));
			resCompGroup->add(new lie::test::ValueTest<int>("Resource Comp Count", 3, url.getResourceComponentCount()));
			resCompGroup->add(new lie::test::ValueTest<std::string>("Resource Comp0", "hello", url.getResourceComponent(0)));
			resCompGroup->add(new lie::test::ValueTest<std::string>("Resource Comp1", "world", url.getResourceComponent(1)));
			resCompGroup->add(new lie::test::ValueTest<std::string>("Resource Comp2", "test", url.getResourceComponent(2)));

			resCompGroup->add(new lie::test::Comment("set comp 1 to 'thing'"));
			url.setResourceComponent(1, "thing");
			resCompGroup->add(new lie::test::ValueTest<std::string>("Resource", "hello/thing/test/", url.getResource()));
			resCompGroup->add(new lie::test::ValueTest<int>("Resource Comp Count", 3, url.getResourceComponentCount()));
			resCompGroup->add(new lie::test::ValueTest<std::string>("Resource Comp0", "hello", url.getResourceComponent(0)));
			resCompGroup->add(new lie::test::ValueTest<std::string>("Resource Comp1", "thing", url.getResourceComponent(1)));
			resCompGroup->add(new lie::test::ValueTest<std::string>("Resource Comp2", "test", url.getResourceComponent(2)));

			resCompGroup->add(new lie::test::Comment("popped x2'"));
			url.popResourceComponent();
			url.popResourceComponent();
			resCompGroup->add(new lie::test::ValueTest<std::string>("Resource", "hello/", url.getResource()));
			resCompGroup->add(new lie::test::ValueTest<int>("Resource Comp Count", 1, url.getResourceComponentCount()));
			resCompGroup->add(new lie::test::ValueTest<std::string>("Resource Comp0", "hello", url.getResourceComponent(0)));

			resCompGroup->add(new lie::test::Comment("popped x2'"));
			url.popResourceComponent();
			url.popResourceComponent();
			resCompGroup->add(new lie::test::ValueTest<std::string>("Resource", "", url.getResource()));
			resCompGroup->add(new lie::test::ValueTest<int>("Resource Comp Count", 0, url.getResourceComponentCount()));

			resCompGroup->add(new lie::test::Comment("pushed thing/here.php'"));
			url.pushResourceComponent("thing/here.php");
			resCompGroup->add(new lie::test::ValueTest<std::string>("Resource", "thing/here.php", url.getResource()));
			resCompGroup->add(new lie::test::ValueTest<int>("Resource Comp Count", 2, url.getResourceComponentCount()));
			resCompGroup->add(new lie::test::ValueTest<std::string>("Resource Comp0", "thing", url.getResourceComponent(0)));
			resCompGroup->add(new lie::test::ValueTest<std::string>("Resource Comp1", "here.php", url.getResourceComponent(1)));
		}

		auto parseUrl = [](Url& url, std::string val){url.parse(val); return (std::string)url;};
		lie::test::TestGroup* parseGroup = urlGroup->createSubgroup("Parsing URL");
		{
			Url url;
			lie::test::TestGroup* grp = parseGroup->createSubgroup("Parse www.cppwl.co.uk");
			grp->add(new lie::test::NoExceptionTest<std::string, Url&, std::string>("Parsing", parseUrl, url, "www.cppwl.co.uk"));
			grp->add(new lie::test::ValueTest<std::string>("Protocol", "", url.getProtocol()));
			grp->add(new lie::test::ValueTest<std::string>("Domain", "www.cppwl.co.uk", url.getDomain()));
			grp->add(new lie::test::ValueTest<std::string>("Resource", "", url.getResource()));
			grp->add(new lie::test::ValueTest<std::string>("Query String", "", url.getQueryString()));
			grp->add(new lie::test::ValueTest<int>("Query Param Count", 0, url.getQueryParamCount()));
			grp->add(new lie::test::ValueTest<std::string>("Fragment Identifier", "", url.getFragmentIdentifier()));
			grp->add(new lie::test::ValueTest<std::string>("To String", "www.cppwl.co.uk", url));
		}

		{
			Url url;
			lie::test::TestGroup* grp = parseGroup->createSubgroup("Parse www.cppwl.co.uk/");
			grp->add(new lie::test::NoExceptionTest<std::string, Url&, std::string>("Parsing", parseUrl, url, "www.cppwl.co.uk/"));
			grp->add(new lie::test::ValueTest<std::string>("Protocol", "", url.getProtocol()));
			grp->add(new lie::test::ValueTest<std::string>("Domain", "www.cppwl.co.uk", url.getDomain()));
			grp->add(new lie::test::ValueTest<std::string>("Resource", "", url.getResource()));
			grp->add(new lie::test::ValueTest<std::string>("Query String", "", url.getQueryString()));
			grp->add(new lie::test::ValueTest<int>("Query Param Count", 0, url.getQueryParamCount()));
			grp->add(new lie::test::ValueTest<std::string>("Fragment Identifier", "", url.getFragmentIdentifier()));
			grp->add(new lie::test::ValueTest<std::string>("To String", "www.cppwl.co.uk", url));
		}

		{
			Url url;
			lie::test::TestGroup* grp = parseGroup->createSubgroup("Parse http://www.test.com/hello");
			grp->add(new lie::test::NoExceptionTest<std::string, Url&, std::string>("Parsing", parseUrl, url, "http://www.test.com/hello"));
			grp->add(new lie::test::ValueTest<std::string>("Protocol", "http", url.getProtocol()));
			grp->add(new lie::test::ValueTest<std::string>("Domain", "www.test.com", url.getDomain()));
			grp->add(new lie::test::ValueTest<std::string>("Resource", "hello", url.getResource()));
			grp->add(new lie::test::ValueTest<std::string>("Query String", "", url.getQueryString()));
			grp->add(new lie::test::ValueTest<int>("Query Param Count", 0, url.getQueryParamCount()));
			grp->add(new lie::test::ValueTest<std::string>("Fragment Identifier", "", url.getFragmentIdentifier()));
			grp->add(new lie::test::ValueTest<std::string>("To String", "http://www.test.com/hello", url));
		}

		{
			Url url;
			lie::test::TestGroup* grp = parseGroup->createSubgroup("Parse http://www.test.com/hello/");
			grp->add(new lie::test::NoExceptionTest<std::string, Url&, std::string>("Parsing", parseUrl, url, "http://www.test.com/hello/"));
			grp->add(new lie::test::ValueTest<std::string>("Protocol", "http", url.getProtocol()));
			grp->add(new lie::test::ValueTest<std::string>("Domain", "www.test.com", url.getDomain()));
			grp->add(new lie::test::ValueTest<std::string>("Resource", "hello/", url.getResource()));
			grp->add(new lie::test::ValueTest<std::string>("Query String", "", url.getQueryString()));
			grp->add(new lie::test::ValueTest<int>("Query Param Count", 0, url.getQueryParamCount()));
			grp->add(new lie::test::ValueTest<std::string>("Fragment Identifier", "", url.getFragmentIdentifier()));
			grp->add(new lie::test::ValueTest<std::string>("To String", "http://www.test.com/hello/", url));
		}

		{
			Url url;
			lie::test::TestGroup* grp = parseGroup->createSubgroup("Parse https://www.blob.org#stuff");
			grp->add(new lie::test::NoExceptionTest<std::string, Url&, std::string>("Parsing", parseUrl, url, "https://www.blob.org#stuff"));
			grp->add(new lie::test::ValueTest<std::string>("Protocol", "https", url.getProtocol()));
			grp->add(new lie::test::ValueTest<std::string>("Domain", "www.blob.org", url.getDomain()));
			grp->add(new lie::test::ValueTest<std::string>("Resource", "", url.getResource()));
			grp->add(new lie::test::ValueTest<std::string>("Query String", "", url.getQueryString()));
			grp->add(new lie::test::ValueTest<int>("Query Param Count", 0, url.getQueryParamCount()));
			grp->add(new lie::test::ValueTest<std::string>("Fragment Identifier", "stuff", url.getFragmentIdentifier()));
			grp->add(new lie::test::ValueTest<std::string>("To String", "https://www.blob.org#stuff", url));
		}

		{
			Url url;
			lie::test::TestGroup* grp = parseGroup->createSubgroup("Parse https://www.blob.org?a=1&b=20&hi=four");
			grp->add(new lie::test::NoExceptionTest<std::string, Url&, std::string>("Parsing", parseUrl, url, "https://www.blob.org?a=1&b=20&hi=four"));
			grp->add(new lie::test::ValueTest<std::string>("Protocol", "https", url.getProtocol()));
			grp->add(new lie::test::ValueTest<std::string>("Domain", "www.blob.org", url.getDomain()));
			grp->add(new lie::test::ValueTest<std::string>("Resource", "", url.getResource()));
			grp->add(new lie::test::ValueTest<std::string>("Query String", "a=1&b=20&hi=four", url.getQueryString()));
			grp->add(new lie::test::ValueTest<std::string>("Query Param 'a'", "1", url.getQueryParam("a")));
			grp->add(new lie::test::ValueTest<std::string>("Query Param 'b'", "20", url.getQueryParam("b")));
			grp->add(new lie::test::ValueTest<std::string>("Query Param 'hi'", "four", url.getQueryParam("hi")));
			grp->add(new lie::test::ValueTest<int>("Query Param Count", 3, url.getQueryParamCount()));
			grp->add(new lie::test::ValueTest<std::string>("Fragment Identifier", "", url.getFragmentIdentifier()));
			grp->add(new lie::test::ValueTest<std::string>("To String", "https://www.blob.org?a=1&b=20&hi=four", url));
		}

		{
			Url url;
			lie::test::TestGroup* grp = parseGroup->createSubgroup("Parse file:///C:/test/folder/file.txt");
			grp->add(new lie::test::NoExceptionTest<std::string, Url&, std::string>("Parsing", parseUrl, url, "file:///C:/test/folder/file.txt"));
			grp->add(new lie::test::ValueTest<std::string>("Protocol", "file", url.getProtocol()));
			grp->add(new lie::test::ValueTest<std::string>("Domain", "", url.getDomain()));
			grp->add(new lie::test::ValueTest<std::string>("Resource", "C:/test/folder/file.txt", url.getResource()));
			grp->add(new lie::test::ValueTest<std::string>("Query String", "", url.getQueryString()));
			grp->add(new lie::test::ValueTest<int>("Query Param Count", 0, url.getQueryParamCount()));
			grp->add(new lie::test::ValueTest<std::string>("Fragment Identifier", "", url.getFragmentIdentifier()));
			grp->add(new lie::test::ValueTest<std::string>("To String", "file:///C:/test/folder/file.txt", url));
		}

		{
			Url url;
			lie::test::TestGroup* grp = parseGroup->createSubgroup("Parse file://localhost/C:/test/folder/file.txt");
			grp->add(new lie::test::NoExceptionTest<std::string, Url&, std::string>("Parsing", parseUrl, url, "file://localhost/C:/test/folder/file.txt"));
			grp->add(new lie::test::ValueTest<std::string>("Protocol", "file", url.getProtocol()));
			grp->add(new lie::test::ValueTest<std::string>("Domain", "localhost", url.getDomain()));
			grp->add(new lie::test::ValueTest<std::string>("Resource", "C:/test/folder/file.txt", url.getResource()));
			grp->add(new lie::test::ValueTest<std::string>("Query String", "", url.getQueryString()));
			grp->add(new lie::test::ValueTest<int>("Query Param Count", 0, url.getQueryParamCount()));
			grp->add(new lie::test::ValueTest<std::string>("Fragment Identifier", "", url.getFragmentIdentifier()));
			grp->add(new lie::test::ValueTest<std::string>("To String", "file://localhost/C:/test/folder/file.txt", url));
		}

		{
			Url url;
			lie::test::TestGroup* grp = parseGroup->createSubgroup("Parse file://www.google.com/C:/test/folder/file.txt");
			grp->add(new lie::test::NoExceptionTest<std::string, Url&, std::string>("Parsing", parseUrl, url, "file://www.google.com/C:/test/folder/file.txt"));
			grp->add(new lie::test::ValueTest<std::string>("Protocol", "file", url.getProtocol()));
			grp->add(new lie::test::ValueTest<std::string>("Domain", "www.google.com", url.getDomain()));
			grp->add(new lie::test::ValueTest<std::string>("Resource", "C:/test/folder/file.txt", url.getResource()));
			grp->add(new lie::test::ValueTest<std::string>("Query String", "", url.getQueryString()));
			grp->add(new lie::test::ValueTest<int>("Query Param Count", 0, url.getQueryParamCount()));
			grp->add(new lie::test::ValueTest<std::string>("Fragment Identifier", "", url.getFragmentIdentifier()));
			grp->add(new lie::test::ValueTest<std::string>("To String", "file://www.google.com/C:/test/folder/file.txt", url));
		}

		{
			//if a '?' is after a '#' then the '?' and what follows is part of the fragment identifier, not the query string
			//https://en.wikipedia.org/wiki/Fragment_identifier
			Url url;
			lie::test::TestGroup* grp = parseGroup->createSubgroup("Parse test.com#anchor?yes");
			grp->add(new lie::test::NoExceptionTest<std::string, Url&, std::string>("Parsing", parseUrl, url, "test.com#anchor?yes"));
			grp->add(new lie::test::ValueTest<std::string>("Protocol", "", url.getProtocol()));
			grp->add(new lie::test::ValueTest<std::string>("Domain", "test.com", url.getDomain()));
			grp->add(new lie::test::ValueTest<std::string>("Resource", "", url.getResource()));
			grp->add(new lie::test::ValueTest<std::string>("Query String", "", url.getQueryString()));
			grp->add(new lie::test::ValueTest<int>("Query Param Count", 0, url.getQueryParamCount()));
			grp->add(new lie::test::ValueTest<std::string>("Fragment Identifier", "anchor?yes", url.getFragmentIdentifier()));
			grp->add(new lie::test::ValueTest<std::string>("To String", "test.com#anchor?yes", url));
		}

		{
			Url url;
			lie::test::TestGroup* grp = parseGroup->createSubgroup("Invalid URLs, expect exceptions");
			grp->add(new lie::test::ExceptionTest<lie::core::ExcepParseError, std::string, Url&, std::string>(
				"Parse empty string", parseUrl, url, ""));
			grp->add(new lie::test::ExceptionTest<lie::core::ExcepParseError, std::string, Url&, std::string>(
				"Parse '?'", parseUrl, url, "?"));
			grp->add(new lie::test::ExceptionTest<lie::core::ExcepParseError, std::string, Url&, std::string>(
				"Parse 'http:/:://thing.com/'", parseUrl, url, "http:/:://thing.com/"));
			grp->add(new lie::test::ExceptionTest<lie::core::ExcepParseError, std::string, Url&, std::string>(
				"Parse '://www.hi.com'", parseUrl, url, "://www.hi.com"));
			grp->add(new lie::test::ExceptionTest<lie::core::ExcepParseError, std::string, Url&, std::string>(
				"Parse '/hi'", parseUrl, url, "/hi"));
			grp->add(new lie::test::ExceptionTest<lie::core::ExcepParseError, std::string, Url&, std::string>(
				"Parse 'www.test.com#'", parseUrl, url, "www.test.com#"));
			grp->add(new lie::test::ExceptionTest<lie::core::ExcepParseError, std::string, Url&, std::string>(
				"Parse 'www.test.com?'", parseUrl, url, "www.test.com?"));
			grp->add(new lie::test::ExceptionTest<lie::core::ExcepParseError, std::string, Url&, std::string>(
				"Parse 'www.test.com?#'", parseUrl, url, "www.test.com?#"));
			grp->add(new lie::test::ExceptionTest<lie::core::ExcepParseError, std::string, Url&, std::string>(
				"Parse 'http:///hi.html'", parseUrl, url, "http:///hi.html"));
		}
		


	}

}

#endif