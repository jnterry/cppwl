/////////////////////////////////////////////////
/// Part of C++ Web Library - By Jamie Terry  ///
/////////////////////////////////////////////////
/// \file all.hpp
/// \author Jamie Terry
/// \date 2015/08/01
/// \brief This file includes all module unit-test files, which
/// in turn include all unit tests for cppwl.
/// It also includes a function that allows the entire test suite to
/// be run by calling cppwl::Test_All();
/// 
/// \defgroup unit-tests Unit Tests
/// \brief Contains unit tests for cppwl
/// 
/// \ingroup unit-tests
/////////////////////////////////////////////////

#ifndef CPPWL_TESTS_ALL_HPP
#define CPPWL_TESTS_ALL_HPP

#include <lie\test.hpp>
#include "core-test.hpp"
#include "route-test.hpp"

namespace cppwl{
	void Test_All(lie::test::TestGroup* group){
		lie::test::TestGroup* cppwlGroup = new lie::test::TestGroup("cppwl");
		group->add(cppwlGroup);

		Test_Core(cppwlGroup);
		Test_Route(cppwlGroup);
		
	}
}

#endif